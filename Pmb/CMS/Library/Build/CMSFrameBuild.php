<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: CMSFrameBuild.php,v 1.7 2023/01/31 11:36:53 qvarin Exp $
namespace Pmb\CMS\Library\Build;

class CMSFrameBuild extends FrameBuild
{

    private $cmsCadre = null;

    /**
     *
     * @return array
     */
    public function getHeaders()
    {
    	return $this->getCMSCadre() ? $this->getCMSCadre()->get_headers() : parent::getHeaders();
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Pmb\CMS\Library\Build\FrameBuild::buildNode()
     */
    public function buildNode()
    {
    	global $opac_parse_html, $charset;
    	
    	if (! empty($this->getCMSCadre())) {
    		
    		if (!$this->cmsCadre->check_conditions()) {
    			return false;
    		}
    		
    		$id = $this->getCMSCadre()->get_dom_id();
    		$html = $this->getCMSCadre()->show_cadre();
    	} else {
    		$id = $this->layoutElement->getSemantic()->getIdTag();
    		$html = $this->buildHTMLError("CMS Frame not found ({$id})");
    	}
    	
    	if ($opac_parse_html) {
    		$html = parseHTML($html);
    	}
    	
    	if ($charset == "utf-8") {
    		$html = "<?xml version='1.0' encoding='$charset'>" . $html;
    	}
    	
    	$dom = new \domDocument();
    	if (! @$dom->loadHTML($html)) {
    		return false;
    	}

    	$elementNode =  $this->portalDocument->importNode($dom->getElementById($id), true);
    	
    	$semantic = $this->layoutElement->getSemantic();
    	$newElementNode = $this->portalDocument->importNode($semantic->getNode(), true);
    	$containerNode = $semantic->getContainerNode();
    	
    	if (! empty($containerNode) && $containerNode->getAttribute('id') != $newElementNode->getAttribute('id')) {
    	    // On importe le noeud container et on r�cup�re tout les enfants du cadre CMS
    	    $containerNode = $this->portalDocument->importNode($containerNode, true);
    	    $this->portalDocument->switchParent($elementNode, $containerNode);
    	    $newElementNode->appendChild($containerNode);
    	    // Pour l'�l�ment $semantic->getIdTag() on stock le noeud container
    	    $this->portalDocument->elementNodeContainer[$semantic->getIdTag()] = $containerNode;
    	} else {
    	    $this->portalDocument->switchParent($elementNode, $newElementNode);
    	}
    	
    	return $this->portalDocument->importNode($newElementNode, true);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Pmb\CMS\Library\Build\FrameBuild::checkConditions()
     */
    public function checkConditions(): bool
    {
    	if (!$this->getCMSCadre()) {
    		return false;
    	}
    	return $this->cmsCadre->check_conditions() == true;
    }

    /**
     * 
     * @return \cms_module_root|null
     */
    private function getCMSCadre()
    {
        if (! isset($this->cmsCadre)) {
            $tagId = $this->layoutElement->getSemantic()->getIdTag();
            $idCadre = substr($tagId, strrpos($tagId, "_") + 1);
            $idCadre = intval($idCadre);

            $this->cmsCadre = \cms_modules_parser::get_module_class_by_id($idCadre);
        }
        return $this->cmsCadre;
    }

    /**
     * 
     * @param string $error
     * @return string
     */
    private function buildHTMLError(string $error)
    {
        $html = '<!-- ' . $error . ' -->';
        $html .= '<div id="' . $this->layoutElement->getSemantic()->getIdTag() . '" class="error_on_template" title="' . htmlspecialchars($error, ENT_QUOTES) . '">';
        $html .= 'Error Build';
        $html .= '</div>';
        return $html;
    }
}