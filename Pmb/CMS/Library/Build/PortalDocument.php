<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: PortalDocument.php,v 1.13 2022/11/14 09:16:46 qvarin Exp $
namespace Pmb\CMS\Library\Build;

use Pmb\Common\Helper\HTML;

class PortalDocument extends \DOMDocument
{

    private const ROOT_CONTAINER_ID = "container";

    /**
     *
     * @var \DOMElement|null
     */
    public $substituteContainer = null;

    public $elementNodeContainer = array();
    
    protected $deleted = array();
    
    /**
     *
     * @param string $version
     * @param string $encoding
     */
    public function __construct(string $version = "1.0", string $encoding = "")
    {
        global $charset;
        if (empty($encoding)) {
            $encoding = $charset;
        }
        parent::__construct($version, $encoding);
    }

    /**
     *
     * @param \DomNode $node
     * @param string $idParent
     * @param string $idPrevious
     * @return \DomNode
     */
    public function insertDomNode(\DomNode $node, string $idParent = '', string $idPrevious = ''): \DomNode
    {
        if ($idParent == self::ROOT_CONTAINER_ID) {
            $elementParent = $this->substituteContainer;
        } else {
            $elementParent = $this->getElementById($idParent);
            if (empty($elementParent)) {
                throw new \Exception("Parent element not found");
            }
        }

        if (! empty($this->elementNodeContainer[$idParent])) {
            $elementParent = $this->elementNodeContainer[$idParent];
        }

        if ($idParent == self::ROOT_CONTAINER_ID && empty($idPrevious)) {
            $elementPrevious = $elementParent->firstChild;
        } else {
            $elementPrevious = $this->getElementById($idPrevious);
        }

        $node = $elementParent->appendChild($node);
        if (! empty($elementPrevious)) {
            $node = $elementParent->insertBefore($elementPrevious, $node);
        }
        return $node;
    }

    /**
     *
     * {@inheritdoc}
     * @see \DOMDocument::loadHTML()
     */
    public function loadHTML($source, $options = null)
    {
    	if (! @parent::loadHTML(HTML::cleanHTML($source, $this->encoding), $options)) {
            throw new \Exception("HTML could not be loaded");
        }

        $this->substituteContainer = $this->createElement('div');
        $this->getBody()->appendChild($this->substituteContainer);
    }

    /**
     *
     * @return \DOMNode
     */
    public function getBody()
    {
        return $this->getElementsByTagName('body')->item(0);
    }

    /**
     *
     * @return \DOMNode
     */
    public function getHead()
    {
        return $this->getElementsByTagName('head')->item(0);
    }

    /**
     *
     * {@inheritdoc}
     * @see \DOMDocument::saveHTML()
     */
    public function saveHTML($node = null)
    {
        $rootContainerNode = $this->getElementById(self::ROOT_CONTAINER_ID);
        while ($domNode = $this->substituteContainer->childNodes->item(0)) {
            $rootContainerNode->appendChild($domNode);
        }
        $this->substituteContainer->parentNode->removeChild($this->substituteContainer);
        
        $index = count($this->deleted);
        for ($i = 0; $i < $index; $i++) {
            $element = $this->getElementById($this->deleted[$i]);
            if (!empty($element)) {
            	// On �vite de perdre le javascript
            	$DomNodeList = $element->getElementsByTagName("script");
            	while ($domNode = $DomNodeList->item(0)) {
            		$rootContainerNode->appendChild($domNode);
            	}
                $element->parentNode->removeChild($element);
            }
        }
        
        /**
         * On ajoute l'attribut "defer" pour les scripts VueJS
         * Pour qu'il trouve leur <div> (Noeud DOM racine)
         * 
         * Et on deplace les scripts[src="*"] pour les mettres dans la balise head
         * 
         * @var \DOMNodeList $DomNodeList
         */
        $DomNodeList = $this->getBody()->getElementsByTagName("script");
        
        $moveBefore = [];
        $index = count($DomNodeList);
        for ($i = 0; $i < $index; $i++) {
            $DomNode = $DomNodeList->item($i);   
            if ($DomNode->hasAttribute('src') && !$DomNode->hasAttribute('defer')) {
                $value = $DomNode->getAttribute('src');
                if (strpos($value, "vuejs") !== false) {
                    $DomNode->setAttribute('defer', 'defer');
                } else {
                	$moveBefore[] = $DomNode;
                }
            }
        }
        
        foreach ($moveBefore as $DomNode) {
        	$this->getHead()->appendChild($DomNode);
        }
       
        return parent::saveHTML($node);
    }

    /**
     *
     * @param \DomNode $parentOld
     * @param \DomNode $newParent
     */
    public function switchParent(\DomNode $parentOld, \DomNode $newParent)
    {
        $newParent = $this->mergeDomNodeAttributes($parentOld, $newParent);
        while ($childNode = $parentOld->childNodes->item(0)) {
            $newParent->appendChild($childNode);
        }
    }

    /**
     *
     * @param \DomNode $nodeOld
     * @param \DomNode $nodeNew
     * @return \DomNode
     */
    public function mergeDomNodeAttributes(\DomNode $nodeOld, \DomNode $nodeNew): \DomNode
    {
        $index = count($nodeOld->attributes);
        for ($i = 0; $i < $index; $i ++) {
            /**
             *
             * @var \DOMAttr $DomAttr
             */
            $DomAttr = $nodeOld->attributes->item($i);
            if ($DomAttr->name == "id") {
                continue;
            }

            $value = $DomAttr->value;
            if ($nodeNew->hasAttributes() && $nodeNew->hasAttribute($DomAttr->name)) {
                $newValue = $nodeNew->getAttributeNode($DomAttr->name)->value;
                switch ($DomAttr->name) {
                    case "class":
                        if ($newValue != $value) {
                            $values = array_merge(explode(" ", $newValue), explode(" ", $DomAttr->value));
                            $value = implode(" ", array_unique($values));
                        }
                        break;

                    default:
                        $value = $newValue;
                        break;
                }
            }
            $nodeNew->setAttribute($DomAttr->name, $value);
        }
        return $nodeNew;
    }
    
    public function isDeleted($id_tag) 
    {
        $this->deleted[] = $id_tag;
    }

    public function addHeader(string $header) 
    {
    	global $charset;
    	if ($charset == "utf-8") {
    		$header = "<?xml version='1.0' encoding='$charset'>" . $header;
    	}
    	
    	$domDocument = new \domDocument();
    	if (! @$domDocument->loadHTML($header)) {
    		return false;
    	}
    	
    	$head = $domDocument->getElementsByTagName("head")->item(0);
    	for ($i = 0; $i < $head->childNodes->length; $i++) {
    		$node = $this->importNode($head->childNodes->item($i), true);
    		$this->getHead()->appendChild($node);
    	}
    }

    public function replaceHeader(string $header) 
    {
    	global $charset;
    	if ($charset == "utf-8") {
    		$header = "<?xml version='1.0' encoding='$charset'>" . $header;
    	}
    	
    	$domDocument = new \domDocument();
    	if (! @$domDocument->loadHTML($header)) {
    		return false;
    	}
    	
    	$head = $domDocument->getElementsByTagName("head")->item(0);
    	for ($i = 0; $i < $head->childNodes->length; $i++) {
    		$newNode = $head->childNodes->item($i);
    		$similarNode = $this->foundSimilarNode($newNode);
    		
    		if (!empty($similarNode)){
    			$similarNode->parentNode->removeChild($similarNode);
    		}
    		if (!empty($newNode)) {
    			$this->getHead()->appendChild($this->importNode($newNode, true));
    		}
    	}
    }

    public function foundSimilarNode(\DomNode $node) 
    {
    	$elements = $this->getElementsByTagName($node->nodeName);
    	if ($elements->length == 0) {
    		return null;
    	}
    	
    	$attibutes = $node->attributes ?? new \DOMNamedNodeMap();
    	if ($attibutes->length == 0) {
    		return null;
    	}
    	
    	for ($i = 0; $i < $elements->length; $i++) {    		
    		$element = $elements->item($i);
    		
    		$match = true;
    		for ($j = 0; $j < $attibutes->length; $j++) {
    			$attibute = $attibutes->item($j);
    			if ($attibute->name == "content" || $attibute->name == "value"){
    				continue;
    			}
    			
    			if (!$element->hasAttributes($attibute->name) || $element->getAttribute($attibute->name) != $attibute->value) {
    				$match = false;
    				break;
    			}
	    	}
	    	
	    	if ($match){
	    		return $element;
	    	}
    	}
    	
    	return null;
    }
}