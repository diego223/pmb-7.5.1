<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootUrlBuilder.php,v 1.1 2022/03/31 13:10:36 qvarin Exp $
namespace Pmb\CMS\Library\UrlBuilder;

class RootUrlBuilder implements UrlBuilder
{

    public function makeUrl(): string
    {
        global $opac_url_base;
        return $opac_url_base;
    }

    /**
     * 
     * @param string|int $type
     * @param string|int $subType
     * @return RootUrlBuilder
     */
    public static function getClassUrlBuilder($type, $subType = ""): RootUrlBuilder
    {
        $type = intval($type);
        $subType = intval($subType);
        
        
        $classname = "\\Pmb\\CMS\\Library\\UrlBuilder\\UrlBuilder_{$type}";
        if (!empty($subType)) {
            $classname .= "_{$subType}";
        }

        if (class_exists($classname)) {
            return new $classname();
        }

        return new RootUrlBuilder();
    }
}

