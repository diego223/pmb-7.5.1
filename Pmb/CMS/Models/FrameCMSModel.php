<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: FrameCMSModel.php,v 1.5 2023/01/31 11:36:54 qvarin Exp $
namespace Pmb\CMS\Models;

class FrameCMSModel extends FrameAbstractModel
{
    public static function getHashCadre(string $tagId) 
    {
      
        $idCadre = substr($tagId, strrpos($tagId, "_") + 1);
        $idCadre = intval($idCadre);
        
        $cadreName = str_replace("_$idCadre", "", $tagId);
        
        return call_user_func(array($cadreName, "get_hash_cache"), $tagId, $idCadre);
    }
    
    public function clearCache() 
    {
        $hash = static::getHashCadre($this->getSemantic()->getIdTag());
        if (!empty($hash)) {
            pmb_mysql_query("DELETE FROM cms_cache_cadres WHERE cache_cadre_hash ='$hash'");
        }
    }
    
    public function getName()
    {
        $tagId = $this->getSemantic()->getIdTag();
        $idCadre = substr($tagId, strrpos($tagId, "_") + 1);
        $idCadre = intval($idCadre);

        $cmsCadre = \cms_modules_parser::get_module_class_by_id($idCadre);
        return $cmsCadre->name;
    }

    public function setName($name)
    {
        $this->name = "";
    }
}