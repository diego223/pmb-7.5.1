<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: LayoutContainerModel.php,v 1.6 2022/03/11 13:13:45 jparis Exp $
namespace Pmb\CMS\Models;

class LayoutContainerModel extends LayoutNodeModel
{

    public static $nbInstance = 0;

    /**
     * @var LayoutContainerModel[]
     */
    public static $instances = array();

    /**
     *
     * @var \Pmb\CMS\Semantics\RootSemantic|null
     */
    protected $semantic = null;
    
    public $isHidden = false;

    /**
     * 
     * @return \Pmb\CMS\Semantics\RootSemantic|null
     */
    public function getSemantic()
    {
        return $this->semantic;
    }
}