<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: GlobalContext.php,v 1.3 2023/01/11 14:50:01 qvarin Exp $

namespace Pmb\Common\Helper;

class GlobalContext
{
    /**
     * Recuperation d'une valuer globale
     *
     * @param string $name
     * @return NULL|mixed
     */
    public static function get(string $name)
    {
        global ${$name};
        global ${"pmb_$name"};
        global ${"opac_$name"};
        
        if (defined('GESTION') && isset(${"pmb_$name"})) {
            return ${"pmb_$name"};
        }
        if (!defined('GESTION') && isset(${"opac_$name"})) {
            return ${"opac_$name"};
        }

        return ${$name} ?? null;
    }

    public static function msg(string $code, string $default = "")
    {
        global $msg;
        return (!empty($msg) && !empty($msg[$code])) ? $msg[$code] : $default;
    }
    
}

