<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: Image.php,v 1.3 2022/12/23 08:35:12 qvarin Exp $
namespace Pmb\Common\Library\Image;

use Pmb\Common\Helper\GlobalContext;

class Image
{
    public static function resize(string $image, ?int $maxSize = null)
    {
        if (empty($image)) {
            return null;
        }
        
        if (!$maxSize) {
            $maxSize = GlobalContext::get("notice_img_pics_max_size") ?? 150;
        }
        
        $img = imagecreatefromstring($image);
        $redim = false;
        
        if (imagesx($img) >= imagesy($img)) {
            if (imagesx($img) <= $maxSize) {
                $largeur = imagesx($img);
                $hauteur = imagesy($img);
            } else {
                $redim = true;
                $largeur = $maxSize;
                $hauteur = ($largeur * imagesy($img)) / imagesx($img);
            }
        } else {
            if (imagesy($img) <= $maxSize) {
                $hauteur = imagesy($img);
                $largeur = imagesx($img);
            } else {
                $redim = true;
                $hauteur = $maxSize;
                $largeur = ($hauteur * imagesx($img)) / imagesy($img);
            }
        }
        
        $imgResized = imagecreatetruecolor($largeur, $hauteur);
        $white = imagecolorallocate($imgResized, 255, 255, 255);
        imagefilledrectangle($imgResized, 0, 0, $largeur, $hauteur, $white);
        if ($redim) {
            imagecopyresampled($imgResized, $img, 0, 0, 0, 0, $largeur, $hauteur, imagesx($img), imagesy($img));
        } else {
            imagecopyresampled($imgResized, $img, 0, 0, 0, 0, $largeur, $hauteur, $largeur, $hauteur);
        }
        
        return $imgResized;
    }

    public static function format(string $image, ?int $maxSize = null, string $watermark = "")
    {
        $image = Image::resize($image, $maxSize);
        if (empty($image)) {
            return null;
        }
        
        // Copyright
        if ($watermark) {
            $white = imagecolorallocate($image, 255, 255, 255);
            imagestring($image, 1, (imagesx($image) / 3), (imagesy($image) / 1.1), $watermark, $white);
        }
        
        return $image;
    }
    
    
    public static function printPNG($image)
    {
        if (empty($image)) {
            return null;
        }
        
        header('Content-Type: image/png');
        imagepng($image);
        imagedestroy($image);
        return true;
    }
}

