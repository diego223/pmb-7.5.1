<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ParserDirectory.php,v 1.2 2022/12/06 09:44:50 tsamson Exp $

namespace Pmb\Common\Library\Parser;

class ParserDirectory
{
    /**
     * repertoire de depart du parse
     * @var string
     */
    protected $baseDir = __DIR__;
	
	/**
	 * 
	 * @var array
	 */
	protected $manifest = [];

	/**
	 * 
	 * @var array
	 */
	protected $pathManifest = [];

	/**
	 * 
	 * @var boolean
	 */
	protected $parsed = false;
	
	/**
	 * 
	 * @var ParserDirectory
	 */
	public static $instance = null;
	
	/**
	 * namespace complet de la classe du parser manifest a utiliser (dsi, thumbnail,...)
	 * @var string
	 */
	protected $parserManifest = "\Pmb\Common\Library\Parser\ParserManifest";
	
	private function __construct()
	{
	    //$this->parse($this->baseDir);
	}
	
	public static function getInstance() {
	    if (is_null(static::$instance)) {
	        static::$instance = new static();
	        static::$instance->parse();
	    }
	    return static::$instance;
	}
	
	/**
	 * 
	 * @param string $path
	 * @return boolean
	 */
	protected function parse() 
	{
	    $path = $this->baseDir;
		$this->manifest = $this->loadManifests($path);
		$this->parsed = true;
	}
	
	/**
	 * 
	 * @param string $path
	 * @param array $ignoredManifest
	 * @return ParserManifest[]|array
	 */
	protected function loadManifests(string $path, array $ignoreManifest = [])
	{
		$path = realpath($path);
		$ignoreManifest = array_map('realpath', $ignoreManifest);
		
		if (!is_dir($path)) {
			return [];
		}
		
		$manifest = array();
		$dirs = glob($path . '/*', GLOB_ONLYDIR);
		foreach($dirs as $dir) {
			if (strpos($dir, 'CVS') !== false) {
				continue;
			}
			
			$file = "{$dir}/manifest.xml";
			if (is_file($file) && !in_array($file, $ignoreManifest)) {
				if (empty($this->pathManifest[$file])) {
				    $this->pathManifest[$file] = new $this->parserManifest($file);
				}
				$manifest[] = $this->pathManifest[$file];
			}
			$manifest = array_merge($manifest, $this->loadManifests($dir, $ignoreManifest));
		}
		return $manifest;
	}
	
	public function getManifests(string $path = "") {
	    $path = realpath($path);
	    
	    if (!is_dir($path)) {
	        return $this->manifest;
	    }
	    
	    $manifests = array();
	    
	    if (!empty($this->pathManifest)) {
	        foreach ($this->pathManifest as $file => $manifest) {
	            if (strpos($file, $path) === 0) {
	                $manifests[] = $manifest;
	            }
	        }
	    }
	    return $manifests;
	}
	
}

