<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: VueJsView.php,v 1.9 2022/10/18 13:45:59 qvarin Exp $

namespace Pmb\Common\Views;

class VueJsView
{
    protected $name = "";
    
    protected $data = [];
    
    protected $path = "./includes/templates/vuejs/";
    
    protected $distPath = "./javascript/vuejs/";

    public function __construct(string $name, $data = [], $path = "")
    {
        $this->name = $name;
        $this->data = $data;
        if(!empty($path)){
            $this->path = $path;
        }
        
    }
    
    public function render()
    {
        $content = "";
        if(file_exists($this->path.$this->name."/".basename($this->name).".html")){
            $content = file_get_contents($this->path.$this->name."/".basename($this->name).".html");
        }

        $content.= "<script type='text/javascript'>var \$data = ".\encoding_normalize::json_encode($this->data).";</script>";
        $content.= "<script type='text/javascript' src='".$this->distPath."babel-polyfill.js'></script>";
        $content.= "<script type='text/javascript' src='".$this->distPath.$this->name.".js'></script>";
        return $content;
    }
}

