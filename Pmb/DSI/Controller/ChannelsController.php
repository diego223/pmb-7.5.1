<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ChannelsController.php,v 1.12 2023/02/08 13:37:31 jparis Exp $
namespace Pmb\DSI\Controller;

use Pmb\DSI\Models\Channel\Mail\MailChannel;
use Pmb\DSI\Models\Channel\RootChannel;
use Pmb\DSI\Models\DSIParserDirectory;


class ChannelsController extends CommonController
{

	protected const VUE_NAME = "dsi/channels";
    /**
	 *
	 * {@inheritdoc}
	 * @see CommonController::getBreadcrumb
	 */
	protected function getBreadcrumb(): string
    {
		global $msg;
		return "{$msg['dsi_menu']} {$msg['menu_separator']} {$msg['dsi_channels']}";
	}

	protected function defaultAction()
	{
		$channel = RootChannel::getInstance();
		$this->render([
			"list" => array_filter($channel->getList(), function ($value) {
				return $value["model"] == true;
			}),
			"channelTypeList" => $this->getTypeList()
		]);
	}

	protected function editAction()
	{
		global $id;
		$id = intval($id);

		$this->render([
			"channel" => RootChannel::getInstance($id),
			"channelTypeList" => $this->getTypeList()
		]);
	}

	protected function addAction()
	{
		$this->render([
			"channel" => new RootChannel(),
			"channelTypeList" => $this->getTypeList()
		]);
	}

	public function save()
	{
		$this->data->id = intval($this->data->id);

		$channel = RootChannel::getInstance($this->data->id);
		$result = $channel->check($this->data);
		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$channel->setFromForm($this->data);

		if (0 == $this->data->id) {
			$channel->create();
		} else {
			$channel->update();
		}

		$this->ajaxJsonResponse($channel);
		exit();
	}

	public function delete()
	{
		$channel = RootChannel::getInstance($this->data->id);
		$result = $channel->delete();

		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$this->ajaxJsonResponse([
			'success' => true
		]);
		exit();
	}

	public function getMailList()
	{
		$data = MailChannel::getMailList();
		$this->ajaxJsonResponse($data);
	}

	private function getTypeList(): array
    {
		$channelTypeList = [];
		$manifests = DSIParserDirectory::getInstance()->getManifests("Pmb/DSI/Models/Channel/");
		foreach ($manifests as $manifest) {
			$message = $manifest->namespace::getMessages();
			$channelTypeList[] = [
				"id" => RootChannel::IDS_TYPE[$manifest->namespace],
				"namespace" => $manifest->namespace,
				"name" => $message['name']
			];
		}

		return $channelTypeList;
	}

	public function getTypeListAjax()
	{
		$this->ajaxJsonResponse($this->getTypeList());
	}

	public function getModels()
	{
		$this->ajaxJsonResponse($this->fetchModels());
	}

	protected function fetchModels(): array
    {
		$result = array();
		$channel = RootChannel::getInstance();
		$list = $channel->getList();
		foreach ($list as $elem) {
			if ($elem['model']) {
				$result[] = $elem;
			}
		}
		return $result;
	}

    /**
     * @param int $idModel
     */
    public function getModel(int $idModel)
	{
		$this->ajaxJsonResponse(RootChannel::getInstance($idModel));
	}

	public function getRequirements(int $type): array
    {
		$namespace = array_search($type, RootChannel::IDS_TYPE);
		if ($namespace !== false) {
			$this->ajaxJsonResponse($namespace::CHANNEL_REQUIREMENTS);
		}
		return array();
	}
	
	/**
	 * relie un tag a l'entite
     */
	public function unlinkTag()
	{
		$channel = RootChannel::getInstance();
		$delete = $channel->unlinkTag($this->data->numTag, $this->data->numEntity);
		$this->ajaxJsonResponse($delete);
	}
	
	/**
	 * Supprime le lien entre un tag et une entite
     */
	public function linkTag()
	{
		$channel = RootChannel::getInstance();
		$link = $channel->linkTag($this->data->numTag, $this->data->numEntity);
		$this->ajaxJsonResponse($link);
	}
}

