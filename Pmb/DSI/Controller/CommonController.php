<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: CommonController.php,v 1.8.4.1 2023/03/08 15:01:22 jparis Exp $
namespace Pmb\DSI\Controller;

use Pmb\Common\Views\VueJsView;
use Pmb\Common\Controller\Controller;
use Pmb\Common\Helper\Helper;
use Pmb\Common\Helper\HelperEntities;
use notice;
use emprunteur;
use search;

class CommonController extends Controller
{

    protected const VUE_NAME = "";

    public function proceed()
    {
        $method = Helper::camelize("{$this->data->action}_Action");
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }
        $this->defaultAction();
    }

    /**
     * Recuperation fil d'Arianne
     *
     * @return string
     */
    protected function getBreadcrumb()
    {
        global $msg;
        return "{$msg['dsi_menu']}";
    }

    /**
     * Generation vue
     *
     * @param array $data
     */
    protected function render(array $data = [])
    {
        global $pmb_url_base;
        $vueJsView = new VueJsView(static::VUE_NAME, array_merge(Helper::toArray($this->data), [
            "breadcrumb" => $this->getBreadcrumb(),
            "url_webservice" => $pmb_url_base . "rest.php/dsi/",
        ], Helper::toArray($data)));
        print $vueJsView->render();
    }

    /**
     * Joue une recherche multicritere
     */
    public function search()
    {
        $search_fields = HelperEntities::get_entities_search_fields()[$this->data->type];

        $search = new search(false, $search_fields);
        $search->unserialize_search(serialize(json_decode(json_encode($this->data->search), true)));

        $table = $search->make_search();
        $tableId = HelperEntities::get_entities_table_ids()[$this->data->type];

        $entities = array();
        $result = pmb_mysql_query("SELECT * FROM $table");
        if (pmb_mysql_num_rows($result)) {
            while ($row = pmb_mysql_fetch_object($result)) {
                switch ($this->data->type) {
                    case "record":
                        $notice = new notice($row->{$tableId});
                        $entities[$row->{$tableId}] = gen_plus($row->{$tableId}, HelperEntities::get_entity_title($row->{$tableId}, $this->data->type), $notice->get_detail());
                        break;
                    case "empr":
                        $empr = new emprunteur($row->{$tableId}, "", false, 1);
                        $entities[$row->{$tableId}] = $empr->lien_nom_prenom;
                        break;
                }
            }
        }
        $this->ajaxJsonResponse($entities);
    }
}

