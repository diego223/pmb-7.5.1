<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: DiffusionsController.php,v 1.15 2023/02/09 14:43:01 rtigero Exp $
namespace Pmb\DSI\Controller;

use Pmb\Common\Helper\HelperEntities;
use Pmb\DSI\Models\Channel\RootChannel;
use Pmb\DSI\Models\Diffusion;
use Pmb\DSI\Models\DiffusionProduct;
use Pmb\DSI\Models\DiffusionStatus;
use Pmb\DSI\Models\DSIParserDirectory;
use Pmb\DSI\Models\EventDiffusion;
use Pmb\DSI\Models\Product;
use Pmb\DSI\Models\View\RootView;
use Pmb\DSI\Orm\DiffusionProductOrm;

class DiffusionsController extends CommonController
{

	protected const VUE_NAME = "dsi/diffusions";

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Pmb\DSI\Controller\CommonController::getBreadcrumb()
	 */
	protected function getBreadcrumb()
	{
		global $msg;
		return "{$msg['dsi_menu']} {$msg['menu_separator']} {$msg['dsi_diffusions']}";
	}

	/**
	 * Ajout diffusion
	 */
	protected function addAction()
	{
		print $this->render($this->getFormData());
	}

	/**
	 * Edition diffusion
	 */
	protected function editAction()
	{
		global $id;
		$id = intval($id);

		print $this->render($this->getFormData($id));
	}

	/**
	 * Liste des diffusions
	 */
	protected function defaultAction()
	{
	    $diffusion = new Diffusion();
	    $diffusionStatus = new DiffusionStatus();
	    
	    print $this->render([
	        "list" => $diffusion->getList(),
	        "diffusionStatus" => $diffusionStatus->getList(),
			"channelsType" => $this->getChannelTypeList()
	    ]);
	}
	
	/**
	 * Recuperation donnees formulaire ajout/edition
	 * 
	 * @param number $id
	 * @return array[]
	 */
	protected function getFormData($id = 0)
	{
		$data = array();

		$data = ["channels" => $this->getChannelTypeList()];

		$diffusionStatus = new DiffusionStatus();
		$data["diffusionStatus"] = $diffusionStatus->getList();
		$data["diffusion"] = new Diffusion($id);
		
		$product = new Product();
		$data["products"] = $product->getList();

		return $data;
	}
	
	public function save()
	{
		$this->data->id = intval($this->data->id);
		
		$diffusion = new Diffusion($this->data->id);
		$result = $diffusion->check($this->data);
		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		
		$diffusion->setFromForm($this->data);
		if (0 == $this->data->id) {
			$diffusion->create();
		} else {
			$diffusion->update();
		}

		//Save des events de la diffusion
		foreach ($this->data->events as $eventDiffusion) {
			$eventDiffusionModel = new EventDiffusion($eventDiffusion->id, $diffusion->id);
			$result = $eventDiffusionModel->check($eventDiffusion);
			if (!$result) {
				$this->ajaxError($result['errorMessage']);
				exit();
			}
			
			if ((isset($eventDiffusion->id) && 0 == $eventDiffusion->id)) {
				$eventDiffusionModel->create();
			} else {
				$eventDiffusionModel->update();
			}
		}
		
		$this->ajaxJsonResponse($diffusion);
		exit();
	}
	
	public function delete()
	{
		$diffusion = new Diffusion($this->data->id);
		$result = $diffusion->delete();
		
		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$this->ajaxJsonResponse([
			'success' => true
		]);
		exit();
	}

	public function getChannelTypeList() {
        $channelTypeList = [];
        $manifests = DSIParserDirectory::getInstance()->getManifests("Pmb/DSI/Models/Channel/");
		foreach ($manifests as $manifest) {
			$message = $manifest->namespace::getMessages();
			$channelTypeList[] = [
                "id" => RootChannel::IDS_TYPE[$manifest->namespace],
				"namespace" => $manifest->namespace,
				"name" => $message['name']
			];
		}

        return $channelTypeList;
    }

	public function getEntityList() {
        return $this->ajaxJsonResponse(HelperEntities::get_entities_labels());
    }
    
    /**
     * relie un tag a l'entite
     * @return array
     */
    public function unlinkTag()
    {
    	$diffusion = new Diffusion();
    	$delete = $diffusion->unlinkTag($this->data->numTag, $this->data->numEntity);
    	return $this->ajaxJsonResponse($delete);
    }
    
    /**
     * Supprime le lien entre un tag et une entite
     * @return array
     */
    public function linkTag()
    {
    	$diffusion = new Diffusion();
    	$link = $diffusion->linkTag($this->data->numTag, $this->data->numEntity);
    	return $this->ajaxJsonResponse($link);
    }
    
    public function renderView(int $idEntity)
    {
    	$diffusion = new Diffusion($idEntity);
    	return $this->ajaxJsonResponse($diffusion->view->render($diffusion->item));
    }
}

