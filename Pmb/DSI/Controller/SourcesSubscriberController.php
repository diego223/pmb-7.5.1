<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: SourcesSubscriberController.php,v 1.2 2022/10/20 12:47:40 dbellamy Exp $

namespace Pmb\DSI\Controller;

class SourcesSubscriberController extends CommonController
{

	protected const VUE_NAME = "dsi/sourcesSubscriber";
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Pmb\DSI\Controller\CommonController::getBreadcrumb()
	 */
	protected function getBreadcrumb()
	{
	    global $msg;
	    return "{$msg['dsi_menu']} {$msg['menu_separator']} {$msg['dsi_sources_subscribers']}";
	}
	
	protected function defaultAction()
	{
		print $this->render();
	}
	
}

