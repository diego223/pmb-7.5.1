<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: SubscriberListController.php,v 1.19 2023/02/15 10:26:18 rtigero Exp $
namespace Pmb\DSI\Controller;

use Pmb\DSI\Models\SubscriberList\RootSubscriberList;
use Pmb\DSI\Models\DSIParserDirectory;
use Pmb\Common\Helper\HelperEntities;
use Pmb\DSI\Models\SubscriberList\Subscriber;
use Pmb\DSI\Models\SubscriberList\SubscriberListContent;
use Pmb\DSI\Models\Channel\RootChannel;

class SubscriberListController extends CommonController
{

	protected const VUE_NAME = "dsi/subscriberList";

	/**
	 *
	 * {@inheritdoc}
	 * @see \Pmb\DSI\Controller\CommonController::getBreadcrumb()
	 */
	protected function getBreadcrumb()
	{
		global $msg;
		return "{$msg['dsi_menu']} {$msg['menu_separator']} {$msg['dsi_subscriber_list']}";
	}

	protected function defaultAction()
	{
		$data = array();
		$data['list'] = $this->fetchModels();
		print $this->render($data);
	}

	protected function addAction()
	{
		$data['subscriberList'] = RootSubscriberList::getSubscriberList();
		$data["types"] = HelperEntities::get_subscriber_entities();
		print $this->render($data);
	}

	protected function editAction()
	{
		global $id;
		$id = intval($id);

		$data['subscriberList'] = RootSubscriberList::getSubscriberList($id);
		$data["types"] = HelperEntities::get_subscriber_entities();
		print $this->render($data);
	}

	public function delete()
	{
		$subscriberList = RootSubscriberList::getSourceSubscriberList($this->data->id);
		$result = $subscriberList->delete();

		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$this->ajaxJsonResponse([
			'success' => true
		]);
		exit();
	}

	private function getFormatedManifests($namespace = "")
	{
		$manifests = DSIParserDirectory::getInstance()->getManifests($namespace);
		foreach ($manifests as $manifest) {
			$message = $manifest->namespace::getMessages();
			$subscriberListTypeList[] = [
				"id" => $manifest->id,
				"namespace" => $manifest->namespace,
				"name" => $message['name']
			];
		}

		return $subscriberListTypeList;
	}

	private function getManifestById($id = 0)
	{}

	public function getSources($id = 0)
	{
		$entity = HelperEntities::get_subscriber_entities()[$id];
		$data = $this->getFormatedManifests("./Pmb/DSI/Models/Source/Subscriber/Entities/" . $entity . "/");
		$this->ajaxJsonResponse($data);
	}

	public function getSelectors($namespace = "")
	{
		$data = array();

		$compatibility = DSIParserDirectory::getInstance()->getCompatibility($namespace);
		$selectors = $compatibility['selector'];

		foreach ($selectors as $selector) {
			$message = $selector::getMessages();
			$data[] = [
				"namespace" => $selector,
				"name" => $message['name']
			];
		}
		$this->ajaxJsonResponse($data);
	}

	public function save()
	{
		$this->data->idSubscriberList = intval($this->data->idSubscriberList);
		$subscriberList = RootSubscriberList::getSourceSubscriberList($this->data->idSubscriberList);
		$result = $subscriberList->check($this->data);
		if (isset($result['error'])) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}

		$subscriberList->setFromForm($this->data);
		if (0 == $this->data->idSubscriberList) {
			$subscriberList->create();
		} else {
			$subscriberList->update();
		}

		$this->ajaxJsonResponse($subscriberList);
		exit();
	}

	public function getEntity($idSubscriberList = 0)
	{
		return $this->ajaxJsonResponse(RootSubscriberList::getSourceSubscriberList($idSubscriberList));
	}

	public function getTypes()
	{
		return $this->ajaxJsonResponse(HelperEntities::get_subscriber_entities());
	}

	public function getModels()
	{
		return $this->ajaxJsonResponse($this->fetchModels());
	}

	protected function fetchModels()
	{
		$result = array();
		$subscriberList = RootSubscriberList::getSourceSubscriberList();
		$list = $subscriberList->getList();
		foreach ($list as $elem) {
			if ($elem['model']) {
				$result[] = $elem;
			}
		}
		return $result;
	}

	public function getModel($idModel)
	{
		return $this->ajaxJsonResponse(RootSubscriberList::getSourceSubscriberList($idModel));
	}

	public function addSubscriber($idSubscriberList = 0)
	{
		$this->data->id = intval($this->data->id);
		$subscriber = Subscriber::getInstance($this->data->id, $this->data->type);
		$subscriber->setFromForm($this->data);
		//Gestion de la jointure
		$subscriberListContent = new SubscriberListContent($subscriber->idSubscriber, $idSubscriberList);
		if ((isset($subscriber->idSubscriber) && 0 == $idSubscriberList) || (isset($idSubscriberList) && 0 == $subscriber->idSubscriber)) {
			$subscriberListContent->create();
		} else {
			$subscriberListContent->update();
		}
		$this->ajaxJsonResponse($subscriber);
		exit();
	}

	/**
	 * Suppression du lien entre le subscriber et la liste
	 *
	 * @param number $idSubscriberList
	 */
	public function removeSubscriberFromList($idSubscriberList = 0)
	{
		$this->data->id = intval($this->data->id);
		$subscriberListContent = new SubscriberListContent($this->data->id, $idSubscriberList);

		$result = $subscriberListContent->delete();

		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$this->ajaxJsonResponse([
			'success' => true
		]);
		exit();
	}

	/**
	 * Retourne une les donnees du selecteur a partir d'un formulaire de subscriberlist
	 */
	public function getSubscribersFromList()
	{
		if (empty($this->data->settings->subscriberListSource->subscriberListSelector->data)) {
			return $this->ajaxJsonResponse(array());
		}
		$subscriberListModel = RootSubscriberList::getSourceSubscriberList();
		$subscriberListModel->setFromForm($this->data);
		$this->ajaxJsonResponse($subscriberListModel->getSelectorData());
	}
	
	public function filterSubscribers($idSubscriberList, $channelType)
	{
		$subscriberList = RootSubscriberList::getSourceSubscriberList($idSubscriberList);
		$channelNamespace = array_search($channelType, RootChannel::IDS_TYPE);
		if($channelNamespace === false) {
			return $this->ajaxJsonResponse($subscriberList->subscribers);
		}
		$subscriberList->filterList($channelNamespace::CHANNEL_REQUIREMENTS['subscribers']);
		return $this->ajaxJsonResponse($subscriberList->subscribers);
	}
	
	/**
	 * relie un tag a l'entite
	 * @return unknown
	 */
	public function unlinkTag()
	{
		$subscriberList = RootSubscriberList::getSourceSubscriberList();
		$delete = $subscriberList->unlinkTag($this->data->numTag, $this->data->numEntity);
		return $this->ajaxJsonResponse($delete);
	}
	
	/**
	 * Supprime le lien entre un tag et une entite
	 * @return unknown
	 */
	public function linkTag()
	{
		$subscriberList = RootSubscriberList::getSourceSubscriberList();
		$link = $subscriberList->linkTag($this->data->numTag, $this->data->numEntity);
		return $this->ajaxJsonResponse($link);
	}
	
	/**
	 * Retourne la liste des subscribers en base d'une subscriberList
	 * @param int $idSubscriberList
	 * @return array
	 */
	public function getSubscribers(int $idSubscriberList)
	{
		$subscriberList = RootSubscriberList::getSourceSubscriberList($idSubscriberList);
		return $this->ajaxJsonResponse($subscriberList->getSubscribersFromDatabase());
	}
}