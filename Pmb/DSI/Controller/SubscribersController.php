<?php
namespace Pmb\DSI\Controller;

use Pmb\DSI\Models\SubscriberList\Subscribers\Subscriber;

class SubscribersController extends CommonController
{

	public function delete($entityType = "")
	{
		$subscriber = Subscriber::getInstance($entityType, $this->data->id);
		$result = $subscriber->delete();

		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$this->ajaxJsonResponse([
			'success' => true
		]);
		exit();
	}

	public function getEntity($entityType = "", $idSubscriber = 0)
	{
		return $this->ajaxJsonResponse(Subscriber::getInstance($entityType, $idSubscriber));
	}

	public function save($entityType)
	{
		$this->data->id = intval($this->data->id);
		$subscriber = Subscriber::getInstance($entityType, $this->data->id);
		$result = $subscriber->check($this->data);
		if (isset($result['error'])) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}

		$subscriber->setFromForm($this->data);
		if (0 == $this->data->id) {
			$subscriber->create();
		} else {
			$subscriber->update();
		}
		$this->ajaxJsonResponse($subscriber);
		exit();
	}
	
	/**
	 * Ajoute les subscribers a partir d'une liste contenant une source
	 *
	 * @param number $idSubscriberList
	 */
	public function importSubscribers(string $entityType, int $idEntity = 0)
	{
		$subscribers = array();
		if (! empty($this->data->subscribers)) {
			foreach ($this->data->subscribers as $subscriber) {
				$subscriberModel = Subscriber::getInstance($entityType);
				$subscriberModel->setFromForm($subscriber);
				$subscriberModel->setEntity($idEntity);
				$result = $subscriberModel->check($subscriber);
				if (isset($result['error'])) {
					continue;
				}
				$subscriberModel->create();
				$subscribers[] = $subscriberModel;
				// 				if ($subscriberModel->idSubscriber) {
				// 					$subscriberListContent = new SubscriberListContent($subscriberModel->idSubscriber, $idSubscriberList);
				// 					$subscriberListContent->create();
				// 				}
			}
		}
		return $this->ajaxJsonResponse($subscribers);
	}
	
	/**
	 * Desinscrit un abonne issu d'une source
	 * @param string $entityType
	 * @param int $entityId
	 */
	public function unsubscribe(string $entityType, int $entityId)
	{
		$subscriber = Subscriber::getInstance($entityType, $this->data->id);
		$subscriber->setFromForm($this->data);
		$subscriber->setEntity($entityId);
		$subscriber->unsubscribe();
		
		if (0 == $this->data->id) {
			$subscriber->create();
		} else {
			$subscriber->update();
		}
		$this->ajaxJsonResponse($subscriber);
		exit();
	}
	
	/**
	 * Reinscrit un abonne desinscrit
	 * @param string $entityType
	 * @param int $entityId
	 */
	public function subscribe(string $entityType, int $entityId)
	{
		$subscriber = Subscriber::getInstance($entityType, $this->data->id);
		$result = $subscriber->subscribe();

		$this->ajaxJsonResponse($result);
		exit();
	}
	
	/**
	 * Supprime tous les subscribers d'une entite en base
	 */
	public function emptySubscribers()
	{
		$subscriber = Subscriber::getInstance($this->data->entityType);
		$subscriber->setEntity($this->data->entityId);
		$empty = $subscriber->emptySubscribers();
		return $this->ajaxJsonResponse($empty);
	}
}

