<?php
namespace Pmb\DSI\Controller;

use Pmb\DSI\Models\Tag;

class TagsController extends CommonController
{

	public function save()
	{
		$tag = new Tag();

		$result = $tag->check($this->data);
		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}

		$tag->setFromForm($this->data);
		if (0 == $this->data->id) {
			$tag->create();
		} else {
			$tag->update();
		}
		$this->ajaxJsonResponse($tag);
		exit();
	}

	public function delete()
	{
		$tag = new Tag($this->data->id);
		$result = $tag->delete();

		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$this->ajaxJsonResponse([
			'success' => true
		]);
		exit();
	}

	public function getRelatedEntities(int $numTag)
	{
		$tagModel = new Tag();
		$entities = $tagModel->getRelatedEntities($numTag);
		$this->ajaxJsonResponse($entities);
	}
	
	public function getTags()
	{
		$tagModel = new Tag();
		$this->ajaxJsonResponse($tagModel->getTags());
	}
}

