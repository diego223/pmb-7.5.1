<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ViewsController.php,v 1.20 2023/02/22 13:54:03 jparis Exp $

namespace Pmb\DSI\Controller;

use Pmb\Common\Helper\HelperEntities;
use Pmb\DSI\Models\View\RootView;
use Pmb\DSI\Models\DSIParserDirectory;
use Pmb\DSI\Models\Item\RootItem;

class ViewsController extends CommonController
{

	protected const VUE_NAME = "dsi/views";
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Pmb\DSI\Controller\CommonController::getBreadcrumb()
	 */
	protected function getBreadcrumb()
	{
	    global $msg;
	    return "{$msg['dsi_menu']} {$msg['menu_separator']} {$msg['dsi_views']}";
	}
	
	protected function defaultAction()
	{
		$view = RootView::getInstance();
	    print $this->render([
	        "list" => array_filter($view->getList(), function($value) { return $value["model"] == true; }),
            "types" => $this->getTypeList(),
	    ]);
	}
	
	/**
	 * Ajout diffusion
	 */
	protected function addAction()
	{
		print $this->render($this->getFormData());
	}
	
	/**
	 * Edition diffusion
	 */
	protected function editAction()
	{
		global $id;
		$id = intval($id);
		
		print $this->render($this->getFormData($id));
	}

	public function save()
	{
		$this->data->id = intval($this->data->id);
		$view = RootView::getInstance($this->data->id);
		$result = $view->check($this->data);
		if ($result['error']) {
			$this->ajaxError($result['errorMessage']);
			exit();
		}
		$view->setFromForm($this->data);

		if (0 == $this->data->id) {
			$view->create();
		} else {
			$view->update();
		}
		$view->saveChilds();
		
		$this->ajaxJsonResponse($view);
		exit();
	}
	
	public function delete()
	{
		$view = RootView::getInstance($this->data->id);
		$view->deleteChilds();

		$result = $view->delete();
        if ($result['error']) {
            $this->ajaxJsonResponse($result);
			exit();
		}
		$this->ajaxJsonResponse([
			'success' => true
		]);
		exit();
	}

		/**
	 * Recuperation donnees formulaire ajout/edition
	 *
	 * @param number $id
	 * @return array[]
	 */
	protected function getFormData($id = 0)
	{
		$data = array();
		$data["view"] = new RootView($id);
		$data["types"] = $this->getTypeList();
		$data["entities"] = HelperEntities::get_entities_labels();
		return $data;
	}

	protected function getTypeList() {
        $viewTypeList = [];
        $manifests = DSIParserDirectory::getInstance()->getManifests("Pmb/DSI/Models/View/");
		foreach ($manifests as $manifest) {
			$message = $manifest->namespace::getMessages();
			$viewTypeList[] = [
				"id" => RootView::IDS_TYPE[$manifest->namespace],
				"namespace" => $manifest->namespace,
				"name" => $message['name']
			];
		}

        return $viewTypeList;
    }

	public function getTypeListAjax() {
		$this->ajaxJsonResponse($this->getTypeList());
	}
    
    public function getEntitiesDefaultTemplates() {
    	$data = HelperEntities::get_entities_default_templates();
    	$this->ajaxJsonResponse($data);
    }
    public function getEntityTree($type=0) {
    	$data = HelperEntities::get_entity_tree($type);
    	$this->ajaxJsonResponse($data);
    }
    
    public function getTemplateDirectories($entityType=0) {
    	switch($entityType) {
    		case TYPE_NOTICE:
    			return $this->ajaxJsonResponse(\notice_tpl::get_directories());
    		default :
    			return $this->ajaxJsonResponse(\auth_templates::get_directories());
    	}
    }

	public function getModels()
	{
		return $this->ajaxJsonResponse($this->fetchModels());
	}

	protected function fetchModels()
	{
		$result = array();
		$view = RootView::getInstance();
		$list = $view->getList();
		foreach ($list as $elem) {
			if ($elem['model']) {
				$result[] = $elem;
			}
		}
		return $result;
	}

	public function getModel($idModel)
	{
		return $this->ajaxJsonResponse(RootView::getInstance($idModel));
	}
	
	public function getCompatibility($type)
	{
		$result = array();
		$typeNamespace = array_search($type, RootView::IDS_TYPE);
		$compatibility = DSIParserDirectory::getInstance()->getCompatibility($typeNamespace);
		if(isset($compatibility['item'])) {
			foreach($compatibility['item'] as $item) {
				$result[] = $item::TYPE;
			}
		}
		return $this->ajaxJsonResponse($result);
		
	}
	
	/**
	 * relie un tag a l'entite
	 * @return array
	 */
	public function unlinkTag()
	{
		$view = RootView::getInstance();
		$delete = $view->unlinkTag($this->data->numTag, $this->data->numEntity);
		return $this->ajaxJsonResponse($delete);
	}
	
	/**
	 * Supprime le lien entre un tag et une entite
	 * @return array
	 */
	public function linkTag()
	{
		$view = RootView::getInstance();
		$link = $view->linkTag($this->data->numTag, $this->data->numEntity);
		return $this->ajaxJsonResponse($link);
	}

    /**
     * Retourne en AJAX la liste de toutes les vues
     */
    public function getViews()
    {
        $this->ajaxJsonResponse(RootView::getInstance()->getList());
    }
    
    public function renderView(int $idView, int $idItem)
    {
        $view = RootView::getInstance($idView);
        $item = RootItem::getInstance($idItem);
        return $this->ajaxJsonResponse($view->render($item));
    }

    /**
     * Retourne en AJAX un instance vide d'un RootView
     */
    public function getEmptyInstance()
    {
        $this->ajaxJsonResponse(RootView::getInstance());
    }
}

