<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: Channel.php,v 1.5 2023/02/08 10:54:18 rtigero Exp $

namespace Pmb\DSI\Models\Channel;

interface Channel
{
    public const CHANNEL_REQUIREMENTS = null;
	public function send();
}

