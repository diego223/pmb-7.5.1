<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: MailChannel.php,v 1.5 2023/01/26 15:07:32 rtigero Exp $

namespace Pmb\DSI\Models\Channel\Mail;

use Pmb\DSI\Models\Channel\RootChannel;

class MailChannel extends RootChannel
{
	public const CHANNEL_REQUIREMENTS = [
		"subscribers" => [
			"email" => [
				"input_type" => "email",
				"input_label" => "subscriber_email"
			]
		]
	];
	
	public function __construct(int $id = 0)
	{
		$this->id = $id;
        $this->read();
	}

    public static function getMailList() {
        $list = [];
        $query = "SELECT id_mail_configuration, name_mail_configuration FROM mails_configuration";
        $result = pmb_mysql_query($query);
        if(pmb_mysql_num_rows($result)) {
            while($row = pmb_mysql_fetch_object($result)) {
                $list[$row->id_mail_configuration] = $row->name_mail_configuration;
            }
        }
        return $list;
    }

    public function send() {
        // TODO
    }
}

