<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: PortalChannel.php,v 1.1 2022/11/23 10:50:49 jparis Exp $

namespace Pmb\DSI\Models\Channel\Portal;

use Pmb\DSI\Models\Channel\RootChannel;

class PortalChannel extends RootChannel
{
    public function __construct(int $id = 0)
	{
		$this->id = $id;
		$this->read();
	}
}

