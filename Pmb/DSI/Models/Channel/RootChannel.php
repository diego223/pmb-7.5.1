<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootChannel.php,v 1.13 2023/02/22 13:54:03 jparis Exp $

namespace Pmb\DSI\Models\Channel;

use Pmb\Common\Helper\Helper;
use Pmb\DSI\Models\Root;
use Pmb\DSI\Orm\ChannelOrm;

class RootChannel extends Root implements Channel
{
	protected const EXCLUDED_PROPERTIES = array(
		"idChannel",
		'numModel',
		'model'
	);
	
	public const TAG_TYPE = 2;
	
	public const IDS_TYPE = [
        "Pmb\DSI\Models\Channel\Mail\MailChannel" => 1,
        "Pmb\DSI\Models\Channel\SMS\SMSChannel" => 2,
        "Pmb\DSI\Models\Channel\Portal\PortalChannel" => 3,
    ];

    protected $ormName = "Pmb\DSI\Orm\ChannelOrm";

    public $id = 0;
    public $name = "";
    public $type = 0;
    public $model = false;
    public $settings = "";
    public $tags = null;
    
    // ORM props
    protected $idChannel = 0;

    public $numModel = 0;

    public function __construct(int $id = 0) {
        $this->id = intval($id);
		$this->read();
    }

    public static function getInstance(int $id = 0) {
        if(!empty($id)) {
            $channel = ChannelOrm::findById($id);
            if(!empty($channel)) {
                foreach(self::IDS_TYPE as $key => $value) {
                    if(self::IDS_TYPE[$key] == $channel->type) {
                        return new $key($id);
                    }
                }
            }
        }
        return new RootChannel($id);
    }

    public function read()
	{
		$this->fetchData();
	}

	public function check(object $data) {
		if (!is_string($data->name)) {
			return [
				'error' => true,
				'errorMessage' => 'msg:data_errors'
			];
		}

		if(!empty($data->name)) {
			$fields = ['name' => $data->name, 'model' => $data->model];
			if (!empty($data->id)) {
				$fields[$this->ormName::$idTableName] = [
					'value' =>  $data->id,
					'operator' => '!='
				];
			}
			$result = $this->ormName::finds($fields);
			if (!empty($result)) {
				return [
					'error' => true,
					'errorMessage' => 'msg:diffusion_duplicated'
				];
			}
		}
		
		return [
			'error' => false,
			'errorMessage' => ''
		];
	}

    public function create()
	{
        $orm = new $this->ormName();
		$orm->name = $this->name;
		$orm->type = $this->type;
		$orm->model = $this->model;
		$orm->settings = $this->settings;
		$orm->num_model = $this->numModel;
		$orm->save();
		
		$this->id = $orm->{$this->ormName::$idTableName};
		$this->{Helper::camelize($this->ormName::$idTableName)} = $orm->{$this->ormName::$idTableName};
    }
	
	public function update()
	{
		$orm = new $this->ormName($this->id);
		$orm->name = $this->name;
		$orm->type = $this->type;
		$orm->model = $this->model;
		$orm->settings = $this->settings;
		$orm->num_model = $this->numModel;
		$orm->save();
	}

	public function delete()
	{
		try {
            if(!$this->checkBeforeDelete()) {
                return [
                    'error' => true,
                    'errorMessage' => "msg:model_check_use"
                ];
            }
			$orm = new $this->ormName($this->id);
			$orm->delete();
		} catch(\Exception $e) {
			return [
				'error' => true,
				'errorMessage' => $e->getMessage()
			];
		}
		
		$this->id = 0;
		$this->{Helper::camelize($orm::$idTableName)} = 0;
		$this->name = '';
		$this->type = '';
		$this->model = false;
		$this->settings = [];
		$this->numModel = null;
		
		return [
			'error' => false,
			'errorMessage' => ''
		];
	}

    public function setFromForm(object $data)
	{
		$this->name = $data->name;
		$this->type = intval($data->type);
		$this->model = $data->model;
		$this->settings = json_encode($data->settings);
		$this->numModel = $data->numModel;
	}

    public function send() {
        // Derivate
    }
}

