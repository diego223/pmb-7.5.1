<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: SMSChannel.php,v 1.2 2023/01/26 15:07:32 rtigero Exp $

namespace Pmb\DSI\Models\Channel\SMS;

use Pmb\DSI\Models\Channel\RootChannel;

class SMSChannel extends RootChannel
{
    
	public const CHANNEL_REQUIREMENTS = [
		"subscribers" => [
			"phone" => [
				"input_type" => "tel",
				"input_label" => "subscriber_phone"
			]
		]
	];
	
	public function __construct(int $id = 0)
	{
		$this->id = $id;
		$this->read();
	}
}

