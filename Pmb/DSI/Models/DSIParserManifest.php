<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: DSIParserManifest.php,v 1.2.4.1 2023/03/08 16:04:59 qvarin Exp $

namespace Pmb\DSI\Models;

use Pmb\Common\Helper\Helper;
use Pmb\Common\Library\Parser\ParserManifest;

class DSIParserManifest extends ParserManifest
{

	/**
	 *
	 * @var string
	 */
	public $namespace;

	/**
	 *
	 * @var array
	 */
	public $compatibility = [];

	public $manually;

	public $id;

	/**
	 * 
	 * @param string $path
	 * @throws \InvalidArgumentException
	 */
	protected function formatData()
	{
		foreach ($this->simplexml->children() as $prop => $value) {
			if (in_array($prop, ['compatibility', 'author'])) {
				continue;
			}

			$this->{Helper::camelize($prop)} = $value->__toString();
		}

		if (! empty($this->simplexml->compatibility)) {
			foreach ($this->simplexml->compatibility->children() as $type => $value) {
				if (empty($type)) {
					// compatibility est vide
					break;
				}
				
				$value = $value->__toString();
				if ("\\" == substr($value, -1)) {
					// On a un Dossier
					global $base_path;
					$value = str_replace("\\", "/", $value);
					//a verifier le ignoremanifest
					//foreach (DSIParserDirectory::getInstance()->getManifests("{$base_path}/{$value}", [$this->path]) as $manifest) {
					foreach (DSIParserDirectory::getInstance()->getManifests("{$base_path}/{$value}") as $manifest) {
						$this->compatibility[$type][] = $manifest->namespace;
					}
				} elseif (class_exists($value)) {
					// On a un Namespace
					$this->compatibility[$type][] = $value;
				}
			}
		}
	}
}

