<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: Diffusion.php,v 1.18.2.1 2023/03/07 14:29:30 jparis Exp $
namespace Pmb\DSI\Models;

use Pmb\DSI\Models\Root;
use Pmb\Common\Helper\Helper;
use Pmb\DSI\Models\Channel\RootChannel;
use Pmb\DSI\Models\Event\RootEvent;
use Pmb\DSI\Models\Item\SimpleItem;
use Pmb\DSI\Models\View\RootView;
use Pmb\DSI\Models\SubscriberList\RootSubscriberList;
use Pmb\DSI\Orm\EventDiffusionOrm;
use Pmb\DSI\Orm\DiffusionProductOrm;
use Pmb\DSI\Models\SubscriberList\DiffusionSubscriberList;
use Pmb\DSI\Models\Item\RootItem;
use Pmb\DSI\Orm\DiffusionOrm;

class Diffusion extends Root implements CRUD
{

	public const TAG_TYPE = 1;
	
	protected $ormName = "Pmb\DSI\Orm\DiffusionOrm";
	protected $idDiffusion = 0;
	public $name = "";
	
	public $settings = "";

	public $status = "";

	public $channel = null;
	public $view = null;
	public $item = null;
	public $subscriberList = null;
	public $tags = null;
	public $automatic = 0;

	protected $numChannel = 0;
	protected $numView = 0;
	protected $numItem = 0;
	public $numSubscriberList = 0;
	protected $numStatus = 0;
	protected $diffusionProducts = null;
	protected $events = null;
	protected $diffusionHistory = array();
	
	public function __construct(int $id = 0)
	{
		$this->id = $id;
		$this->read();
	}

	public function create()
	{
		$orm = new $this->ormName();
		$orm->name = $this->name;
		$orm->settings = json_encode($this->settings);
		$orm->num_status = $this->status;
		$orm->num_channel = isset($this->numChannel) ? $this->numChannel : 0;
		$orm->num_view = isset($this->numView) ? $this->numView : 0;
		$orm->num_item = isset($this->numItem) ? $this->numItem : 0;
		$orm->num_subscriber_list = isset($this->numSubscriberList) ? $this->numSubscriberList : 0;
		$orm->save();

		$this->id = $orm->{$this->ormName::$idTableName};
		$this->{Helper::camelize($this->ormName::$idTableName)} = $orm->{$this->ormName::$idTableName};
	}
	
	public function check(object $data) {
		if (empty($data->name) || !is_string($data->name)) {
			return [
				'error' => true,
				'errorMessage' => 'msg:data_errors'
			];
		}
		
		$fields = ['name' => $data->name];
		if (!empty($data->id)) {
			$fields[$this->ormName::$idTableName] = [
				'value' =>  $data->id,
				'operator' => '!='
			];
		}
		
		$result = $this->ormName::finds($fields);
		if (!empty($result)) {
			return [
				'error' => true,
				'errorMessage' => 'msg:diffusion_duplicated'
			];
		}
		
		return [
			'error' => false,
			'errorMessage' => ''
		];
	}
	
	public function setFromForm(object $data)
	{
		$this->name = $data->name;
		$this->settings = $data->settings;
		$this->status = intval($data->numStatus);
		$this->numChannel = isset($data->numChannel) ? $data->numChannel : 0;
		$this->numView = isset($data->numView) ? $data->numView : 0;
		$this->numItem = isset($data->numView) ? $data->numItem : 0;
		$this->numSubscriberList = $data->numSubscriberList;
	}
	
	public function read()
	{
		$this->fetchData();
		$this->fetchRelations();
	}
	
	public function update()
	{
		$orm = new $this->ormName($this->id);
		$orm->name = $this->name;
		$orm->settings = json_encode($this->settings);
		$orm->num_status = $this->status;
		$orm->num_channel = $this->numChannel;
		$orm->num_view = $this->numView;
		$orm->num_item = $this->numItem;
		$orm->num_subscriber_list = $this->numSubscriberList;
		$orm->save();
	}
	
	public function delete()
	{
		try {
		    
			$orm = new $this->ormName($this->id);
			//Suppression des liens
			if($orm->num_subscriber_list != 0) {
		        $subscriberList = RootSubscriberList::getDiffusionSubscribers($this->id, $orm->num_subscriber_list);
		        $subscriberList->delete();
			}
			
			foreach ($orm->events as $diffusionEvent) {
		        $event = RootEvent::getInstance($diffusionEvent->num_event);
		        $event->delete();
			}
			
			if($orm->num_view != 0) {
    			$orm->view->delete();
			}
			
			if($orm->num_item != 0) {
			    $orm->item->delete();
			}
			
			if($orm->num_channel != 0) {
			    $orm->channel->delete();
			}
			
			$diffusionsProduct = DiffusionProductOrm::finds(["num_diffusion" => $this->id]);
			foreach ($diffusionsProduct as $diffusionProduct) {
			    $diffusionProduct = new DiffusionProductOrm(["num_diffusion" => $diffusionProduct->ids["num_diffusion"], "num_product" => $diffusionProduct->ids["num_product"]]);
			    $diffusionProduct->delete();
			}
			
			$orm->delete();
		} catch(\Exception $e) {
			return [
				'error' => true,
				'errorMessage' => $e->getMessage()
			];
		}
		
		$this->id = 0;
		$this->{Helper::camelize($orm::$idTableName)} = 0;
		$this->name = '';
		$this->status = '';
		
		return [
			'error' => false,
			'errorMessage' => ''
		];
	}

	public function fetchRelations() {
		$this->fetchView();
		$this->fetchItem();
		$this->fetchChannel();
		$this->fetchSubscriberList();
		$this->fetchEvents();
		
	}

	protected function fetchChannel()
	{
		$this->channel = RootChannel::getInstance($this->numChannel);
	}

	protected function fetchView()
	{
		$this->view = RootView::getInstance($this->numView);
	}

	protected function fetchItem()
	{
		$this->item = SimpleItem::getInstance($this->numItem);
	}
	
	protected function fetchSubscriberList() 
	{
		$this->subscriberList = RootSubscriberList::getDiffusionSubscribers($this->id, $this->numSubscriberList);
	}

	protected function fetchEvents() 
	{
		$events = EventDiffusionOrm::finds(["num_diffusion" => $this->id]);
		
		$this->events = array();
		foreach($events as $event) {
			$this->events[] = RootEvent::getInstance($event->num_event);
		}
	}
	
	/**
	 * Retourne le rendu de la vue associee a la diffusion
	 * @return string
	 */
	public function renderView()
	{
		if($this->view instanceof RootView && $this->item instanceof RootItem) {
			$limit = $this->view->settings->limit ?? 0;

			return $this->view->render($this->item, $this->id, $limit, "diffusions");
		}
		return "";
	}
	
	public static function listToSend()
	{
		$diffusions = DiffusionOrm::findAll();
		$result = array();
		foreach($diffusions as $diffusionOrm) {
			$diffusion = new self($diffusionOrm->id_diffusion);
			if($diffusion->trigger()){
				$result[] = $diffusion;
			}
		}
		return $result;
	}
	
	public function trigger()
	{
		foreach($this->events as $event) {
			if($event->trigger()) {
				$this->addDiffusionHistory();
				return true;
			}
		}
		return false;
	}
	
	protected function addDiffusionHistory()
	{
		$diffusionHistory = new DiffusionHistory();
		$diffusionHistory->numDiffusion = $this->id;
		$diffusionHistory->date = new \Datetime();
		$diffusionHistory->state = DiffusionHistory::INITIALIZED;
		$subscribers = RootSubscriberList::getDiffusionSubscribers($this->id, $this->numSubscriberList);
		$diffusionHistory->setSubscribers([
			$subscribers['source']->settings->subscriberListSource->namespace => $subscribers['source']->getFormatedSubscribers($subscribers['lists']), 
			get_class($subscribers['lists']) => $subscribers['lists']->getFormatedSubscribers()
			
		]);
		$diffusionHistory->setItems($this->item->getFormatedItem());
		$diffusionHistory->create();
		$this->diffusionHistory[] = $diffusionHistory;
	}
}

