<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: PeriodicalEvent.php,v 1.2 2022/10/13 12:41:33 qvarin Exp $

namespace Pmb\DSI\Models\Event\Periodical;

use Pmb\DSI\Models\Event\RootEvent;

class PeriodicalEvent extends RootEvent
{
}

