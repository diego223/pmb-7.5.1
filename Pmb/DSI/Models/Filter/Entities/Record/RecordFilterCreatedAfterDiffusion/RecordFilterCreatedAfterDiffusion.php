<?php
namespace Pmb\DSI\Models\Filter\Entities\Record\RecordFilterCreatedAfterDiffusion;

use Pmb\DSI\Models\Filter\Entities\Record\RecordFilter;

class RecordFilterCreatedAfterDiffusion extends RecordFilter
{

	/**
	 */
	public function __construct()
	{
		parent::__construct();
	}
}

