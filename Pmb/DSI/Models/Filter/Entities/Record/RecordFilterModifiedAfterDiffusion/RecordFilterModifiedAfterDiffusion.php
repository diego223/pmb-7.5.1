<?php
namespace Pmb\DSI\Models\Filter\Entities\Record\RecordFilterModifiedAfterDiffusion;

use Pmb\DSI\Models\Filter\Entities\Record\RecordFilter;

class RecordFilterModifiedAfterDiffusion extends RecordFilter
{

	/**
	 */
	public function __construct()
	{
		parent::__construct();
	}
}

