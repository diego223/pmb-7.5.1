<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: AggregatorItem.php,v 1.1 2023/02/08 13:37:31 jparis Exp $

namespace Pmb\DSI\Models\Item\Aggregator;

use Pmb\DSI\Models\Item\RootItem;

class AggregatorItem extends RootItem
{
    public const TYPE = 0;
    public function getItemList()
    {
        
    }
}

