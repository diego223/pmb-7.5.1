<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ArticleListItem.php,v 1.1.2.2 2023/03/08 08:34:40 jparis Exp $

namespace Pmb\DSI\Models\Item\Entities\Article\ArticleListItem;

use Pmb\DSI\Models\Item\SimpleItem;

class ArticleListItem extends SimpleItem
{
	public const TYPE = TYPE_CMS_ARTICLE;

    public function getData() {
        return $this->itemSource->getData();
    }
}

