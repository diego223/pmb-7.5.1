<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ItemWatchListItem.php,v 1.1.2.2 2023/03/08 08:34:39 jparis Exp $

namespace Pmb\DSI\Models\Item\Entities\ItemWatch\ItemWatchListItem;

use Pmb\DSI\Models\Item\SimpleItem;

class ItemWatchListItem extends SimpleItem
{
	public const TYPE = TYPE_DOCWATCH;

    public function getData() {
        return $this->itemSource->getData();
    }
}

