<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RecordListItem.php,v 1.5 2023/02/20 16:11:07 jparis Exp $

namespace Pmb\DSI\Models\Item\Entities\Record\RecordListItem;

use Pmb\DSI\Models\Item\SimpleItem;

class RecordListItem extends SimpleItem
{
	public const TYPE = TYPE_NOTICE;
	
	public function __construct(int $id = 0)
	{
		parent::__construct($id);
	}
    public function getData() {
        return $this->itemSource->getData();
    }
}

