<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: Item.php,v 1.3 2022/12/02 13:42:37 jparis Exp $

namespace Pmb\DSI\Models\Item;

interface Item
{
    public function getData();
}

