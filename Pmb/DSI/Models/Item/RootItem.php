<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootItem.php,v 1.15 2023/02/22 16:04:07 qvarin Exp $

namespace Pmb\DSI\Models\Item;

use Exception;
use Pmb\Common\Helper\Helper;
use Pmb\DSI\Models\Item\Aggregator\AggregatorItem;
use Pmb\DSI\Models\Root;
use Pmb\DSI\Orm\ItemOrm;

class RootItem extends Root implements Item
{
	protected const EXCLUDED_PROPERTIES = array(
		"idItem",
		'numModel',
		'model'
	);
	
	public const TAG_TYPE = 6;
	
	protected $ormName = "Pmb\DSI\Orm\ItemOrm";
	
	public $idItem = 0;
	public $name = "";
	public $type = "";
	public $model = false;
	public $numModel = 0;
	public $settings = "";
	public $childs = [];
    public $tags = [];
	
	// ORM props
	protected $numParent = 0;
	protected $itemParent = null;
	protected $itemSource = null;
	
	public function __construct(int $id = 0)
	{
	    $this->id = $id;
	    $this->read();
	}
	
	public static function getInstance(int $id = 0) 
	{

	    $orm = new ItemOrm($id);
	    if(empty($orm->type)) {
	        $fields["num_parent"] = [
	            'value' =>  $id,
	            'operator' => '='
	        ];
	        $childs = ItemOrm::finds($fields);
	        if(!empty($childs)) {
                return new AggregatorItem($id);
	        }
	    }
	    return SimpleItem::getInstance($id);
	}
	
	public function read()
	{
	    $this->fetchData();
	    $this->fetchChilds();

        if(isset($this->settings->namespace) && class_exists($this->settings->namespace)) {
            $this->itemSource = new $this->settings->namespace($this->settings);
        }
	}
	
	public function check($data): array
    {
	    if (!is_string($data->name)) {
	        return [
	            'error' => true,
	            'errorMessage' => 'msg:data_errors'
	        ];
	    }

/*	    if(!empty($data->name)) {
	        $fields = ['name' => $data->name, 'model' => $data->model];
	        if (!empty($data->id)) {
	            $fields[$this->ormName::$idTableName] = [
	                'value' =>  $data->id,
	                'operator' => '!='
	            ];
	        }
	        $result = $this->ormName::finds($fields);
	        if (!empty($result)) {
	            return [
	                'error' => true,
	                'errorMessage' => 'msg:item_duplicated'
	            ];
	        }
	    }*/

	    return [
	        'error' => false,
	        'errorMessage' => ''
	    ];
	}
	
	public function create()
	{
	    $orm = new $this->ormName();
	    $orm->name = $this->name;
	    $orm->model = $this->model;
	    $orm->type = intval($this->type);
	    $orm->settings = $this->settings;
	    $orm->num_model = $this->numModel;
	    $orm->num_parent = $this->numParent;
	    $orm->save();
	    
	    $this->id = $orm->{$this->ormName::$idTableName};
	    $this->{Helper::camelize($this->ormName::$idTableName)} = $orm->{$this->ormName::$idTableName};
	}
	
	public function update()
	{
	    $orm = new $this->ormName($this->id);
	    $orm->name = $this->name;
	    $orm->model = $this->model;
	    $orm->settings = $this->settings;
	    $orm->type = intval($this->type);
	    $orm->num_model = $this->numModel;
	    $orm->num_parent = $this->numParent;
	    $orm->save();
	}
	
	public function delete(): array
    {
	    try {
            if(!$this->checkBeforeDelete()) {
                return [
                    'error' => true,
                    'errorMessage' => "msg:model_check_use"
                ];
            }

	        $orm = new $this->ormName($this->id);
            if(!empty($orm->childs)) {
                foreach ($orm->childs as $child) {
                    $child->delete();
                }
            }
	        $orm->delete();
	    } catch(Exception $e) {
	        return [
	            'error' => true,
	            'errorMessage' => $e->getMessage()
	        ];
	    }
	    
	    $this->id = 0;
	    $this->{Helper::camelize($orm::$idTableName)} = 0;
	    $this->name = '';
	    $this->model = false;
	    $this->settings = "";
	    $this->type = 0;
	    $this->numModel = 0;
	    $this->numParent = 0;
	    
	    return [
	        'error' => false,
	        'errorMessage' => ''
	    ];
	}
	
	public function setFromForm(object $data)
	{
	    $this->name = $data->name;
	    $this->type = $data->type;
	    $this->model = $data->model;
	    $this->settings = json_encode($data->settings);
	    $this->numModel = $data->numModel;
	    $this->numParent = $data->numParent ?? 0;
	    $this->childs = $data->childs ?? array();
	}
	
	public function fetchChilds()
	{
	    if(0 != $this->id) {
    	    $fields["num_parent"] = [
    	        'value' =>  $this->id,
    	        'operator' => '='
    	    ];

            $result = $this->ormName::finds($fields);
            foreach($result as $child) {
    	        $this->childs[] = RootItem::getInstance($child->id_item);
    	    }
	    }
	}

    public function saveChilds()
    {
        $this->deleteNotFoundChilds();
        foreach($this->childs as $child)
        {
            $childModel = self::getInstance(($child->id !== 0 ? $child->id : 0));

            $child->numParent = $this->id;
            $childModel->setFromForm($child);

            if($child->id != 0) {
                $childModel->update();
            }else {
                $childModel->create();
                $child->id = $childModel->id;
            }

            $childModel->saveChilds();
        }
    }

    private function findChildById(int $id)
    {
        foreach ($this->childs as $child) {
            if ($id == $child->id) {
                return $child;
            }
        }
        return null;
    }
	
	public function deleteNotFoundChilds()
	{
        $fields["num_parent"] = [
            'value' =>  $this->id,
            'operator' => '='
        ];
        $result = $this->ormName::finds($fields);
        foreach($result as $elem) {
            if(empty($this->findChildById($elem->id_item))) {
                $childModel = RootItem::getInstance($elem->id_item);
                $childModel->delete();
            }
        }

	}

	public function getData() {
	}
}