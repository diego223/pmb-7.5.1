<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: SimpleItem.php,v 1.8.2.2 2023/03/08 08:34:40 jparis Exp $

namespace Pmb\DSI\Models\Item;

use Pmb\DSI\Models\Item\Entities\Article\ArticleListItem\ArticleListItem;
use Pmb\DSI\Models\Item\Entities\ItemWatch\ItemWatchListItem\ItemWatchListItem;
use Pmb\DSI\Models\Item\Entities\Record\RecordListItem\RecordListItem;
use Pmb\DSI\Orm\ItemOrm;

class SimpleItem extends RootItem
{
	public static function getInstance(int $id = 0) {
		$orm = new ItemOrm($id);

        switch($orm->type) {
            case TYPE_NOTICE:
                return new RecordListItem($id);
            case TYPE_CMS_ARTICLE:
                return new ArticleListItem($id);
            case TYPE_DOCWATCH:
                return new ItemWatchListItem($id);
        }

		return new SimpleItem($id);
	}
	
	public function getData() {
		// Derived
	}
}

