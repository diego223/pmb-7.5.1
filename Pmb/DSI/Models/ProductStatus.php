<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ProductStatus.php,v 1.2 2022/10/18 13:45:59 qvarin Exp $

namespace Pmb\DSI\Models;

class ProductStatus extends Status
{
	protected $ormName = "Pmb\DSI\Orm\ProductStatusOrm";
}

