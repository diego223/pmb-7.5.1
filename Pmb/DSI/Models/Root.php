<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: Root.php,v 1.13 2023/02/22 16:04:07 qvarin Exp $
namespace Pmb\DSI\Models;

use Pmb\Common\Helper\Helper;
use Pmb\Common\Models\Model;
use Pmb\DSI\Orm\TagOrm;
use Pmb\DSI\Orm\EntitiesTagsOrm;

class Root extends Model implements CRUD, Taggable
{

	/**
	 * Liste des proprietes a ne pas cloner quand on reprend un modele
	 * Une constante EXCLUDED_PROPERTIES peut etre ajoutee dans la classe enfant
	 *
	 * @var array
	 */
	protected const GLOBAL_EXCLUDED_PROPERTIES = array(
		"datafetch",
		"structure",
		"id"
	);

	protected $num_tag = 0;

	protected static $messages = [];

	public function __get(string $prop)
	{
		if (method_exists($this, Helper::camelize("get " . $prop))) {
			return $this->{Helper::camelize("get " . $prop)}();
		} elseif (property_exists($this, $prop)) {
			return $this->{$prop};
		}
		throw new \Exception("Unknown property");
	}

	public function __set(string $prop, $value)
	{
		if (method_exists($this, Helper::camelize("set " . $prop))) {
			return $this->{Helper::camelize("set " . $prop)}($value);
		} elseif (property_exists($this, $prop)) {
			return $this->{$prop} = $value;
		}
		throw new \Exception("Unknown property");
	}

	public static function parseCatalog()
	{
		return DSIParserDirectory::getInstance()->getManifests(__DIR__);
	}

	public static function getAvailableTypes()
	{
		return DSIParserDirectory::getInstance()->getCompatibility(static::class);
	}

	public static function getMessages()
	{
		if (empty(static::$messages[static::class])) {
			static::parseMessages();
		}
		return static::$messages[static::class];
	}

	protected static function getClassPath()
	{
		global $base_path;
		$explode = explode("\\", static::class);
		array_pop($explode);
		return realpath("{$base_path}/" . implode("/", $explode));
	}

	public static function parseMessages()
	{
		global $lang;

		$path = static::getClassPath() . "/messages/{$lang}.xml";
		if (! is_file($path)) {
			static::$messages[static::class] = [];
			return;
		}

		$xmlList = new \XMLlist($path);
		$xmlList->analyser();

		static::$messages[static::class] = $xmlList->table ?? [];
	}

	public function create()
	{}

	public function read()
	{}

	public function update()
	{}

	public function delete()
	{}

	public function getEntityTags()
	{
		$result = array();
		$relatedTags = EntitiesTagsOrm::finds([
			"num_entity" => $this->id,
			"type" => static::TAG_TYPE
		]);
		foreach ($relatedTags as $related) {
			$result[] = new Tag($related->num_tag);
		}
		return $result;
	}

	public function unlinkTag($numTag, $numEntity)
	{
		$result = EntitiesTagsOrm::finds([
			"num_tag" => $numTag,
			"num_entity" => $numEntity,
			"type" => static::TAG_TYPE
		]);
		if (count($result) == 1) {
			$link = $result[0];
			$link->delete();
			return true;
		}
		return false;
	}

	public function linkTag($numTag, $numEntity)
	{
		$result = EntitiesTagsOrm::finds([
			"num_tag" => $numTag,
			"num_entity" => $numEntity,
			"type" => static::TAG_TYPE
		]);
		if (count($result)) {
			return [
				'error' => true,
				'errorMessage' => 'msg:tag_already_linked'
			];
		}

		$link = new EntitiesTagsOrm();
		$link->num_tag = $numTag;
		$link->num_entity = $numEntity;
		$link->type = static::TAG_TYPE;
		$link->save();
		return $link;
	}

	public function fetchData()
	{
		parent::fetchData();
		//Gestion des tags
		$type = static::TAG_TYPE;
		if (isset($type)) {
			$this->tags = $this->getEntityTags();
		}

		//gestion des parametres
		if (! isset($this->settings)) {
			$this->settings = json_decode("{}");
			return;
		}
		if ($this->settings != "") {
			$this->settings = json_decode($this->settings);
		} else {
			$this->settings = json_decode("{}");
		}
		if (! isset($this->numModel)) {
			return;
		}
		//Gestion du verrou des modeles
		if (!empty($this->settings->locked) && $this->settings->locked) {
			if (method_exists(static::class, 'getInstance')) {
				$model = static::getInstance($this->numModel);
			} else {
				$model = new static($this->numModel);
			}
			$reflect = new \ReflectionClass($model);
			$props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);
			$exclude = array_merge(self::GLOBAL_EXCLUDED_PROPERTIES, static::EXCLUDED_PROPERTIES);

			foreach ($props as $prop) {
				if (! in_array($prop->name, $exclude)) {
					$this->{$prop->name} = $model->{$prop->name};
				}
			}
			//Apres copie on repasse le verrou aux parametres
			$this->settings->locked = true;
		}
	}

    public function checkBeforeDelete() {
        // On check si on utilise le mod�le dans d'autres items
        $findModels = $this->ormName::finds([
            "num_model" => $this->id
        ]);

        if(empty($findModels)) {
            return true;
        }

        return false;
    }
}

