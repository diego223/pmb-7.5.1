<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: AllArticleSelector.php,v 1.1.2.1 2023/03/07 14:29:31 jparis Exp $

namespace Pmb\DSI\Models\Selector\Item\Entities\Article\All;

use Pmb\DSI\Models\Selector\SubSelector;

class AllArticleSelector extends SubSelector
{
    public function __construct($selectors){}

    public function getData(): array
    {
        $articles = array();
        $query = "select id_article,if(article_start_date != '0000-00-00 00:00:00',article_start_date,article_creation_date) as publication_date from cms_articles";
        $result = pmb_mysql_query($query);
        if(pmb_mysql_num_rows($result) > 0){
            while($row = pmb_mysql_fetch_object($result)){
                $articles[] = new \cms_article($row->id_article);
            }
        }
        return $articles;
    }
}

