<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ParentSectionSelector.php,v 1.1.2.2 2023/03/08 08:34:40 jparis Exp $

namespace Pmb\DSI\Models\Selector\Item\Entities\Article\ParentSection;

use Pmb\DSI\Models\Selector\SubSelector;

class ParentSectionSelector extends SubSelector
{
    protected $data = [];

    public function __construct($selectors){
        if(!empty($selectors->data)) {
            $this->data = $selectors->data;
        }
    }

    public function getData(): array
    {
        $articles = array();
        $query = "SELECT id_article, if(article_start_date != '0000-00-00 00:00:00',article_start_date,article_creation_date)
                as publication_date from cms_articles WHERE num_section = " . intval($this->data->sectionId);
        $result = pmb_mysql_query($query);
        if(pmb_mysql_num_rows($result)){
            while($row = pmb_mysql_fetch_object($result)){
                $articles[] = new \cms_article($row->id_article);
            }
        }
        return $articles;
    }
}

