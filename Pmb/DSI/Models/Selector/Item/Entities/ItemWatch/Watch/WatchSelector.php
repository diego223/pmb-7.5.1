<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: WatchSelector.php,v 1.1.2.2 2023/03/08 08:34:39 jparis Exp $

namespace Pmb\DSI\Models\Selector\Item\Entities\ItemWatch\Watch;

use Pmb\DSI\Models\Selector\SubSelector;

class WatchSelector extends SubSelector
{
    protected $data = [];

    public function __construct($selectors){
        if(!empty($selectors->data)) {
            $this->data = $selectors->data;
        }
    }

    public function getData(): array
    {
        $itemsWatch = array();
        $query = "SELECT id_item FROM docwatch_items WHERE item_num_watch = " . intval($this->data->watchId);
        $result = pmb_mysql_query($query);
        if(pmb_mysql_num_rows($result)){
            while($row = pmb_mysql_fetch_object($result)){
                $itemsWatch[] = (new \docwatch_item($row->id_item))->get_normalized_item();
            }
        }
        return $itemsWatch;
    }
}

