<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RecordRMCSelector.php,v 1.5.2.1 2023/03/07 14:29:30 jparis Exp $

namespace Pmb\DSI\Models\Selector\Item\Entities\Record\RMC;

use Pmb\DSI\Models\Selector\SubSelector;
use search;
use searcher_records;
use notice;

class RecordRMCSelector extends SubSelector
{
    public $selector = null;
    public $data = []; 

    public function __construct($selectors)
    {
        if(!empty($selectors)) {
            $this->data = $selectors->data ?? [];
        }
    }

    public function getData() {
        if(empty($this->data) && !is_null($this->selector)) {
            return $this->selector->getData();
        }
        return $this->getRMCResult();
    }

    protected function getRMCResult() {
        $records = array();
        if(!isset($this->data->search_serialize)) {
            return $records;
        }
        
        $search = new search();
        $search->unserialize_search($this->data->search_serialize);

        $table = $search->make_search();
        $query = "select notice_id from ".$table;
        $result = pmb_mysql_query($query);
        if(pmb_mysql_num_rows($result)){
            while($row = pmb_mysql_fetch_object($result)){
                $records[$row->notice_id] = notice::get_notice_title($row->notice_id);
            }
        }
        
        return $records;
    }
}

