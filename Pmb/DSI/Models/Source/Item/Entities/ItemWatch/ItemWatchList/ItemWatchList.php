<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ItemWatchList.php,v 1.1.2.2 2023/03/08 08:34:39 jparis Exp $

namespace Pmb\DSI\Models\Source\Item\Entities\ItemWatch\ItemWatchList;

use Pmb\DSI\Models\Source\Item\ItemSource;

class ItemWatchList extends ItemSource
{
    public $selector = null;

    public function __construct($selectors)
    {
        if(!empty($selectors->selector->namespace)) {
            $this->selector = new $selectors->selector->namespace($selectors->selector);
        }
    }

    public function getData() {
        return $this->selector->getData();
    }
}

