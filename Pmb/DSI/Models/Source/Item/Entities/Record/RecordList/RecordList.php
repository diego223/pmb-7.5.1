<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RecordList.php,v 1.5 2023/02/20 16:11:07 jparis Exp $

namespace Pmb\DSI\Models\Source\Item\Entities\Record\RecordList;

use Pmb\DSI\Models\Source\Item\ItemSource;

class RecordList extends ItemSource
{
    public $selector = null;

    public function __construct(\stdClass $selectors)
    {
        if(!empty($selectors->selector->namespace)) {
            $this->selector = new $selectors->selector->namespace($selectors->selector);
        }
    }

    public function getData() {
        return $this->selector->getData();
    }
}

