<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: EmprList.php,v 1.3 2022/12/23 09:57:25 rtigero Exp $

namespace Pmb\DSI\Models\Source\Subscriber\Entities\Empr;

use Pmb\DSI\Models\Source\Subscriber\SubscribersSource;

class EmprList extends SubscribersSource
{
	public function getData()
	{
		return $this->selector->getData();
	}
}

