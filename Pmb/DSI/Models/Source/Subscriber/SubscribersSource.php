<?php
namespace Pmb\DSI\Models\Source\Subscriber;

use Pmb\DSI\Models\Source\RootSource;

class SubscribersSource extends RootSource
{
	protected $selector = null;
	
	public function __construct($selector)
	{
		if(class_exists($selector->namespace)) {
			$this->selector = new $selector->namespace($selector->data);			
		}
	}
	
	public function getData()
	{
		return array();
	}
}

