<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: DiffusionSubscriberList.php,v 1.5 2023/02/08 14:52:29 rtigero Exp $

namespace Pmb\DSI\Models\SubscriberList;

use Pmb\DSI\Models\SubscriberList\Subscribers\Subscriber;
use Pmb\DSI\Models\Diffusion;
use Pmb\DSI\Orm\DiffusionOrm;
use Pmb\DSI\Models\Channel\RootChannel;

class DiffusionSubscriberList extends LocalSubscriberList
{	
	protected $ormName = "Pmb\DSI\Orm\SubscribersDiffusionOrm";
	
	protected $entityOrmName = "Pmb\DSI\Orm\DiffusionOrm";
	
	protected $numDiffusion = 0;
	
	protected function getSubscribers()
	{
		$result = array();
		$lists = $this->ormName::finds(['num_diffusion' => $this->numEntity]);
		foreach($lists as $list) {
			$this->subscribers[] = Subscriber::getInstance("diffusions", $list->{$this->ormName::$idTableName});
		}
		
		$this->filterSubscribers();
		
		return $result;
	}
	
	protected function filterSubscribers()
	{
		//Recuperation des contraintes du channel
		$entity = $this->entityOrmName::findById($this->numEntity);
		if (! $entity->num_channel) {
			return;
		}
		$channel = RootChannel::getInstance($entity->num_channel);
		
		if (! $channel::CHANNEL_REQUIREMENTS || ! isset($channel::CHANNEL_REQUIREMENTS['subscribers'])) {
			return;
		}
		$requirements = array_keys($channel::CHANNEL_REQUIREMENTS['subscribers']);
		for ($i = 0; $i < count($this->subscribers); $i ++) {
			//On ne filtre pas les unsubscribers
			if($this->subscribers[$i]->updateType == Subscriber::UPDATE_TYPE_UNSUBSCRIBER) {
				continue;
			}
			$validSettings = 0;
			foreach ($this->subscribers[$i]->settings as $setting => $value) {
				if ((in_array($setting, $requirements)) && ! empty($value)) {
					$validSettings ++;
				}
			}
			if ($validSettings < count($requirements)) {
				array_splice($this->subscribers, $i, 1);
			}
		}
	}
}