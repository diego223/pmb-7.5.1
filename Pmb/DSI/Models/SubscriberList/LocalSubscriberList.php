<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: LocalSubscriberList.php,v 1.5 2023/02/08 14:52:29 rtigero Exp $
namespace Pmb\DSI\Models\SubscriberList;

use Pmb\DSI\Models\SubscriberList\Subscribers\Subscriber;
use Pmb\DSI\Models\Channel\RootChannel;

class LocalSubscriberList extends RootSubscriberList
{

	public $numEntity = 0;

	public $subscribers = array();

	public function __construct(int $numEntity = 0)
	{
		$this->numEntity = $numEntity;
		$this->read();
	}

	public function read()
	{
		$this->getSubscribers();
	}

	protected function getSubscribers()
	{
		return array();
	}

	public function getUnsubscribers(SourceSubscriberList $source)
	{
		$result = array();
		foreach ($this->subscribers as $subscriber) {
			if ($subscriber->type == static::SUBSCRIBER_TYPE_SOURCE && $subscriber->updateType == Subscriber::UPDATE_TYPE_UNSUBSCRIBER) {
				$result[] = $subscriber;
			}
		}
		return $result;
	}

	protected function filterSubscribers()
	{}
}

