<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ProductSubscriberList.php,v 1.6 2023/02/08 14:52:29 rtigero Exp $

namespace Pmb\DSI\Models\SubscriberList;

use Pmb\DSI\Models\SubscriberList\Subscribers\Subscriber;

class ProductSubscriberList extends LocalSubscriberList
{
	protected $ormName = "Pmb\DSI\Orm\SubscribersProductOrm";
	
	protected $entityOrmName = "Pmb\DSI\Orm\ProductOrm";
	
	public $numProduct = 0;
	
	protected function getSubscribers()
	{
		$result = array();
		$lists = $this->ormName::finds(['num_product' => $this->numEntity]);
		foreach($lists as $list) {
			$this->subscribers[] = Subscriber::getInstance("products", $list->{$this->ormName::$idTableName});
		}
		$this->filterSubscribers();
		
		return $result;
	}
}