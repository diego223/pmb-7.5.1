<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootSubscriberList.php,v 1.16 2023/02/08 14:52:29 rtigero Exp $
namespace Pmb\DSI\Models\SubscriberList;

use Pmb\DSI\Models\Root;
use Pmb\DSI\Models\Channel\Channel;
use Pmb\DSI\Orm\SubscriberListContentOrm;
use Pmb\DSI\Models\SubscriberList\Subscribers\Subscriber;

class RootSubscriberList extends Root
{

	public const SUBSCRIBER_LIST_TYPE_LOCAL = 1;
	public const SUBSCRIBER_LIST_TYPE_SOURCE = 2;
	
	public const SUBSCRIBER_TYPE_MANUAL = 1;
	public const SUBSCRIBER_TYPE_SOURCE = 2;
	public const SUBSCRIBER_TYPE_IMPORT = 3;
	
	protected function __construct()
	{
	}

	public function check($data)
	{
		return true;
	}
	
	public static function getDiffusionSubscribers(int $idDiffusion, int $idSubscriberList)
	{
		$subscribers = array();
		$source = new SourceSubscriberList($idSubscriberList);
		$subscribers['source'] = $source;
		$subscribers['lists'] = new DiffusionSubscriberList($idDiffusion);
		$subscribers['source']->filterSource($subscribers['lists']);
		return $subscribers;
	}
	
	public static function getProductSubscribers(int $idProduct, int $idSubscriberList)
	{
		$subscribers = array();
		$source = new SourceSubscriberList($idSubscriberList);
		$subscribers['source'] = $source;
		$subscribers['lists'] = new ProductSubscriberList($idProduct);
		$subscribers['source']->filterSource($subscribers['lists']);
		return $subscribers;
	}
	
	public static function getSubscriberList(int $idSubscriberList = 0) 
	{
		$subscribers = array();
		$source = new SourceSubscriberList($idSubscriberList);
		$subscribers['source'] = $source;
		$subscribers['lists'] = $source->getSubscribersFromDatabase();
		$subscribers['source']->filterSource($subscribers['lists']);
		return $subscribers;
		return new SourceSubscriberList($idSubscriberList);
	}
	
	public static function getSourceSubscriberList(int $idSubscriberList = 0) 
	{
		return new SourceSubscriberList($idSubscriberList);
	}

	/**
	 * Filtre la liste d'abonnes selon le canal passe en parametre
	 *
	 * @param Channel $channel
	 * @return boolean
	 */
	public function filterList(array $requirements = array())
	{
		$filteredSubscribers = array();
		$requirements = array_keys($requirements);
		foreach ($this->subscribers as $subscriber) {
			foreach ($requirements as $requirement) {
				if (isset($subscriber->settings->$requirement) && ! empty($subscriber->settings->$requirement)) {
					$filteredSubscribers[] = $subscriber;
				}
			}
		}
		$this->subscribers = $filteredSubscribers;
	}

	protected function fetchSubscribers()
	{
		$this->subscribers = array();
	}
	
	/**
	 * Permet de comparer deux abonnes
	 * @param Subscriber $subscriber
	 * @param Subscriber $otherSubscriber
	 * @return boolean
	 */
	public static function isSameSubscriber(Subscriber $subscriber, Subscriber $otherSubscriber)
	{
		if(($subscriber->name != $otherSubscriber->name) || ($subscriber->settings != $otherSubscriber->settings)) {
			return false;
		}
		return true;
	}
}

