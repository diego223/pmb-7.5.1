<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: DiffusionSubscriber.php,v 1.3 2023/02/15 10:26:18 rtigero Exp $
namespace Pmb\DSI\Models\SubscriberList\Subscribers;

use Pmb\DSI\Models\CRUD;
use Pmb\Common\Helper\Helper;

class DiffusionSubscriber extends Subscriber implements SubscriberDiffusion
{
	
	public const DEDUPLICATION_FIELDS = [
		"email"
	];
	
	protected $ormName = "Pmb\DSI\Orm\SubscribersDiffusionOrm";
	
	public $numDiffusion = 0;
	
	public function create()
	{
		$orm = new $this->ormName();
		$orm->name = $this->name;
		$orm->settings = json_encode($this->settings);
		$orm->type = $this->type;
		$orm->update_type = $this->updateType;
		$orm->num_diffusion = $this->numDiffusion;
		$orm->save();
		
		$this->id = $orm->{$this->ormName::$idTableName};
		$this->{Helper::camelize($this->ormName::$idTableName)} = $orm->{$this->ormName::$idTableName};
	}
	
	public function setFromForm($data)
	{
		$this->name = $data->name;
		$this->settings = $data->settings;
		$this->updateType = $data->updateType ?? "";
		$this->type = $data->type;
		$this->numDiffusion = $data->numDiffusion;
	}
	
	public function update()
	{
		$orm = new $this->ormName($this->id);
		$orm->name = $this->name;
		$orm->settings = json_encode($this->settings);
		$orm->type = $this->type;
		$orm->update_type = $this->updateType;
		$orm->num_diffusion = $this->numDiffusion;
		$orm->save();
	}
	public function getIdSubscriber()
	{
		return $this->id;
	}
	public function getName()
	{
		return $this->name;
	}
	public function getStatus()
	{
		return $this->updateType;
	}
	
	public function setEntity(int $entityId)
	{
		$this->numDiffusion = $entityId;
	}
	
	public function emptySubscribers()
	{
		$subscribers = $this->ormName::finds([
			"num_diffusion" => $this->numDiffusion
		]);
		foreach($subscribers as $subscriber) {
			$subscriber->delete();
		}
		return array();
	}
}