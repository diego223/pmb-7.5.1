<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: Subscriber.php,v 1.2 2023/02/08 14:52:29 rtigero Exp $
namespace Pmb\DSI\Models\SubscriberList\Subscribers;

use Pmb\Common\Models\Model;
use Pmb\DSI\Models\CRUD;
use Pmb\Common\Helper\Helper;

class Subscriber extends Model implements CRUD
{

	public const FROM_DIFFUSION = "diffusions";

	public const FROM_PRODUCT = "products";

	public const FROM_SUBSCRIBER_LIST = "subscriber_list";

	public const UPDATE_TYPE_UNSUBSCRIBER = 1;
	
	public const DEDUPLICATION_FIELDS = array();

	public $idSubscriber = 0;

	public $name;

	public $settings;

	public $type;

	public $updateType = 0;

	public static function getInstance(string $type = "", int $id = 0)
	{
		switch ($type) {
			case self::FROM_DIFFUSION:
				return new DiffusionSubscriber($id);
			case self::FROM_PRODUCT:
				return new ProductSubscriber($id);
			case self::FROM_SUBSCRIBER_LIST:
				return new SubscriberListSubscriber($id);
			default:
				return new self($id);
		}
	}

	protected function __construct(int $id = 0)
	{
		$this->id = $id;
		$this->read();
	}

	public function create()
	{}

	public function update()
	{}

	public function read()
	{
		$this->fetchData();
		if ($this->settings != "") {
			$this->settings = json_decode($this->settings);
		} else {
			$this->settings = json_decode("{}");
		}
	}

	public function delete()
	{
		try {
			$orm = new $this->ormName($this->id);
			$orm->delete();
		} catch (\Exception $e) {
			return [
				'error' => true,
				'errorMessage' => $e->getMessage()
			];
		}

		$this->id = 0;
		$this->{Helper::camelize($orm::$idTableName)} = 0;
		$this->name = "";
		$this->updateType = 0;
		$this->status = '';

		return [
			'error' => false,
			'errorMessage' => ''
		];
	}

	public function getIdSubscriber()
	{
		return 0;
	}

	public function getName()
	{
		return "";
	}

	public function getStatus()
	{
		return true;
	}

	public function save()
	{}

	public function check($data)
	{
		$fields = array();

		foreach (static::DEDUPLICATION_FIELDS as $field) {
			switch (true) {
				case isset($data->$field):
					$fields[$field] = [
						"value" => $data->$field,
						"operator" => "="
					];
					break;
				case isset($data->settings->$field):
					if (! isset($fields["settings"])) {
						$fields['settings'] = array();
					}
					$fields['settings'][] = [
						"value" => '%"' . $field . '":"' . $data->settings->$field . '"%',
						"operator" => "LIKE"
					];
					break;
				default:
					break;
			}
		}
		if (! count($fields)) {
			return;
		}
		$result = $this->ormName::finds($fields);

		if (! empty($result)) {
			return [
				'error' => true,
				'errorMessage' => 'msg:subscriber_duplicated'
			];
		}
	}

	public function unsubscribe()
	{
		$this->updateType = self::UPDATE_TYPE_UNSUBSCRIBER;
	}
	public function subscribe()
	{
		$subscriber = clone $this;
		if($this->id) {
			$delete = $this->delete();
			if(! $delete['error']) {
				$subscriber->id = 0;
				$subscriber->updateType = 0;
				return $subscriber;
			}
		}
		return [
			'error' => true,
			'errorMessage' => 'msg:subscriber_duplicated'
		];
	}
}

