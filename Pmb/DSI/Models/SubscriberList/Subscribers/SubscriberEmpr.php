<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: SubscriberEmpr.php,v 1.1 2023/02/07 10:51:15 rtigero Exp $
namespace Pmb\DSI\Models\SubscriberList\Subscribers;

class SubscriberEmpr extends Subscriber
{

	public const DEDUPLICATION_FIELDS = [
		"name",
		"idEmpr",
		"email"
	];
	
	public $name = "";

	public $type;

	public $settings = null;

	public function __construct()
	{
// 		$this->type = static::TYPE_EMPR;
		if (! isset($this->settings)) {
			$this->settings = json_decode("{}");
		}
	}

	public function getName()
	{
		return $this->name;
	}
}