<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: DjangoView.php,v 1.3.2.2 2023/03/08 08:34:39 jparis Exp $

namespace Pmb\DSI\Models\View\DjangoView;

use Pmb\DSI\Models\View\RootView;
use Pmb\DSI\Models\Item\Item;
use Pmb\Common\Helper\HelperEntities;

class DjangoView extends RootView
{	
    protected const TEMPLATES_LOCATION = "./opac_css/includes/templates/";
    
    protected $entityNamespace;
    protected $entityPluralNamespace;
    protected $html;
    protected $templatePath;
    
    public function render(Item $item, int $entityId, int $limit, string $context)
    {   
        if(isset($this->html)) {
            return $this->html;
        }
        if(!isset($this->settings->entityType) || ! isset($this->settings->templateDirectory) || !isset($this->settings->html)) {
            return "";
        }
        $this->setData();
        $data = $item->getData();
        
        if(empty($data)) {
            return "";
        }

        $this->filterData($data, $entityId);
        $this->limitData($data, $limit);
        $this->formatData($data, $item->type);

        file_put_contents($this->templatePath, $this->settings->html);

        $this->html = "";
        if(!empty($this->templatePath)) {
            $h2o = \H2o_collection::get_instance($this->templatePath);
            $this->html .= $h2o->render(array($this->entityPluralNamespace => $data));
            $this->html .= '<link rel="stylesheet" type="text/css" href="./opac_css/styles/'.$this->settings->templateDirectory.'/record_display.css">';
        }
        return $this->html;
    }
    
    protected function formatData(&$data, $type)
    {
        if($type === TYPE_NOTICE) {
            $h2o = \H2o_collection::get_instance($this->getTemplate());
            foreach($data as $id => $title) {
                $data[$id] = [];
                $data[$id]['object'] = new \record_datas($id);
                $data[$id]['content'] = $h2o->render(array($this->entityNamespace => $data[$id]['object']));
            }
        }
    }
    
    private function setData()
    {
        global $base_path;
        
        $this->entityNamespace = HelperEntities::get_entities_namespace()[$this->settings->entityType];
        $this->entityPluralNamespace = HelperEntities::get_entities_namespace_plural()[$this->settings->entityType];
        $this->templatePath = $base_path.'/temp/'.LOCATION.'_dsi_view_django_template_content_'.$this->id;
    }
    
    protected function getTemplate()
    {
        $path = self::TEMPLATES_LOCATION;
        if($this->settings->entityType == TYPE_NOTICE) {
            $path .= $this->entityNamespace . '/' . $this->settings->templateDirectory . '/'.$this->entityNamespace.'_in_result_display.tpl.html';
            return $path;
        }
        $path .= "authorities/" . $this->settings->templateDirectory . "/" . $this->entityNamespace . ".html";
        return $path;
    }
}

