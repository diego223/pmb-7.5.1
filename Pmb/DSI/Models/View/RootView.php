<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootView.php,v 1.21 2023/02/22 16:04:07 qvarin Exp $

namespace Pmb\DSI\Models\View;

use Pmb\Common\Helper\Helper;
use Pmb\DSI\Models\Root;
use Pmb\DSI\Orm\ViewOrm;
use Pmb\DSI\Models\Item\Item;

class RootView extends Root
{
	protected const EXCLUDED_PROPERTIES = array(
		"idView",
		"viewModel",
		'numModel',
		'model'
	);
	
	public const TAG_TYPE = 3;
	
	public const IDS_TYPE = [
        "Pmb\DSI\Models\View\DjangoView\DjangoView" => 1,
        "Pmb\DSI\Models\View\WYSIWYGView\WYSIWYGView" => 2,
        "Pmb\DSI\Models\View\SimpleView\SimpleView" => 3,
    ];

    protected $ormName = "Pmb\DSI\Orm\ViewOrm";

    public $id = 0;
    public $name = "";
    public $type = 0;
    public $model = false;
    public $settings = "";
    public $childs = array();
    public $tags = null;
    
    // ORM props
    protected $idView = 0;
    
    public $numModel = 0;
    protected $numParent = 0;

    protected $viewModel = null;
    protected $parentView = null;

    public function __construct(int $id = 0) {
        $this->id = $id;
		$this->read();
    }

    public static function getInstance(int $id = 0) {
        if(!empty($id)) {
            $view = ViewOrm::findById($id);
            if(!empty($view)) {
                foreach(self::IDS_TYPE as $key => $type) {
                    if(self::IDS_TYPE[$key] == $view->type) {
                        return new $key($id);
                    }
                }
            }
        }
        return new RootView($id);
    }

    public function read()
	{
		$this->fetchData();
		$this->fetchChilds();
	}

	public function check(object $data) {
		if (!is_string($data->name)) {
			return [
				'error' => true,
				'errorMessage' => 'msg:data_errors'
			];
		}
		
/*		if(!empty($data->name)) {
			$fields = ['name' => $data->name, 'model' => $data->model];
			if (!empty($data->id)) {
				$fields[$this->ormName::$idTableName] = [
					'value' =>  $data->id,
					'operator' => '!='
				];
			}
			$result = $this->ormName::finds($fields);
			if (!empty($result)) {
				return [
					'error' => true,
					'errorMessage' => 'msg:item_duplicated'
				];
			}
		}*/
		
		return [
			'error' => false,
			'errorMessage' => ''
		];
	}

    public function create()
	{
        $orm = new $this->ormName();
		$orm->name = $this->name;
		$orm->type = $this->type;
		$orm->model = $this->model;
		$orm->settings = json_encode($this->settings);
		$orm->num_model = $this->numModel;
		$orm->num_parent = $this->numParent;
		$orm->save();
		
		$this->id = $orm->{$this->ormName::$idTableName};
		$this->{Helper::camelize($this->ormName::$idTableName)} = $orm->{$this->ormName::$idTableName};
    }
	
	public function update()
	{
		$orm = new $this->ormName($this->id);
		$orm->name = $this->name;
		$orm->type = $this->type;
		$orm->model = $this->model;
		$orm->settings = json_encode($this->settings);
		$orm->num_model = $this->numModel;
		$orm->num_parent = $this->numParent;
		$orm->save();
	}

	public function delete()
	{
		try {
            if(!$this->checkBeforeDelete()) {
                return [
                    'error' => true,
                    'errorMessage' => "msg:model_check_use"
                ];
            }

			$this->deleteChilds();
			$orm = new $this->ormName($this->id);
			$orm->delete();

		} catch(\Exception $e) {
			return [
				'error' => true,
				'errorMessage' => $e->getMessage()
			];
		}
		
		$this->id = 0;
		$this->{Helper::camelize($orm::$idTableName)} = 0;
		$this->name = '';
		$this->type = '';
		$this->model = false;
		$this->settings = [];
		$this->numModel = null;
		$this->numParent = null;
		
		return [
			'error' => false,
			'errorMessage' => ''
		];
	}

    public function setFromForm(object $data)
	{
		$this->name = $data->name;
		$this->type = intval($data->type);
		$this->model = $data->model;
		$this->settings = $data->settings;
		$this->numModel = $data->numModel;
		$this->numParent = $data->numParent ?? 0;
		$this->childs = $data->childs ?? array();
		
	}
	
	public function fetchChilds()
	{
		if($this->id == 0) {
			return;
		}
		$fields["num_parent"] = [
			'value' =>  $this->id,
			'operator' => '='
		];
		$result = $this->ormName::finds($fields);
		foreach($result as $child) {
			$this->childs[] = RootView::getInstance($child->id_view);
		}
	}
	
	public function saveChilds()
	{
		foreach($this->childs as $child)
		{
			$childModel = self::getInstance($child->id);
			$child->numParent = $this->id;
			$childModel->setFromForm($child);
			
			if (0 == $child->id) {
				$childModel->create();
			} else {
				$childModel->update();
			}
		}
	}
	
	public function deleteChilds()
	{
		foreach($this->childs as $child)
		{
			$childModel = self::getInstance($child->id);
			$childModel->delete();
		}
	}
	
	public function render(Item $item)
	{
	}
}