<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: SimpleView.php,v 1.4 2023/02/16 15:30:15 jparis Exp $
namespace Pmb\DSI\Models\View\SimpleView;

use Pmb\DSI\Models\View\RootView;
use Pmb\DSI\Models\Item\Item;
use Pmb\Common\Helper\HelperEntities;

class SimpleView extends RootView
{
	protected const TEMPLATES_LOCATION = "./opac_css/includes/templates/";
	protected $entityNamespace;
	protected $html;
	
	public function render(Item $item)
	{
		if(isset($this->html)) {
			return $this->html;
		}
		if(!isset($this->settings->entityType) || ! isset($this->settings->templateDirectory)) {
			return "";
		}
		$this->setData();
		$data  = $item->getData();
		$this->formatData($data);
		$template = $this->getTemplate();
		$this->html = "";
		if(is_file($template)) {
			$h2o = \H2o_collection::get_instance($template);
			foreach($data as $element) {
				$this->html .= $h2o->render(array($this->entityNamespace => $element));
				$this->html .= "<hr>";
			}
			$this->html .= '<link rel="stylesheet" type="text/css" href="./opac_css/styles/'.$this->settings->templateDirectory.'/record_display.css">';
		}
		return $this->html;
	}
	
	protected function getTemplate()
	{
		$path = self::TEMPLATES_LOCATION;
		if($this->settings->entityType == TYPE_NOTICE) {
			$path .= $this->entityNamespace . '/' . $this->settings->templateDirectory . '/'.$this->entityNamespace.'_in_result_display.tpl.html';
			return $path;
		}
		$path .= "authorities/" . $this->settings->templateDirectory . "/" . $this->entityNamespace . ".html";
		return $path;
		
	}
	
	protected function formatData(&$data)
	{
		foreach($data as $id => $title) {
			$data[$id] = new \record_datas($id);
		}
	}
	
	private function setData()
	{
		$this->entityNamespace = HelperEntities::get_entities_namespace()[$this->settings->entityType];
	}
}

