<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ItemOrm.php,v 1.9 2023/02/08 13:37:31 jparis Exp $
namespace Pmb\DSI\Orm;

use Pmb\Common\Orm\Orm;

class ItemOrm extends Orm
{

	/**
	 * Table name
	 *
	 * @var string
	 */
	public static $tableName = "dsi_item";

	/**
	 * Primary Key
	 *
	 * @var string
	 */
	public static $idTableName = "id_item";

	/**
	 *
	 * @var integer
	 */
	protected $id_item = 0;
	
	/**
	 *
	 * @var string
	 */
	protected $name = "";
	
	/**
	 *
	 * @var boolean
	 */
	protected $model = false;

	/**
	 *
	 * @var string
	 */
	protected $settings = "";

	/**
	 *
	 * @var integer
	 */
	protected $type = 0;

	/**
	 *
	 * @var integer
	 */
	protected $num_model = 0;

	/**
	 *
	 * @var integer
	 */
	protected $num_parent = 0;

	/**
	 *
	 * @Relation 0n
	 * @Orm Pmb\DSI\Orm\ItemOrm
	 * @RelatedKey num_parent
	 */
	protected $item_parent = null;

    public function checkBeforeDelete()
    {
        $fields["num_parent"] = [
            'value' =>  $this->id_item,
            'operator' => '='
        ];
        $result = $this::finds($fields);
        foreach($result as $child) {
            $child->delete();
        }

        return true;
    }

    /**
	 *
	 * @var \ReflectionClass
	 */
	protected static $reflectionClass = null;
	
	protected static $relations = array();
}