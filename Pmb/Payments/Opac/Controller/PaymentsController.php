<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: PaymentsController.php,v 1.4 2023/01/26 15:29:45 qvarin Exp $
namespace Pmb\Payments\Opac\Controller;

use Pmb\Payments\Opac\Views\PaymentsView;

class PaymentsController
{

    /**
     *
     * @var string
     */
    public $action = "list";

    public function proceed(string $action = "", $data = null)
    {
        $this->action = $action;
        switch ($action) {
            default:
            case "list":
                return $this->listPaymentsAction();
                break;
        }
    }

    public function listPaymentsAction()
    {
        global $pmb_gestion_devise;
        $accounts = array(
            "accounts" => [
                [
                    "label" => "Compte abonnement",
                    "id" => 1,
                    "sold" => 5,
                    "transacash" => [
                        [
                            "label" => "Paiement Abo",
                            "sold" => 5
                        ]
                    ]
                ],
                [
                    "label" => "Compte amende",
                    "id" => 3,
                    "sold" => -15,
                    "transacash" => [
                        [
                            "label" => "Exemplaire perdu",
                            "sold" => -15
                        ]
                    ]
                ],
                [
                    "label" => "Autres comptes",
                    "id" => 4,
                    "sold" => -30,
                    "transacash" => [
                        [
                            "label" => "Carte perdue",
                            "sold" => -15
                        ],
                        [
                            "label" => "Carte perdue",
                            "sold" => -15
                        ]
                    ]
                ]
            ],
            "sold" => -45
        );
        $newVue = new PaymentsView("payments/payments", [
            "accounts" => $accounts["accounts"],
            "sold" => $accounts["sold"],
            "action" => $this->action,
            "devise" => $pmb_gestion_devise
        ]);
        print $newVue->render();
    }
}