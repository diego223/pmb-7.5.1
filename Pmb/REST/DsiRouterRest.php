<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: DsiRouterRest.php,v 1.33.2.3 2023/03/08 16:04:59 qvarin Exp $
namespace Pmb\REST;

use Pmb\Common\Helper\Helper;

class DsiRouterRest extends RouterRest
{

    protected const CONTROLLER = "\\Pmb\\DSI\\Controller\\DsiApiController";
    

    protected function generateRoutes()
    {
        // Diffusion
        $this->post('{controller}/save', 'save');
        $this->post('{controller}/delete', 'delete');
        $this->post('{controller}/deleteProductDiffusion', 'deleteProductDiffusion');
        $this->post('{controller}/deleteEventDiffusion', 'deleteEventDiffusion');
        $this->post('{controller}/deleteEventProduct', 'deleteEventProduct');
        $this->post('{controller}/search', 'search');
        $this->post('{controller}/addSubscriber/{idSubscriberList}', 'addSubscriber');
        $this->post('{controller}/removeSubscriberFromList/{idSubscriberList}', 'removeSubscriberFromList');
        $this->post('{controller}/{entityType}/importSubscribers/{entityId}', 'importSubscribers');
        $this->post('{controller}/getSubscribersFromList', 'getSubscribersFromList');
        $this->post('{controller}/unlinkTag', 'unlinkTag');
        $this->post('{controller}/linkTag', 'linkTag');

        $this->get('{controller}/getSourceList/{id_type}', 'getSourceList')->with("id_type", static::LIMIT_NUMBER);
        $this->get('{controller}/getSelectorList/{namespace}', 'getSelectorList');
        $this->get('{controller}/getMailList/', 'getMailList');
        $this->get('{controller}/getEntitiesDefaultTemplates/', 'getEntitiesDefaultTemplates');
        $this->get('{controller}/getEntityTree/{id_type}', 'getEntityTree');
        $this->get('{controller}/getTemplateDirectories/{entityType}', 'getTemplateDirectories');
        $this->get('{controller}/getSources/{type}', 'getSources');
        $this->get('{controller}/getSelectors/{type}', 'getSelectors');
        $this->get('{controller}/haveSubSelector/{namespace}', 'haveSubSelector');
        $this->get('{controller}/getCompatibility/{type}', 'getCompatibility');
        
        $this->get('{controller}/getEntityList/', 'getEntityList');
        $this->get('{controller}/getTypeListAjax/', 'getTypeListAjax');
        
        $this->get('{controller}/getEntity/{idEntity}', 'getEntity');
        $this->get('{controller}/getEntity', 'getEntity');
        $this->get('{controller}/getTypes/', 'getTypes');
        $this->get('{controller}/getModels/', 'getModels');
        $this->get('{controller}/getItems/', 'getItems');
        $this->get('{controller}/getViews/', 'getViews');

        $this->get('{controller}/getModel/{idModel}', 'getModel');
        $this->get('{controller}/render/{idEntity}', 'renderView');
        $this->get('{controller}/render/{idView}/{idItem}/{limit}/{context}', 'renderView');
        
        $this->get('{controller}/availableFilters/{idItem}', 'availableFilters');

        $this->get('{controller}/getEmptyInstance/', 'getEmptyInstance');
        $this->get('{controller}/requirements/{type}', 'getRequirements');
        $this->get('{controller}/filterSubscribers/{idSubscriberList}/{channelType}', 'filterSubscribers');
        $this->get('{controller}/tags', 'getTags');
        $this->get('{controller}/getRelatedEntities/{numTag}', 'getRelatedEntities');
        $this->get('{controller}/{entityType}/getEntity', 'getEntity');
        $this->get('{controller}/subscribers/{idSubscriberList}', 'getSubscribers');
        $this->post('{controller}/empty', 'emptySubscribers');
        $this->post('{controller}/{entityType}/save', 'save');
        $this->post('{controller}/{entityType}/delete', 'delete');
        $this->post('{controller}/{entityType}/unsubscribe/{idEntity}', 'unsubscribe');
        $this->post('{controller}/{entityType}/subscribe/{idEntity}', 'subscribe');

        $this->get('{controller}/send', 'sendDiffusions');

        $this->get('{controller}/getSectionList', 'getSectionList');
        $this->get('{controller}/getWatchList', 'getWatchList');
    }
    
    /**
     *
     * @param RouteRest $route
     * @return mixed
     */
    protected function call(RouteRest $route)
    {
    	global $data;
    	
    	$data = \encoding_normalize::json_decode(stripslashes($data ?? ''));
    	if (empty($data) || !is_object($data)) {
    		$data = new \stdClass();
    	}
    	
    	$args = $route->getArguments();
    	$className = $this->foundController($route);
    	if (false === $className) {
    		$className = static::CONTROLLER;
    	} elseif (count($args) > 0) {    			
    		array_splice($args, 0, 1);
    	}
    	
    	$callback = [
    		new $className($data),
    		$route->getMethod()
    	];

    	if (is_callable($callback)) {
    		return call_user_func_array($callback, $args);
    	}
    }
    
    private function foundController(RouteRest $route)
    {
    	$args = $route->getArguments();
    	$controller = $args[0] ?? "";
    	
    	$namespace = "Pmb\\DSI\\Controller\\" . Helper::pascalize("{$controller}_controller");
    	if (class_exists($namespace)) {
    		return $namespace;
    	}
    	return false;
    }
}