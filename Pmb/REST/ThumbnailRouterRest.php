<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ThumbnailRouterRest.php,v 1.4 2022/12/13 16:18:28 qvarin Exp $
namespace Pmb\REST;

class ThumbnailRouterRest extends RouterRest
{

    /**
     *
     * @const string
     */
    protected const CONTROLLER = "\\Pmb\\Thumbnail\\Controller\\ThumbnailAPIController";
    
    /**
     *
     * {@inheritdoc}
     * @see \Pmb\REST\RouterRest::generateRoutes()
     */
    protected function generateRoutes()
    {
        $this->get('/{entityType}/{sourceName}', 'getData');
        
        $this->post('/pivot/{entityType}/save', 'savePivot');
        $this->post('/pivot/{entityType}/sources', 'getSourcesByEntityPivot');
        $this->post('/{entityType}/{sourceName}/save', 'saveSource');

    }
    
    /**
     *
     * @param RouteRest $route
     * @return mixed
     */
    protected function call(RouteRest $route)
    {
    	global $data;
    	
    	$className = static::CONTROLLER;
    	$data = \encoding_normalize::json_decode(stripslashes($data));
    	if (empty($data) || !is_object($data)) {
    		$data = new \stdClass();
    	}
    	
    	$callback = [
    		new $className($data),
    		$route->getMethod()
    	];
    	
    	if (is_callable($callback)) {
    		return call_user_func_array($callback, $route->getArguments());
    	}
    }
}