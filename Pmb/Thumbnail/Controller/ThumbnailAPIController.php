<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ThumbnailAPIController.php,v 1.3 2023/02/07 15:13:01 qvarin Exp $
namespace Pmb\Thumbnail\Controller;

use Pmb\Common\Controller\Controller;
use Pmb\Thumbnail\Models\ThumbnailSourcesHandler;
use Pmb\Common\Library\Image\CacheImage;

class ThumbnailAPIController extends Controller
{
    /**
     * 
     * @param string $entityType
     * @param string $sourceName
     */
    public function saveSource($entityType, $sourceName)
    {
        global $msg;

        $thumbnailSourcesHandler = new ThumbnailSourcesHandler();
        $source = $thumbnailSourcesHandler->getSourceClass($entityType, $sourceName);
        $source->setParameters($this->data->values);
        $succes = $source->save();

        CacheImage::clearCache();

        if ($succes) {
            $this->ajaxJsonResponse([
                'succes' => true
            ]);
        }
        $this->ajaxError($msg['common_failed_save']);
    }
    
    /**
     * 
     * @param string $entityType
     * @param string $sourceName
     */
    public function getData($entityType, $sourceName)
    {
        $thumbnailSourcesHandler = new ThumbnailSourcesHandler();
        $source = $thumbnailSourcesHandler->getSourceClass($entityType, $sourceName);
        $this->ajaxJsonResponse([
            'messages' => $source::getMessages(),
            'parameters' => $source->getParameters()
        ]);
    }
    
    /**
     * 
     * @param string $entityType
     */
    public function savePivot(string $entityType)
    {
        global $msg;
        
        $thumbnailSourcesHandler = new ThumbnailSourcesHandler();
        $succes = $thumbnailSourcesHandler->setSourcesByEntityPivot($entityType, $this->data->pivot, $this->data->sources);
        if ($succes) {
            $this->ajaxJsonResponse([
                'succes' => true
            ]);
        }
        $this->ajaxError($msg['common_failed_save']);
    }

    /**
     * 
     * @param string $entityType
     */
    public function getSourcesByEntityPivot(string $entityType)
    {
        $thumbnailSourcesHandler = new ThumbnailSourcesHandler();
        $sources = $thumbnailSourcesHandler->getSourcesByEntityPivot($entityType, $this->data->pivot);
        $this->ajaxJsonResponse([
            'sources' => $sources
        ]);
    }
}

