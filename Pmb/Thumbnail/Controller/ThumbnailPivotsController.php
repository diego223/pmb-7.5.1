<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ThumbnailPivotsController.php,v 1.6 2022/12/13 14:40:34 qvarin Exp $
namespace Pmb\Thumbnail\Controller;

use Pmb\Common\Controller\Controller;
use Pmb\Common\Views\VueJsView;
use Pmb\Thumbnail\Models\ThumbnailSourcesHandler;

class ThumbnailPivotsController extends Controller
{

    protected $action;

    public function proceed($action = '')
    {
        $this->action = $action;
        switch ($action) {
            case 'edit':
            default:
                return $this->defaultAction();
        }
    }

    protected function defaultAction()
    {
        global $msg;
        
        $pivots = [];
        $handler = new ThumbnailSourcesHandler();
        foreach ($handler->getPivotsByEntity($this->data->type) as $pivotClass) {
            $pivots[] = $pivotClass::getViewData();
        }
        
        $viewData = $this->getViewBaseData();
        $viewData["label"] = $msg["admin_thumbnail_entity_{$this->data->type}"] ?? $this->data->type;
        $viewData["type"] = $this->data->type ?? "";
        $viewData["pivots"] = $pivots ?? [];
        $viewData["sources"] = $handler->getSourcesByEntity($this->data->type) ?? [];
        
        $newVue = new VueJsView("thumbnail/pivots", $viewData);
        print $newVue->render();
    }

    private function getViewBaseData()
    {
        global $pmb_url_base;
        return [
            "url_webservice" => $pmb_url_base . "rest.php/thumbnail/",
            "action" => $this->action
        ];
    }
}

