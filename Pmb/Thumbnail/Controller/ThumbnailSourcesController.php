<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ThumbnailSourcesController.php,v 1.7 2022/12/13 14:40:34 qvarin Exp $
namespace Pmb\Thumbnail\Controller;

use Pmb\Common\Controller\Controller;
use Pmb\Common\Views\VueJsView;
use Pmb\Thumbnail\Models\ThumbnailSourcesHandler;

class ThumbnailSourcesController extends Controller
{

    protected $action;

    public function proceed($action = '', $data = null)
    {
        $this->action = $action;
        switch ($action) {
            case 'edit':
            default:
                return $this->defaultAction();
        }
    }

    protected function defaultAction()
    {
        $handler = new ThumbnailSourcesHandler();
        $viewData = $this->getViewBaseData();
        $viewData["sources"] = $handler->getSourcesByEntity();
        $newVue = new VueJsView("thumbnail/sources", $viewData);
        print $newVue->render();
    }

    private function getViewBaseData()
    {
        global $pmb_url_base;
        return [
            "url_webservice" => $pmb_url_base . "rest.php/thumbnail/",
            "action" => $this->action
        ];
    }

}

