<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: Record.php,v 1.2 2022/12/09 11:49:12 qvarin Exp $
namespace Pmb\Thumbnail\Models\Entities\Record;

use Pmb\Thumbnail\Models\Entities\RootEntity;

class Record extends RootEntity
{

    const PATH_PIVOTS = "Record";
}