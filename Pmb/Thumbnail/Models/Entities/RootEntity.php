<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootEntity.php,v 1.2 2022/12/09 11:49:12 qvarin Exp $
namespace Pmb\Thumbnail\Models\Entities;

use Pmb\Thumbnail\Models\ThumbnailParserManifest;

class RootEntity
{

    const PATH_PIVOTS = "";

    protected $pivotsManifests = null;

    public function getPivots(): array
    {
        if (empty(static::PATH_PIVOTS)) {
            return [];
        }

        $path = realpath(__DIR__ . "/../Pivots/Entities/" . static::PATH_PIVOTS);
        if (false === $path || ! is_dir($path)) {
            return [];
        }

        if (! isset($this->pivotsManifests)) {
            $this->pivotsManifests = [];
            $dirs = glob($path . '/*', GLOB_ONLYDIR);
            foreach ($dirs as $dir) {
                if ('CVS' === basename($dir)) {
                    continue;
                }

                $file = "{$dir}/manifest.xml";
                if (is_file($file)) {
                    $this->pivotsManifests[] = new ThumbnailParserManifest($file);
                }
            }
        }

        return $this->pivotsManifests;
    }
}

