<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RecordBasicPivot.php,v 1.7 2023/01/09 14:28:12 qvarin Exp $
namespace Pmb\Thumbnail\Models\Pivots\Entities\Record\RecordBasicPivot;

use Pmb\Thumbnail\Models\Pivots\RootPivot;
use Pmb\Thumbnail\Orm\SourcesEntitiesOrm;

class RecordBasicPivot extends RootPivot
{
    
    public const DEFAULT_MATCH = 0;
    
    public const TYPEDOC_NIVBIBLIO_MATCH = 1;
    
    public const NIVBIBLIO_MATCH = 2;
    
    public const COMPONENT_NAME = "record_basic_pivot";

    public const DEFAULT_PIVOT = '{"typedoc": "", "nivbiblio": ""}';

    protected $type = TYPE_NOTICE;
    
    protected static $staticType = TYPE_NOTICE;

    public function getPivotData(object $entity)
    {
        return [
            "typedoc" => $entity->get_typdoc() ?? "",
            "nivbiblio" => $entity->get_niveau_biblio() ?? ""
        ];
    }

    public function setDataFromForm(object $pivot)
    {
        $data = new \stdClass();
        $data->typedoc = $pivot->typedoc ?? "";
        $data->nivbiblio = $pivot->nivbiblio ?? "";
        $this->pivot = \encoding_normalize::json_encode($data);
    }

    public function check(object $entity)
    {
        $entityData = $this->getPivotData($entity);
        $data = \encoding_normalize::json_decode($this->getData(), true);
        if ($data["typedoc"] == $entityData["typedoc"] && $data["nivbiblio"] == $entityData["nivbiblio"]) {
            return true;
        }
        return false;
    }

    public function getDataForForm()
    {
        return [
            "sources" => $this->getSourceClass(),
            "pivot" => \encoding_normalize::json_decode($this->getData(), true)
        ];
    }

    /**
     *
     * @return array
     */
    public static function getViewData()
    {
        $typedoc = new \marc_list(\marc_list::TYPE_DOCTYPE);
        $nivbiblio = new \marc_list(\marc_list::TYPE_NIVEAU_BIBLIO);
        
        return array_merge(parent::getViewData(), [
            "typedoc" => $typedoc->table ?? [],
            "nivbiblio" => $nivbiblio->table ?? [],
        ]);
    }
    
    public static function getSourcesFromObjectId(int $objectId)
    {
        $recordDatas = new \record_datas($objectId);
        
        $sourcesEntities = SourcesEntitiesOrm::finds(["pivot_class" => static::class], "rank");
        $pivotsOrm = static::getMatchPivot($sourcesEntities, $recordDatas, RecordBasicPivot::TYPEDOC_NIVBIBLIO_MATCH);
        $pivotsOrm = array_merge($pivotsOrm, static::getMatchPivot($sourcesEntities, $recordDatas, RecordBasicPivot::NIVBIBLIO_MATCH));
        $pivotsOrm = array_merge($pivotsOrm, static::getMatchPivot($sourcesEntities, $recordDatas));
        
        $sources = [];
        foreach ($pivotsOrm as $pivotOrm) {
            $pivotInstance = new static($pivotOrm->id);
            $sources[] = $pivotInstance->getSourceClass();
        }
        return $sources;
    }
    
    protected static function getMatchPivot(array $sourcesEntities, \record_datas $recordDatas, int $match = RecordBasicPivot::DEFAULT_MATCH)
    {
        $matches = array();
        switch ($match) {
            case RecordBasicPivot::DEFAULT_MATCH:
                foreach ($sourcesEntities as $sourcesEntitie) {
                    $pivot = \encoding_normalize::json_decode($sourcesEntitie->pivot, true);
                    if (empty($pivot['typedoc']) || empty($pivot['nivbiblio'])) {
                        $matches[] = $sourcesEntitie;
                    }
                }
                break;
                
            case RecordBasicPivot::NIVBIBLIO_MATCH:
                foreach ($sourcesEntities as $sourcesEntitie) {
                    $pivot = \encoding_normalize::json_decode($sourcesEntitie->pivot, true);
                    if (empty($pivot['typedoc']) || empty($pivot['nivbiblio'])) {
                        continue;
                    }
                    
                    if ($pivot['nivbiblio'] == $recordDatas->get_niveau_biblio()) {
                        $matches[] = $sourcesEntitie;
                    }
                }
                break;
                
            case RecordBasicPivot::TYPEDOC_NIVBIBLIO_MATCH:
                foreach ($sourcesEntities as $sourcesEntitie) {
                    $pivot = \encoding_normalize::json_decode($sourcesEntitie->pivot, true);
                    if (empty($pivot['typedoc']) || empty($pivot['nivbiblio'])) {
                        continue;
                    }
                    
                    if ($pivot['typedoc'] == $recordDatas->get_typdoc() && $pivot['nivbiblio'] == $recordDatas->get_niveau_biblio()) {
                        $matches[] = $sourcesEntitie;
                    }
                }
                break;
                
            default:
                return [];
        }
        return $matches;
    }
}

