<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootPivot.php,v 1.4 2022/12/15 13:41:52 tsamson Exp $
namespace Pmb\Thumbnail\Models\Pivots;

use Pmb\Thumbnail\Models\ParserMessage;
use Pmb\Common\Models\Model;
use Pmb\Thumbnail\Orm\SourcesEntitiesOrm;

abstract class RootPivot extends Model implements Pivot
{
    use ParserMessage;

    public const COMPONENT_NAME = "";
    
    public const DEFAULT_PIVOT = '{}';
    
    protected $ormName = SourcesEntitiesOrm::class;

    protected $sourceClass = null;
    
    protected $pivotClass = null;

    protected $type = 0;

    protected $pivot = null;

    protected $rank = 0;
    
    protected static $staticType = 0;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        if (!isset($this->pivotClass)) {            
            $this->pivotClass = static::class;
        }
        if (!isset($this->pivot)) {            
            $this->pivot = static::DEFAULT_PIVOT;
        }
    }

    public function getName()
    {
        $messages = static::getMessages();
        return $messages['name'] ?? '';
    }

    public function getPivotData(object $entity)
    {
        return [];
    }

    public function setDataFromForm(object $pivot)
    {
        # code here...
    }

    public function check(object $entity)
    {
        return false;
    }

    public function getData()
    {
        return $this->pivot;
    }

    public function getDataForForm()
    {
        return [];
    }

    public function save()
    {
        $orm = new $this->ormName($this->id);
        $orm->source_class = $this->getSourceClass();
        $orm->pivot_class = $this->pivotClass;
        $orm->type = $this->getType();
        $orm->pivot = $this->getData();
        $orm->rank = $this->getRank();
        $orm->save();
        
        $this->id = $orm->id;
        return !empty($this->id);
    }
    
    /**
     * @return string
     */
    public function getSourceClass()
    {
        return $this->sourceClass;
    }

    /**
     * @return number
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return number
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param string $sourceClass
     */
    public function setSourceClass(string $sourceClass)
    {
        $this->sourceClass = $sourceClass;
    }

    /**
     * @param number $rank
     */
    public function setRank(int $rank)
    {
        $this->rank = $rank;
    }
    
    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    
    /**
     * 
     * @return array
     */
    public static function getViewData()
    {
        $pivotsOrm = SourcesEntitiesOrm::finds([
            "pivot_class" => static::class,
            "type" => static::$staticType
        ]);
        
        $pivots = [];
        foreach ($pivotsOrm as $pivotOrm) {
            $instance = new static($pivotOrm->id);
            $formdata = $instance->getDataForForm();
            
            if (!isset($pivots[md5($instance->getData())])) {
                $pivots[md5($instance->getData())] = $formdata;
            }
        }
        
        if (empty($pivots)) {
            $pivots[] = ["pivot" => \encoding_normalize::json_decode(static::DEFAULT_PIVOT, true)];
        }
        
        return [
            "namespace" => static::class,
            "component" => static::COMPONENT_NAME,
            "messages" => static::getMessages(),
            "pivots" => array_values($pivots) ?? []
        ];
    }
    
    public static function getSourcesFromObjectId(int $objectId)
    {
        return [];
    }

}

