<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ElectreThumbnailSource.php,v 1.2 2022/12/07 11:21:31 qvarin Exp $
namespace Pmb\Thumbnail\Models\Sources\Entities\Common\Electre;

use Pmb\Thumbnail\Models\Sources\RootThumbnailSource;

class ElectreThumbnailSource extends RootThumbnailSource
{
}

