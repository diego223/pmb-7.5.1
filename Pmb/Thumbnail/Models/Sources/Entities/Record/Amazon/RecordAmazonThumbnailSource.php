<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RecordAmazonThumbnailSource.php,v 1.2 2022/12/23 11:17:13 qvarin Exp $
namespace Pmb\Thumbnail\Models\Sources\Entities\Record\Amazon;

use Pmb\Thumbnail\Models\Sources\RootThumbnailSource;
use Pmb\Common\Library\ISBN\ISBN;

class RecordAmazonThumbnailSource extends RootThumbnailSource
{
    
    const DEFAULT_AMAZON_IMG_URL = "https://images.amazon.com/images/P/{{ASIN}}.08.{{SIZE}}.jpg";
    const AMAZON_IMG_SIZES = [
        'thumbnail' => "TZZZZZZZ",
        'medium' => "MZZZZZZZ",
        'large' => "LZZZZZZZ",
    ];

    /**
     * 
     * {@inheritDoc}
     * @see \Pmb\Thumbnail\Models\Sources\RootThumbnailSource::getImage()
     */
    public function getImage(int $object_id)
    {
        if(!$object_id) {
            return '';
        }
        $q = "select code from notices where notice_id = ".$object_id." limit 1";
        $r = pmb_mysql_query($q);
        if(!pmb_mysql_num_rows($r)) {
            return '';
        }
        $code = trim(pmb_mysql_result($r, 0, 0));
        if(!$code) {
            return '';
        }
        $asin = ISBN::toISBN10($code);
        
        if(!$asin) {
            return '';
        }
        
        $image_url = str_replace(
            ['{{ASIN}}', '{{SIZE}}' ],
            [ $asin, RecordAmazonThumbnailSource::AMAZON_IMG_SIZES['medium'] ],
            RecordAmazonThumbnailSource::DEFAULT_AMAZON_IMG_URL
        );
        $image = $this->loadImageWithCurl($image_url);
        return $image;
    }
    
    public function getWatermark()
    {
        return "Copyright Amazon";
    }
}

