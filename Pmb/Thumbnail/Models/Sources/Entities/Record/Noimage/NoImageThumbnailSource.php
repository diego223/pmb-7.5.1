<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: NoImageThumbnailSource.php,v 1.6 2023/02/07 15:13:01 qvarin Exp $
namespace Pmb\Thumbnail\Models\Sources\Entities\Record\Noimage;

use Pmb\Thumbnail\Models\Sources\RootThumbnailSource;

class NoImageThumbnailSource extends RootThumbnailSource
{

    public const DEFAULT_MATCH = 0;
    
    public const TYPEDOC_NIVBIBLIO_MATCH = 1;
    
    public const NIVBIBLIO_MATCH = 2;
        
    public const DEFAULT_VALUE = [
        [
            "typedoc" => "",
            "nivbiblio" => "",
            "value" => "no_image.png"
        ]
    ];

    public function getParameters()
    {
        $typedoc = new \marc_list(\marc_list::TYPE_DOCTYPE);
        $nivbiblio = new \marc_list(\marc_list::TYPE_NIVEAU_BIBLIO);

        $value = ! empty($this->settings) ? $this->settings : NoImageThumbnailSource::DEFAULT_VALUE;
        return [
            "typedoc" => $typedoc->table ?? [],
            "nivbiblio" => $nivbiblio->table ?? [],
            "values" => $value
        ];
    }
    
    public function getImage(int $objectId)
    {
        if (!$objectId) {
            return '';
        }
        
        $q = "SELECT niveau_biblio, typdoc FROM notices WHERE notice_id = ".$objectId." limit 1";
        $r = pmb_mysql_query($q);
        if (pmb_mysql_num_rows($r) && !empty($this->settings)) {
            $record = pmb_mysql_fetch_assoc($r);
            
            // Matches typedoc && nivbiblio
            $pivot = $this->getMatchPivot($record, NoImageThumbnailSource::TYPEDOC_NIVBIBLIO_MATCH);
            if (empty($pivot)) {
                // Matches empty typedoc && nivbiblio
                $pivot = $this->getMatchPivot($record, NoImageThumbnailSource::NIVBIBLIO_MATCH);
            }
            if (empty($pivot)) {
                // Matches empty typedoc && empty nivbiblio
                $pivot = $this->getMatchPivot($record, NoImageThumbnailSource::DEFAULT_MATCH);
            }
            
            if (!empty($pivot)) {
                $urlIcon = get_url_icon($pivot["value"], true);
                $image = $this->loadImageWithCurl($urlIcon);
                if (!empty($image)) {
                    return $image;
                }
            }
        }
        return "";
    }
    
    protected function getMatchPivot($record, int $match = NoImageThumbnailSource::DEFAULT_MATCH)
    {
        switch ($match) {
            case NoImageThumbnailSource::DEFAULT_MATCH:
                foreach ($this->settings as $pivot) {
                    if (empty($pivot['typedoc']) && empty($pivot['nivbiblio'])) {
                        return $pivot;
                    }
                }
                break;

            case NoImageThumbnailSource::NIVBIBLIO_MATCH:
                foreach ($this->settings as $pivot) {
                    if (empty($pivot['typedoc']) && $pivot['nivbiblio'] == $record["niveau_biblio"]) {
                        return $pivot;
                    }
                }
                break;

            case NoImageThumbnailSource::TYPEDOC_NIVBIBLIO_MATCH:
                foreach ($this->settings as $pivot) {
                    if ($pivot['typedoc'] == $record["typdoc"] && $pivot['nivbiblio'] == $record["niveau_biblio"]) {
                        return $pivot;
                    }
                }
                break;
                
            default:
                return null;
        }
        return null;
    }
}