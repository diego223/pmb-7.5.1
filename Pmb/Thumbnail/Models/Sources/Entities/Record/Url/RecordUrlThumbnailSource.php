<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RecordUrlThumbnailSource.php,v 1.4 2023/02/07 15:13:01 qvarin Exp $

namespace Pmb\Thumbnail\Models\Sources\Entities\Record\Url;

use Pmb\Thumbnail\Models\Sources\Entities\Common\Url\UrlThumbnailSource;
use Pmb\Common\Helper\GlobalContext;

class RecordUrlThumbnailSource extends UrlThumbnailSource
{
    /**
     *
     * {@inheritDoc}
     * @see \Pmb\Thumbnail\Models\Sources\RootThumbnailSource::getImage()
     */
    public function getImage(int $object_id)
    {
        if (!$object_id) {
            return '';
        }
        
        $query = "SELECT thumbnail_url FROM notices WHERE notice_id = {$object_id}";
        $result = pmb_mysql_query($query);
        if (pmb_mysql_num_rows($result)) {
            $thumbnail_url = trim(pmb_mysql_result($result, 0, 0));
            if (!empty($thumbnail_url)) {
                return $this->loadImageWithCurl($thumbnail_url);
            }
        }
        
        $rep_id = GlobalContext::get("pmb_notice_img_folder_id");
        $query = "SELECT repertoire_path FROM upload_repertoire WHERE repertoire_id ='".$rep_id."'";
        $result = pmb_mysql_query($query);
        if(pmb_mysql_num_rows($result)){
            $row = pmb_mysql_fetch_array($result,PMB_MYSQL_NUM);
            $thumbnail_path = $row[0]."img_".$object_id;
            if (file_exists($thumbnail_path)) {
                $content = file_get_contents($thumbnail_path);
                if (!empty($content)) {
                    return $content;
                }
            }
        }
        return '';
    }
}