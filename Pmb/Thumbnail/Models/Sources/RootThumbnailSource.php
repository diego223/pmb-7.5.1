<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: RootThumbnailSource.php,v 1.10 2022/12/15 10:36:46 tsamson Exp $
namespace Pmb\Thumbnail\Models\Sources;

use Pmb\Common\Models\Model;
use Pmb\Thumbnail\Orm\SourcesORM;
use Pmb\Thumbnail\Models\ParserMessage;

class RootThumbnailSource extends Model
{
    const IMG_MIN_SIZE = 100;
    const IMG_MAX_SIZE = 1024*1024;
    
    use ParserMessage;
    
    protected $ormName = "Pmb\\Thumbnail\\Orm\\SourcesORM";

    protected $settings = [];
    
    protected $active = 1;
    
    private function __construct(int $id = 0)
    {
        parent::__construct($id);
    }

    /**
     * 
     * @return \Pmb\Thumbnail\Models\Sources\RootThumbnailSource
     */
    public static function getInstance()
    {
        $resuts = SourcesORM::find("class", static::class);
        $id = ! empty($resuts) ? $resuts[0]->id : 0;
        return new static($id);
    }

    public function getParameters()
    {
        return $this->settings;
    }

    public function setParameters(array $settings)
    {
        $this->settings = $settings;
    }

    public function getImage(int $object_id)
    {
        return "";
    }

    public function save()
    {
        $orm = new $this->ormName($this->id);
        $orm->class = static::class;
        $orm->settings = \encoding_normalize::json_encode($this->settings);
        $orm->active = true;
        $orm->save();

        $this->id = $orm->id;
        return ! empty($this->id) && $this->id != 0;
    }
    
    protected function loadImageWithCurl(string $image_url)
    {
        $curl = new \Curl();
        $curl->limit = RootThumbnailSource::IMG_MAX_SIZE;
        $curl->timeout = 10;
        $curl->options['CURLOPT_SSL_VERIFYPEER'] =0;
        $curl->options['CURLOPT_ENCODING'] = '';
        
        $content = $curl->get($image_url);
        if($content->headers['Status-Code'] != 200) {
            return '';
        }
        if(empty($content->body)) {
            return '';
        }
        $image = $content->body;
        if( empty($content->headers['Content-Length']) && strlen($image) ) {
            $content->headers['Content-Length'] = strlen($image);
        }
        if( ($content->headers['Content-Length'] > RootThumbnailSource::IMG_MAX_SIZE)
            || ($content->headers['Content-Length'] < RootThumbnailSource::IMG_MIN_SIZE)
            )  {
            return '';
        }
        return $image;
    }
    
    public function isActive() 
    {
        if (!empty($this->active)) {
            return true;
        }
        return false;
    }
    
    public function getWatermark()
    {
        return $this->settings["watermark"] ?? "";
    }
}