<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ThumbnailParserManifest.php,v 1.4 2022/12/09 09:20:18 qvarin Exp $
namespace Pmb\Thumbnail\Models;

use Pmb\Common\Library\Parser\ParserManifest;

class ThumbnailParserManifest extends ParserManifest
{
}