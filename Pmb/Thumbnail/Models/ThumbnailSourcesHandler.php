<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ThumbnailSourcesHandler.php,v 1.18 2023/02/07 15:13:01 qvarin Exp $
namespace Pmb\Thumbnail\Models;

use Pmb\Thumbnail\Models\Sources\RootThumbnailSource;
use Pmb\Thumbnail\Models\Pivots\RootPivot;
use Pmb\Thumbnail\Orm\SourcesEntitiesOrm;
use Pmb\Common\Helper\HelperEntities;
use Pmb\Common\Library\Image\Image;
use Pmb\Common\Helper\GlobalContext;
use Pmb\Common\Library\Image\CacheImage;

class ThumbnailSourcesHandler
{

    /**
     * liste des sources par entite
     *
     * @var array
     */
    private $sourcesByEntity = [];

    private $entities = null;

    /**
     * Retourne une source (instance) en fonction d'un type d'entit� et d'un nom de source
     *
     * @param string $entityType
     * @param string $sourceName
     * @return NULL|RootThumbnailSource
     */
    public function getSourceClass($entityType, $sourceName)
    {
        $path = realpath(__DIR__) . '/Sources/Entities/' . ucfirst($entityType) . "/" . ucfirst($sourceName);
        $sourceManifest = ThumbnailParserDirectory::getInstance()->getManifests($path);
        $sourceManifest = $sourceManifest[0] ?? false;

        if (false === $sourceManifest) {
            return null;
        }

        return $sourceManifest->namespace::getInstance();
    }

    /**
     * Retourne la liste des entit�s (classe enfantes de RootEntity)
     *
     * @return array
     */
    public function getEntitiesList(): array
    {
        global $msg;

        if (! isset($this->entities)) {
            $this->entities = [];
            $manifests = ThumbnailParserDirectory::getInstance()->getEntitiesList();
            foreach ($manifests as $manifest) {
                $this->entities[] = [
                    "namespace" => $manifest->namespace,
                    "type" => $manifest->entityType,
                    "label_code" => "admin_thumbnail_entity_{$manifest->entityType}",
                    "label" => $msg["admin_thumbnail_entity_{$manifest->entityType}"] ?? $manifest->entityType
                ];
            }
        }
        return $this->entities;
    }

    /**
     * Retourne la liste des sources pour un type d'entit� donn�.
     * Si Aucun type donn�e retourne toutes les source pour chaque entit�
     *
     * @param string|null $entityType
     * @return array
     */
    public function getSourcesByEntity(?string $entityType = null)
    {
        if (empty($this->sourcesByEntity)) {
            $manifests = ThumbnailParserDirectory::getInstance()->getManifests();
            foreach ($manifests as $manifest) {
                if ("source" == $manifest->type) {
                    $messages = $manifest->namespace::getMessages();
                    $this->sourcesByEntity[$manifest->entity][] = [
                        "namespace" => $manifest->namespace,
                        "source" => $manifest->name,
                        "label" => $messages['name'] ?? $manifest->name
                    ];
                }
            }
        }
        return $entityType ? $this->sourcesByEntity[$entityType] : $this->sourcesByEntity;
    }

    /**
     * Retourne la liste des pivots pour un type d'entit� donn�.
     *
     * @param string $entityType
     * @return array
     */
    public function getPivotsByEntity(string $entityType)
    {
        $entityIndex = array_search($entityType, array_column($this->getEntitiesList(), "type"));
        if (false === $entityIndex || ! class_exists($this->entities[$entityIndex]['namespace'])) {
            return [];
        }

        $pivots = [];
        $entity = new $this->entities[$entityIndex]['namespace']();
        foreach ($entity->getPivots() as $manifest) {
            if (class_exists($manifest->namespace)) {
                $pivots[] = $manifest->namespace;
            }
        }
        return $pivots;
    }

    /**
     * Retourne la liste des sources qui match pour un type d'entit� donn� avec son identifiant
     *
     * @param string $entityType
     * @param int $objectId
     * @return array
     */
    public function getSourcesByObject(string $entityType, int $objectId)
    {
        $sources = [];
        $pivots = $this->getPivotsByEntity($entityType);
        if (!empty($pivots)) {
            foreach ($pivots as $pivot) {
                $sources = array_merge($sources, $pivot::getSourcesFromObjectId($objectId));
            }
        }
        return $sources;
    }

    /**
     * R�cup�re les donn�e dans la table "SourcesEntitiesOrm"
     *
     * @param string $entityType
     * @param string $pivot
     * @return array
     */
    public function getSourcesByEntityPivot(string $entityType, object $pivot)
    {
        $pivotsOrm = SourcesEntitiesOrm::finds([
            "pivot" => \encoding_normalize::json_encode($pivot),
            "type" => \entities::get_entity_type_from_entity($entityType)
        ], 'rank');
        
        $sources = [];
        foreach ($pivotsOrm as $pivotOrm) {
            $sources[$pivotOrm->rank] = $pivotOrm->source_class;
        }
        
        return $sources;
    }

    /**
     * Permet de remplir la table "SourcesEntitiesOrm"
     *
     * @param string $entity
     * @param mixed $pivot
     * @param array $sources
     */
    public function setSourcesByEntityPivot(string $entityType, $pivot, array $sources)
    {
        $success = true;
        
        $pivotData = clone $pivot;
        unset($pivotData->namespace);
        
        $pivotsOrm = SourcesEntitiesOrm::finds([
            "pivot_class" => $pivot->namespace,
            "pivot" => \encoding_normalize::json_encode($pivotData),
            "type" => \entities::get_entity_type_from_entity($entityType)
        ]);
        if (!empty($pivotsOrm)) {
            array_walk($pivotsOrm, function ($orm) {
                $orm->delete();
            });
        }
        
        foreach ($sources as $rank => $sourceNamespace) {
            
            /**
             * @var RootPivot $pivotInstance
             */
            $pivotInstance = new $pivot->namespace();
            $pivotInstance->setDataFromForm($pivot);
            $pivotInstance->setRank($rank);
            $pivotInstance->setType(\entities::get_entity_type_from_entity($entityType));
            $pivotInstance->setSourceClass($sourceNamespace);
            $success &= $pivotInstance->save();
        }
        
        CacheImage::clearCache();
        
        return $success;
    }
    
    public function printImage(int $type, int $objectId)
    {
        $maxSize = intval(GlobalContext::get("notice_img_pics_max_size"));
        $entitiesNamespaces = HelperEntities::get_entities_namespace();
        $sources = $this->getSourcesByObject($entitiesNamespaces[$type], $objectId);

        $filenameCache = $this->generateFilenameCache($entitiesNamespaces[$type], $objectId);
        $cache = CacheImage::fetch($filenameCache);
        if (!empty($cache) && !is_null(Image::printPNG($cache))) {
            exit;
        }
        
        foreach ($sources as $source) {
            $sourceClass = $source::getInstance();
            if ($sourceClass->isActive()) {
                $img = $sourceClass->getImage($objectId);
                if (empty($img)) {
                    continue;
                }
                
                $img = Image::format($img, $maxSize, $sourceClass->getWatermark());
                CacheImage::add($filenameCache, $img);
                if (!is_null(Image::printPNG($img))) {
                    exit;
                }
            }
        }
    }
    
    /**
     * Permet de v�rifier si le type d'entit� existe
     *
     * @param string $type
     * @return boolean
     */
    public static function checkType(string $type)
    {
        $entitiesNamespaces = HelperEntities::get_entities_namespace();
        if (empty($entitiesNamespaces[$type])) {
            return false;
        }
        return true;
    }

    /**
     * Genere le nom du fichier en cache
     *
     * @param string $entityType
     * @param int $objectId
     * @return string
     */
    public function generateFilenameCache(string $entityType, int $objectId)
    {
        $sources = $this->getSourcesByObject($entityType, $objectId);
        return md5(LOCATION . $entityType . $objectId . implode("_", $sources)) . ".png";
    }

    /**
     * Genere l'URL d'acc�s pour le type d'entite et un idenfiant donnee
     *
     * @param int $type
     * @param int $objectId
     * @return string
     */
    public function generateUrl(int $type, int $objectId): string
    {
        $entitiesNamespaces = HelperEntities::get_entities_namespace();
        $filenameCache = $this->generateFilenameCache($entitiesNamespaces[$type], $objectId);
        $urlCache = CacheImage::generateUrl($filenameCache);
        if (!empty($urlCache)) {
            return $urlCache;
        }
        return GlobalContext::get("url_base") . "thumbnail.php?type={$type}&id={$objectId}";
    }
}
