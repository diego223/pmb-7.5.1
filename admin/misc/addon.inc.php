<?php
// +-------------------------------------------------+
//  2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: addon.inc.php,v 1.6.6.5 2023/03/08 08:43:48 tsamson Exp $

if (stristr($_SERVER['REQUEST_URI'], ".inc.php")) die("no access");

if( !function_exists('traite_rqt') ) {
	function traite_rqt($requete="", $message="") {

		global $charset;
		$retour="";
		if($charset == "utf-8"){
			$requete=utf8_encode($requete);
		}
		pmb_mysql_query($requete) ; 
		$erreur_no = pmb_mysql_errno();
		if (!$erreur_no) {
			$retour = "Successful";
		} else {
			switch ($erreur_no) {
				case "1060":
					$retour = "Field already exists, no problem.";
					break;
				case "1061":
					$retour = "Key already exists, no problem.";
					break;
				case "1091":
					$retour = "Object already deleted, no problem.";
					break;
				default:
					$retour = "<font color=\"#FF0000\">Error may be fatal : <i>".pmb_mysql_error()."<i></font>";
					break;
			}
		}		
		return "<tr><td><font size='1'>".($charset == "utf-8" ? utf8_encode($message) : $message)."</font></td><td><font size='1'>".$retour."</font></td></tr>";
	}
}
echo "<table>";

/******************** AJOUTER ICI LES MODIFICATIONS *******************************/

switch ($pmb_bdd_subversion) {
	case 0:
		// DG - Ajout d'une classification sur les listes
		$rqt = "ALTER TABLE lists ADD list_num_ranking int not null default 0 AFTER list_default_selected" ;
		echo traite_rqt($rqt,"ALTER TABLE lists ADD list_num_ranking");
	case 1:
		// DG - Ajout dans les bannettes la possibilit� d'historiser les diffusions
		$rqt = "ALTER TABLE bannettes ADD bannette_diffusions_history INT(1) UNSIGNED NOT NULL default 0";
		echo traite_rqt($rqt,"ALTER TABLE bannettes ADD bannette_diffusions_history");
		
		// DG - Log des diffusions de bannettes
		$rqt = "CREATE TABLE IF NOT EXISTS bannettes_diffusions (
					id_diffusion int unsigned not null auto_increment primary key,
        			diffusion_num_bannette int(9) unsigned not null default 0,
        			diffusion_mail_object text,
					diffusion_mail_content mediumtext,
					diffusion_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
					diffusion_records text,
					diffusion_deleted_records text,
					diffusion_recipients text,
					diffusion_failed_recipients text
        		)";
		echo traite_rqt($rqt,"create table bannettes_diffusions");
	case 2:
		// TS-RT-JP - Ajout de la table dsi_content_buffer
		$rqt = "CREATE TABLE IF NOT EXISTS dsi_content_buffer (
		  id_content_buffer int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		  type int(11) NOT NULL DEFAULT 0,
		  content longblob NOT NULL,
		  num_diffusion_history int(10) UNSIGNED NOT NULL DEFAULT 0
		)";
		echo traite_rqt($rqt,"CREATE TABLE dsi_content_buffer");
		
		// TS-RT-JP - Ajout du champ automatic sur une diffusion
		$rqt = "ALTER TABLE dsi_diffusion ADD automatic tinyint(1) NOT NULL DEFAULT 0 AFTER settings" ;
		echo traite_rqt($rqt,"ALTER dsi_diffusion ADD automatic");
		
		// TS-RT-JP - Ajout d'un �tat sur l'historique de diffusion
		$rqt = "ALTER TABLE dsi_diffusion_history ADD state tinyint(1) NOT NULL DEFAULT 0 AFTER total_recipients" ;
		echo traite_rqt($rqt,"ALTER dsi_diffusion_history ADD state");
}



/******************** JUSQU'ICI **************************************************/
/* PENSER � faire +1 au param�tre $pmb_subversion_database_as_it_shouldbe dans includes/config.inc.php */
/* COMMITER les deux fichiers addon.inc.php ET config.inc.php en m�me temps */

echo traite_rqt("update parametres set valeur_param='".$pmb_subversion_database_as_it_shouldbe."' where type_param='pmb' and sstype_param='bdd_subversion'","Update to $pmb_subversion_database_as_it_shouldbe database subversion.");
echo "<table>";