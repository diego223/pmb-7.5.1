<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_opac_reader_registration.class.php,v 1.2 2022/08/01 06:44:58 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

class mail_opac_reader_registration extends mail_opac_reader {
	
	protected function _init_default_settings() {
		parent::_init_default_settings();
		$this->_init_setting_value('sender', 'parameter');
	}
	
	protected function get_mail_object() {
		global $msg, $opac_biblio_name;
		
		return str_replace("!!biblio_name!!",$opac_biblio_name,$msg['subs_mail_obj']);
	}
	
	protected function get_mail_content() {
		global $msg;
		global $opac_biblio_name,$opac_url_base ;
		global $opac_url_base;
		
		$mail_content = str_replace("!!biblio_name!!",$opac_biblio_name,$msg['subs_mail_corps']) ;
		$mail_content = str_replace("!!empr_first_name!!", $this->empr->prenom,$mail_content) ;
		$mail_content = str_replace("!!empr_last_name!!",$this->empr->nom,$mail_content) ;
		
		// nouvelle cl� de validation :
		$alphanum  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		$cle_validation = substr(str_shuffle($alphanum), 0, 20);
		$query = "UPDATE empr set cle_validation = '".$cle_validation."' WHERE id_empr = ".$this->mail_to_id;
		pmb_mysql_query($query);
		
		$lien_validation = "<a href='".$opac_url_base."subscribe.php?subsact=validation&login=".urlencode($this->empr->login)."&cle_validation=$cle_validation'>".$opac_url_base."subscribe.php?subsact=validation&login=".$this->empr->login."&cle_validation=$cle_validation</a>";
		$mail_content = str_replace("!!lien_validation!!",$lien_validation,$mail_content) ;
		
		return $mail_content;
	}
}