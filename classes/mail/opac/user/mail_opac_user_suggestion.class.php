<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_opac_user_suggestion.class.php,v 1.2 2022/08/01 06:44:58 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

class mail_opac_user_suggestion extends mail_opac_user {
	
	protected $recipient;
	
	protected function get_mail_object() {
		global $msg;
		
		return $msg["mail_sugg_obj"]." ".$this->recipient->aff_quand;
	}
	
	protected function get_mail_from_name() {
		return $this->recipient->location_libelle;
	}
	
	protected function get_mail_from_mail() {
		return $this->recipient->user_email;
	}
	
	public function set_recipient($recipient) {
		$this->recipient = $recipient;
		return $this;
	}
}