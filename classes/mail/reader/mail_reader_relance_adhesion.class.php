<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_reader_relance_adhesion.class.php,v 1.8 2022/08/01 06:44:58 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

global $class_path;
require_once($class_path."/emprunteur.class.php");

class mail_reader_relance_adhesion extends mail_reader {
	
    protected static function get_parameter_prefix() {
        return "mailrelanceadhesion";
    }
	
    protected function _init_default_parameters() {
    	$this->_init_parameter_value('list_order', 'empr_nom, empr_prenom');
        parent::_init_default_parameters();
    }
    
	protected function get_query_list_order_all() {
	    return " ORDER BY ".$this->get_parameter_value('list_order');
	}
	
	protected function get_query_list_all() {
		global $pmb_lecteurs_localises, $empr_location_id, $deflt2docs_location;
		global $empr_statut_edit, $empr_categ_filter, $empr_codestat_filter;
		global $restricts;
		
		// restriction localisation le cas �ch�ant
		if ($pmb_lecteurs_localises) {
			if ($empr_location_id=="") $empr_location_id = $deflt2docs_location ;
			if ($empr_location_id!=0) $restrict_localisation = " AND empr_location='$empr_location_id' ";
				else $restrict_localisation = "";
		}
	
		// filtr� par un statut s�lectionn�
		$restrict_statut="";
		if ($empr_statut_edit) {
			if ($empr_statut_edit!=0) $restrict_statut = " AND empr_statut='$empr_statut_edit' ";
		}
		$restrict_categ = '';
		if($empr_categ_filter) {
			$restrict_categ = " AND empr_categ= '".$empr_categ_filter."' ";
		}
		$restrict_codestat = '';
		if($empr_codestat_filter) {
			$restrict_codestat = " AND empr_codestat= '".$empr_codestat_filter."' ";
		}
		$requete = "SELECT empr.id_empr  FROM empr, empr_statut ";
		$restrict_empr = " WHERE 1 ";
		$restrict_requete = $restrict_empr.$restrict_localisation.$restrict_statut.$restrict_categ.$restrict_codestat." and ".$restricts;
		$requete .= $restrict_requete;
		$requete.=" and empr_mail!=''";
		$requete .= " and empr_statut=idstatut ";
		$requete .= $this->get_query_list_order_all();
		return $requete;
	}
	
	protected function get_mail_object() {
		return $this->get_parameter_value('objet');
	}
	
	protected function get_mail_content() {
		$mail_content = '';
		if($this->get_parameter_value('madame_monsieur')) {
			$mail_content .= $this->get_parameter_value('madame_monsieur')."\r\n\r\n";
		}
		$mail_content .= $this->get_parameter_value('texte')."\r\n";
		if($this->get_parameter_value('fdp')) {
			$mail_content .= $this->get_parameter_value('fdp')."\r\n\r\n";
		}
		$mail_content .= $this->get_mail_bloc_adresse();
		
		$coords = $this->get_empr_coords();
		$mail_content = str_replace("!!date_fin_adhesion!!", $coords->aff_date_expiration, $mail_content);
		//remplacement nom et prenom
		$mail_content=str_replace("!!empr_name!!", $coords->empr_nom,$mail_content);
		$mail_content=str_replace("!!empr_first_name!!", $coords->empr_prenom,$mail_content);
		
		return $mail_content;
	}
	
	protected function get_mail_headers() {
		global $charset;
		
		return "Content-type: text/plain; charset=".$charset."\n";
	}
	
	public function send_mail() {
		global $action;
		
		if ($action=="print_all") {
			$requete = $this->get_query_list_all();
			$res = pmb_mysql_query($requete);
			while(($empr=pmb_mysql_fetch_object($res))) {
				$this->mail_to_id = $empr->id_empr;
				$coords = $this->get_empr_coords();
				if($this->get_mail_to_mail()) {
					$res_envoi=$this->mailpmb();
					if ($res_envoi) echo $this->get_display_sent_succeed();
					else echo $this->get_display_sent_failed();
				} else {
					echo $this->get_display_unknown_mail();
				}
			}
		} else {
			$coords = $this->get_empr_coords();
			if($coords->empr_mail) {
				$res_envoi=$this->mailpmb();
				if ($res_envoi) echo $this->get_display_sent_succeed();
				else echo $this->get_display_sent_failed();
			} else {
				echo $this->get_display_unknown_mail();
			}
		}
	}
}