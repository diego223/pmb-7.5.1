<?php
// +-------------------------------------------------+
// | 2002-2007 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_configuration.class.php,v 1.20 2023/02/08 07:13:58 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

global $class_path, $include_path;
global $msg, $charset;
global $mail_configuration_address_content_form, $mail_configuration_domain_content_form;
global $mail_configuration_hote_content_form, $mail_configuration_authentification_content_form;
global $mail_configuration_hote, $mail_configuration_port;
global $mail_configuration_authentification, $mail_configuration_user, $mail_configuration_password;
global $mail_configuration_secure_protocol, $mail_configuration_authentification_type;
global $pmb_mail_adresse_from, $opac_mail_adresse_from, $opac_biblio_email;
global $PMBuseremail;

require_once($class_path."/mails/mails_configuration.class.php");
require_once($include_path."/templates/mails/mail_configuration.tpl.php");

class mail_configuration {
    
	protected $id;
	
	protected $type;
	
	protected $name;
	
	protected $protocol;
	
	protected $hote;
	
	protected $port;
	
	protected $authentification;
	
	protected $user;
	
	protected $password;
	
	protected $secure_protocol;
	
	protected $authentification_type;
	
	protected $authentification_type_settings;
	
	protected $allowed_hote_override;
	
	protected $allowed_authentification_override;
	
	protected $validated;
	
	protected $informations;
	
	protected $confidential;
	
	protected $domain;
	
	protected $domain_name;
	
	protected $auto_fill;
	
	protected $uses;
	
	//Types d'authentification en SMTP
	const SMTP_SECURE_PROTOCOLS = [
			'ssl' => 'SSL/TLS',
			'tls' => 'STARTTLS',
	];
	
	public function __construct($name='') {
		$this->id = 0;
		$this->name = $name;
		if(strpos($this->name, '@') !== false) {
			$this->type = 'address';
		} else {
			$this->type = 'domain';
		}
		$this->fetch_data();
	}
	
	protected function init_properties() {
		if(strpos($this->name, '@') !== false) {
			$this->domain_name = substr($this->name, strpos($this->name, '@')+1);
		} else {
			$this->domain_name = $this->name;
		}
		$this->protocol = 'SMTP';
		$this->hote = '';
		$this->port = ($this->type == 'domain' ? 25 : '');
		$this->authentification = 0;
		$this->user = '';
		$this->password = '';
		$this->secure_protocol = '';
		$this->authentification_type = '';
		$this->authentification_type_settings = array();
		$this->allowed_hote_override = 0;
		$this->allowed_authentification_override = 0;
		$this->validated = 0;
		$this->informations = array();
	}
	
	protected function load_properties_from_generic_smtp_servers() {
		if(mails_configuration::has_exists_domain($this->domain_name)) {
			$this->hote = mails_configuration::get_hote_from_domain($this->domain_name);
			$this->port = mails_configuration::get_port_from_domain($this->domain_name);
			$this->secure_protocol = mails_configuration::get_secure_protocol_from_domain($this->domain_name);
		} else {
			$this->hote = strtolower($this->protocol).".".$this->domain_name;
		}
		$this->authentification = 1;
		$this->allowed_authentification_override = 1;
		$this->auto_fill = true;
	}
	
	protected function fetch_data_hote($row) {
		$this->hote = $row->mail_configuration_hote;
		$this->port = $row->mail_configuration_port;
		$this->secure_protocol = $row->mail_configuration_secure_protocol;
	}
	
	protected function fetch_data_authentification($row) {
		$this->authentification = $row->mail_configuration_authentication;
		$this->user = $row->mail_configuration_user;
		if($row->mail_configuration_password != '') {
			$this->password = convert_uudecode($row->mail_configuration_password);
		}
		$this->authentification_type = $row->mail_configuration_authentification_type;
		$this->authentification_type_settings = encoding_normalize::json_decode($row->mail_configuration_authentification_type_settings, true);
	}
	
	protected function fetch_data_override($row) {
		$this->allowed_hote_override = $row->mail_configuration_allowed_hote_override;
		$this->allowed_authentification_override = $row->mail_configuration_allowed_authentification_override;
	}
	
	protected function fetch_domain_data() {
		$query = "SELECT * FROM mails_configuration WHERE name_mail_configuration = '".addslashes($this->get_domain()->get_name())."'";
		$result = pmb_mysql_query($query);
		if(pmb_mysql_num_rows($result)) {
			$row = pmb_mysql_fetch_object($result);
			if(!$this->id || ($this->id && !$row->mail_configuration_allowed_hote_override)) {
				$this->fetch_data_hote($row);
			}
			if(!$this->id || ($this->id && !$row->mail_configuration_allowed_authentification_override)) {
				$this->fetch_data_authentification($row);
			}
		}
	}
	
	protected function fetch_data() {
		$this->init_properties();
		if($this->is_confidential()) {
			pmb_error::get_instance(static::class)->add_message("permission_denied", "mail_configuration_action_edit_locked");
		}
		if($this->name) {
			$query = "SELECT * FROM mails_configuration WHERE name_mail_configuration = '".addslashes($this->name)."'";
			$result = pmb_mysql_query($query);
			if(pmb_mysql_num_rows($result)) {
				$row = pmb_mysql_fetch_object($result);
				$this->id = $row->id_mail_configuration;
				$this->fetch_data_hote($row);
				$this->fetch_data_authentification($row);
				$this->fetch_data_override($row);
				$this->validated = $row->mail_configuration_validated;
				if(!empty($row->mail_configuration_informations)) {
					$this->informations = encoding_normalize::json_decode($row->mail_configuration_informations, true);
				}
			}
			if($this->type == 'address' && $this->get_domain()->get_name()) {
				$this->fetch_domain_data();
			}
		}
	}
	
	protected function get_protocol_selector() {
		return "
			<select name='mail_configuration_protocol' ".($this->is_hote_readonly() ? "readonly='readonly' disabled='disabled'" : "").">
					<option value='SMTP' ".($this->protocol == 'SMTP' ? "selected='selected'" : "").">SMTP</option>
			</select>";	
	}
	
	protected function get_secure_protocol_selector() {
		return "
			<select name='mail_configuration_secure_protocol' ".($this->is_hote_readonly() ? "readonly='readonly' disabled='disabled'" : "").">
					<option value='' ".(empty($this->secure_protocol) ? "selected='selected'" : "")."></option>
					<option value='ssl' ".($this->secure_protocol == 'ssl' ? "selected='selected'" : "").">".static::SMTP_SECURE_PROTOCOLS['ssl']."</option>
					<option value='tls' ".($this->secure_protocol == 'tls' ? "selected='selected'" : "").">".static::SMTP_SECURE_PROTOCOLS['tls']."</option>
			</select>";
	}
	
	protected function get_authentification_type_selector() {
		$selector = "<select name='mail_configuration_authentification_type' ".($this->is_authentification_readonly() ? "readonly='readonly' disabled='disabled'" : "").">
						<option value='' ".(empty($this->authentification_type) ? "selected='selected'" : "")."></option>";
		foreach (mail::SMTP_AUTH_TYPES as $auth_type) {
			$selector .= "<option value='".$auth_type."' ".($this->authentification_type == $auth_type ? "selected='selected'" : "").">".$auth_type."</option>";
		}
		$selector .= "</select>";
		
		return $selector;
	}
	
	public function get_authentification_type_settings_content_form() {
		global $charset;
		global $mail_configuration_authentification_type_settings_xoauth2_content_form;
		
		$content_form = '';
		switch ($this->authentification_type) {
			case 'XOAUTH2':
				$content_form = $mail_configuration_authentification_type_settings_xoauth2_content_form;
				$content_form = str_replace('!!xoauth2_provider!!', (!empty($this->authentification_type_settings['xoauth2_provider']) ? htmlentities($this->authentification_type_settings['xoauth2_provider'], ENT_QUOTES, $charset) : ''), $content_form);
				$content_form = str_replace('!!xoauth2_tenant_id!!', (!empty($this->authentification_type_settings['xoauth2_tenant_id']) ? htmlentities($this->authentification_type_settings['xoauth2_tenant_id'], ENT_QUOTES, $charset) : ''), $content_form);
				$content_form = str_replace('!!xoauth2_client_id!!', (!empty($this->authentification_type_settings['xoauth2_client_id']) ? htmlentities($this->authentification_type_settings['xoauth2_client_id'], ENT_QUOTES, $charset) : ''), $content_form);
				$content_form = str_replace('!!xoauth2_secret_value!!', (!empty($this->authentification_type_settings['xoauth2_secret_value']) ? htmlentities($this->authentification_type_settings['xoauth2_secret_value'], ENT_QUOTES, $charset) : ''), $content_form);
				$content_form = str_replace('!!xoauth2_refresh_token!!', (!empty($this->authentification_type_settings['xoauth2_refresh_token']) ? htmlentities($this->authentification_type_settings['xoauth2_refresh_token'], ENT_QUOTES, $charset) : ''), $content_form);
				break;
		}
		return $content_form;
	}
	
	
	public function get_form() {
		global $msg, $charset;
		global $mail_configuration_address_content_form, $mail_configuration_domain_content_form;
		global $mail_configuration_hote_content_form, $mail_configuration_secure_protocol_content_form;
		global $mail_configuration_hote_read_form, $mail_configuration_secure_protocol_read_form;
		global $mail_configuration_authentification_content_form, $mail_configuration_authentification_user_pwd_content_form;
		global $mail_configuration_authentification_type_content_form;
		
		$content_form = '';
		if(!$this->id && $this->type == 'domain') {
			$this->load_properties_from_generic_smtp_servers();
		}
		switch ($this->type) {
			case 'domain':
				$content_form .= $mail_configuration_domain_content_form;
				$content_form .= $mail_configuration_hote_content_form;
				$content_form .= $mail_configuration_secure_protocol_content_form;
				$content_form .= $mail_configuration_authentification_content_form;
				$content_form .= $mail_configuration_authentification_user_pwd_content_form;
				$content_form .= $mail_configuration_authentification_type_content_form;
//  			$content_form .= $mail_configuration_allowed_overrides_content_form;
				break;
			case 'address':
				$content_form .= $mail_configuration_address_content_form;
				if(!$this->is_hote_readonly()) {
					$content_form .= $mail_configuration_hote_content_form;
					$content_form .= $mail_configuration_secure_protocol_content_form;
				} else {
					if($this->hote) {
						$content_form .= $mail_configuration_hote_read_form;
					}
					if($this->secure_protocol) {
						$content_form .= $mail_configuration_secure_protocol_read_form;
					}
				}
// 				$content_form .= $mail_configuration_authentification_content_form;
				$content_form .= $mail_configuration_authentification_user_pwd_content_form;
				if(!$this->is_hote_readonly()) {
					$content_form .= $mail_configuration_authentification_type_content_form;
				}
				break;
		}
		$interface_form = new interface_form('mail_configuration_form');
		$interface_form->set_label($msg['mail_configuration_edit']);
		$content_form = str_replace('!!type!!', htmlentities($this->type, ENT_QUOTES, $charset), $content_form);
		$content_form = str_replace('!!name!!', htmlentities($this->name, ENT_QUOTES, $charset), $content_form);
// 		$content_form = str_replace('!!protocol_selector!!', $this->get_protocol_selector(), $content_form);
		$content_form = str_replace('!!hote!!', htmlentities($this->hote, ENT_QUOTES, $charset), $content_form);
		$content_form = str_replace('!!port!!', $this->port, $content_form);
		$content_form = str_replace('!!authentification_none!!', (!$this->authentification ? "checked='checked'" : ""), $content_form);
		$content_form = str_replace('!!authentification_domain_reserved!!', ($this->authentification && !$this->allowed_authentification_override ? "checked='checked'" : ""), $content_form);
		$content_form = str_replace('!!authentification_mail_reserved!!', ($this->authentification && $this->allowed_authentification_override ? "checked='checked'" : ""), $content_form);
		$content_form = str_replace('!!user!!', htmlentities($this->user, ENT_QUOTES, $charset), $content_form);
		$content_form = str_replace('!!password!!', (!$this->is_authentification_readonly() ? htmlentities($this->password, ENT_QUOTES, $charset) : ''), $content_form);
		$content_form = str_replace('!!password_placeholder!!', ($this->password ? htmlentities($msg['empr_pwd_opac_affected'], ENT_QUOTES, $charset) : ''), $content_form);
		$content_form = str_replace('!!secure_protocol_selector!!', $this->get_secure_protocol_selector(), $content_form);
		$content_form = str_replace('!!secure_protocol!!', (!empty($this->secure_protocol) ? static::SMTP_SECURE_PROTOCOLS[$this->secure_protocol] : ''), $content_form);
		$content_form = str_replace('!!authentification_type_selector!!', $this->get_authentification_type_selector(), $content_form);
		$content_form = str_replace('!!authentification_type_settings!!', $this->get_authentification_type_settings_content_form(), $content_form);
		
		$content_form = str_replace('!!allowed_hote_override!!', ($this->get_domain()->is_allowed_hote_override() ? "checked='checked'" : ""), $content_form);
// 		$content_form = str_replace('!!allowed_authentification_override!!', ($this->get_domain()->is_allowed_authentification_override() ? "checked='checked'" : ""), $content_form);
		
		$content_form = str_replace('!!hote_readonly!!', ($this->is_hote_readonly() ? "readonly='readonly' disabled='disabled'" : ""), $content_form);
		$content_form = str_replace('!!authentification_readonly!!', ($this->is_authentification_readonly() ? "readonly='readonly' disabled='disabled'" : ""), $content_form);
		
		$content_form = str_replace('!!auto_fill!!', (!empty($this->auto_fill) ? "*" : ""), $content_form);
		
		$interface_form->set_object_id(($this->is_in_database() ? 1 : 0))
		->set_confirm_delete_msg($msg['confirm_suppr_de']." ".$this->name." ?")
		->set_content_form($content_form)
		->set_table_name('mails_configuration');
		if($this->type == 'domain') {
			$interface_form->set_field_focus('mail_configuration_hote');
		} else {
			$interface_form->set_field_focus('mail_configuration_user');
		}
		return $interface_form->get_display();
	}
	
	public function set_properties_from_form() {
		global $mail_configuration_hote, $mail_configuration_port;
		global $mail_configuration_authentification, $mail_configuration_user, $mail_configuration_password;
		global $mail_configuration_secure_protocol, $mail_configuration_authentification_type, $mail_configuration_authentification_type_settings;
		global $mail_configuration_allowed_hote_override;
		
		switch ($this->type) {
			case 'domain':
				$mail_configuration_authentification = intval($mail_configuration_authentification);
				$this->hote = stripslashes($mail_configuration_hote);
				$this->port = intval($mail_configuration_port);
				if($mail_configuration_authentification == 1) {
					$this->user = stripslashes($mail_configuration_user);
					if($mail_configuration_password != '') {
						$this->password = stripslashes($mail_configuration_password);
					}
				} else {
					$this->user = '';
					$this->password = '';
				}
				if($mail_configuration_authentification || ($this->user && $this->password)) {
					$this->authentification = 1;
				} else {
					$this->authentification = 0;
				}
				$this->secure_protocol = stripslashes($mail_configuration_secure_protocol);
				$this->authentification_type = stripslashes($mail_configuration_authentification_type);
				$this->authentification_type_settings = stripslashes_array($mail_configuration_authentification_type_settings);
 				$this->allowed_hote_override = intval($mail_configuration_allowed_hote_override);
 				if($mail_configuration_authentification == 2) {
 					$this->allowed_authentification_override = 1;
 				} else {
 					$this->allowed_authentification_override = 0;
 				}
 				$this->validated = 0;
 				$this->informations = array();
				break;
			case 'address':
				if($this->get_domain()->is_allowed_hote_override()) {
					$this->hote = stripslashes($mail_configuration_hote);
					$this->port = intval($mail_configuration_port);
				} else {
					$this->hote = '';
					$this->port = '';
				}
				if($this->get_domain()->is_allowed_authentification_override()) {
					$this->user = stripslashes($mail_configuration_user);
					if($mail_configuration_password != '') {
						$this->password = stripslashes($mail_configuration_password);
					}
					if($this->user && $this->password) {
						$this->authentification = 1;
					} else {
						$this->authentification = 0;
					}
					$this->secure_protocol = stripslashes($mail_configuration_secure_protocol);
					$this->authentification_type = stripslashes($mail_configuration_authentification_type);
					$this->authentification_type_settings = stripslashes_array($mail_configuration_authentification_type_settings);
				} else {
					$this->authentification = 0;
					$this->user = '';
					$this->password = '';
					$this->secure_protocol = '';
					$this->authentification_type = '';
					$this->authentification_type_settings = array();
				}
 				$this->allowed_hote_override = 1;
 				$this->allowed_authentification_override = 1;
 				$this->validated = 0;
 				$this->informations = array();
				break;
		}
	}
	
	public function is_in_database() {
		$query = 'SELECT * FROM mails_configuration
			WHERE name_mail_configuration = "'.addslashes($this->name).'"';
		$result = pmb_mysql_query($query);
		if(pmb_mysql_num_rows($result)) {
			return true;
		}
		return false;
	}
	
	public function check_configuration() {
		global $msg;
		
		$this->validated = 0;
		if($this->hote && (!$this->authentification || ($this->authentification && $this->user && $this->password))) {
			$mail = new mail();
			$param = array(
					'method' => strtolower($this->protocol),
					'host' => $this->hote.(!empty($this->port) ? ":".$this->port : ''),
					'auth' => $this->authentification,
					'user' => $this->user,
					'pass' => $this->password,
					'secure' => $this->secure_protocol,
					'auth_type' => $this->authentification_type
			);
			if(!empty($this->authentification_type_settings) && is_array($this->authentification_type_settings)) {
				$param = array_merge($param, $this->authentification_type_settings);
			}
			$mailer = $mail->get_instance_PHPMailer($param);
			$mailer->Timeout = 3;
			$mailer->SMTPDebug = 1;
			ob_start();
			if ($mailer->smtpConnect($mailer->SMTPOptions)) {
				$this->validated = 1;
			} else {
				$smtpConnect_error = str_replace(array('<br>', '<br />'), '. ', clean_string(html_entity_decode(ob_get_clean())));
				$this->informations['smtpConnect_error'] = $smtpConnect_error;
			}
		}
		$query = "update mails_configuration set mail_configuration_validated = ".$this->validated.", mail_configuration_informations = '".encoding_normalize::json_encode($this->informations)."' where  name_mail_configuration = '".addslashes($this->name)."'";
		pmb_mysql_query($query);
	}
	
	public function save() {
		if($this->is_in_database()){
			$query = "update mails_configuration set ";
			$clause = " where name_mail_configuration = '".addslashes($this->name)."'";
		}else{
			$query = "insert into mails_configuration set 
				mail_configuration_type = '".addslashes($this->type)."',
				name_mail_configuration = '".addslashes($this->name)."',
			";
			$clause= "";
		}
		$query.= "
			mail_configuration_protocol = '".addslashes($this->protocol)."',
			mail_configuration_hote = '".addslashes($this->hote)."',
			mail_configuration_port = '".$this->port."',
			mail_configuration_authentication = '".$this->authentification."',
			mail_configuration_user = '".addslashes($this->user)."',
			mail_configuration_password = '".addslashes(convert_uuencode($this->password))."',
			mail_configuration_secure_protocol = '".addslashes($this->secure_protocol)."',
			mail_configuration_authentification_type = '".addslashes($this->authentification_type)."',
			mail_configuration_authentification_type_settings = '".encoding_normalize::json_encode($this->authentification_type_settings)."',
			mail_configuration_allowed_hote_override = '".intval($this->allowed_hote_override)."',
			mail_configuration_allowed_authentification_override = '".intval($this->allowed_authentification_override)."',
			mail_configuration_validated = '".intval($this->validated)."',
			mail_configuration_informations = '".encoding_normalize::json_encode($this->informations)."'
			".$clause;
		
		$result = pmb_mysql_query($query);
		if($result){
			if($this->type == 'address' && $this->get_domain()->get_name()) {
				$this->fetch_domain_data();
				$this->check_configuration();
			} elseif($this->type == 'domain' && $this->name) {
				$this->upgrade_domain_childs();
				if(!$this->is_allowed_authentification_override()) {
					$this->check_configuration();
				}
			}
			return true;
		}
		return false;
	}
	
	public function upgrade_domain_childs() {
		$mails_configuration = list_mails_configuration_ui::get_instance()->get_objects();
		if(!empty($mails_configuration)) {
			foreach ($mails_configuration as $mail_configuration) {
				if($mail_configuration->get_id() && $mail_configuration->get_domain_name() == $this->name) {
					if(empty($this->allowed_hote_override)) {
						$mail_configuration->set_hote('')
							->set_port('')
							->set_secure_protocol('');
					} else {
						$mail_configuration->set_allowed_hote_override(1);
					}
					if(empty($this->allowed_authentification_override)) {
						$mail_configuration->set_authentification(0)
							->set_user('')
							->set_password('')
							->set_authentification_type('')
							->set_authentification_type_settings(array());
					} else {
						$mail_configuration->set_authentification(1)
							->set_allowed_authentification_override(1);
					}
					$mail_configuration->save();
				}
			}
		}
	}
	
	public static function delete($name) {
		$query = "delete from mails_configuration where name_mail_configuration = '".addslashes($name)."'";
		pmb_mysql_query($query);
		//Initialisation des mails du domaine
		$mail_configuration = new mail_configuration($name);
		$mail_configuration->upgrade_domain_childs();
		return true;
	}
	
	public function initialization() {
		$this->init_properties();
		if($this->type == 'domain') {
			$this->load_properties_from_generic_smtp_servers();
		}
		$this->save();
		if($this->type == 'domain') {
			//Initialisation des mails du domaine
			$this->upgrade_domain_childs();
		}
		return true;
	}
	
	public function get_id() {
		return $this->name;
	}
	
	public function get_type() {
		return $this->type;
	}
	
	public function get_name() {
		return $this->name;
	}
	
	public function get_domain() {
		if(!isset($this->domain)) {
			$this->domain = new mail_configuration($this->domain_name);
		}
		return $this->domain;
	}
	
	public function get_domain_name() {
		return $this->domain_name;
	}
	
	public function get_protocol() {
		return $this->protocol;
	}
	
	public function get_hote() {
		return $this->hote;
	}
	
	public function get_port() {
		return $this->port;
	}
	
	public function get_authentification() {
		return $this->authentification;
	}
	
	public function get_user() {
		return $this->user;
	}
	
	public function get_password() {
		return $this->password;
	}
	
	public function get_secure_protocol() {
		return $this->secure_protocol;
	}
	
	public function get_authentification_type() {
		return $this->authentification_type;
	}
	
	public function get_authentification_type_settings() {
		return $this->authentification_type_settings;
	}
	
	public function get_information($name) {
		if(!empty($this->informations[$name])) {
			return $this->informations[$name];
		}
		return '';
	}
	
	public function get_informations() {
		return $this->informations;
	}
	
	public function get_uses() {
		global $pmb_mail_adresse_from, $opac_mail_adresse_from, $opac_biblio_email;
		
		if(!isset($this->uses)) {
			$this->uses = array();
			$this->uses['users'] = users::get_users_from_mail($this->name);
			$locations = array();
			$query = "SELECT idlocation FROM docs_location WHERE email = '".addslashes($this->name)."'";
			$result = pmb_mysql_query($query);
			while ($row = pmb_mysql_fetch_object($result)) {
				$locations[$row->idlocation] = new docs_location($row->idlocation);
			}
			$this->uses['locations'] = $locations;
			
			$this->uses['parameters'] = array();
			if($pmb_mail_adresse_from) {
				$tmp_array_email = explode(';', $pmb_mail_adresse_from);
				if($tmp_array_email[0] == $this->name) {
					$this->uses['parameters'][] = 'pmb_mail_adresse_from';
				}
			}
			if($opac_mail_adresse_from) {
				$tmp_array_email = explode(';', $opac_mail_adresse_from);
				if($tmp_array_email[0] == $this->name) {
					$this->uses['parameters'][] = 'opac_mail_adresse_from';
				}
			}
			if($opac_biblio_email == $this->name) {
				$this->uses['parameters'][] = 'opac_biblio_email';
			}
		}
		return $this->uses;
	}
	
	public function set_type($type) {
		$this->type = $type;
		return $this;
	}
	
	public function set_name($name) {
		$this->name = $name;
		if(empty($this->type)) {
			if(strpos($this->name, '@') !== false) {
				$this->type = 'address';
			} else {
				$this->type = 'domain';
			}
		}
		return $this;
	}
	
	public function set_protocol($protocol) {
		$this->protocol = $protocol;
		return $this;
	}
	
	public function set_hote($hote) {
		$this->hote = $hote;
		return $this;
	}
	
	public function set_port($port) {
		$this->port = $port;
		return $this;
	}
	
	public function set_authentification($authentification) {
		$this->authentification = $authentification;
		return $this;
	}
	
	public function set_user($user) {
		$this->user = $user;
		return $this;
	}
	
	public function set_password($password) {
		$this->password = $password;
		return $this;
	}
	
	public function set_secure_protocol($secure_protocol) {
		$this->secure_protocol = $secure_protocol;
		return $this;
	}
	
	public function set_authentification_type($authentification_type) {
		$this->authentification_type = $authentification_type;
		return $this;
	}
	
	public function set_authentification_type_settings($authentification_type_settings) {
		$this->authentification_type_settings = $authentification_type_settings;
		return $this;
	}
	
	public function set_informations($informations) {
		$this->informations = $informations;
		return $this;
	}
	
	public function is_allowed_hote_override() {
		return $this->allowed_hote_override;
	}
	
	public function is_allowed_authentification_override() {
		return $this->allowed_authentification_override;
	}
	
	public function is_validated() {
		return $this->validated;
	}
	
	public function set_allowed_hote_override($allowed_hote_override) {
		$this->allowed_hote_override = intval($allowed_hote_override);
		return $this;
	}
	
	public function set_allowed_authentification_override($allowed_authentification_override) {
		$this->allowed_authentification_override = intval($allowed_authentification_override);
		return $this;
	}
	
	public function set_validated($validated) {
		$this->validated = intval($validated);
		return $this;
	}
	
	public function set_confidential($confidential) {
		$this->confidential = intval($confidential);
		return $this;
	}
	
	public function is_confidential() {
		global $pmb_mail_adresse_from, $opac_mail_adresse_from, $opac_biblio_email;
		global $PMBuseremail;
		
		if(!isset($this->confidential)) {
			$this->confidential = true;
			switch ($this->type) {
				case 'domain':
					$this->confidential = static::domain_is_confidential($this->name);
					break;
				case 'address':
				default:
					if($pmb_mail_adresse_from == $this->name || $opac_mail_adresse_from == $this->name || $opac_biblio_email == $this->name) {
						$this->confidential = false;
					}
					
					if($PMBuseremail == $this->name) {
						$this->confidential = false;
					}
					$query = "SELECT count(*) as nb FROM docs_location WHERE email = '".$this->name."'";
					$result = pmb_mysql_query($query);
					if(pmb_mysql_result($result, 0, 'nb')) {
						$this->confidential = false;
					}
					$query = "SELECT count(*) as nb FROM coordonnees JOIN entites ON entites.id_entite = coordonnees.num_entite WHERE type_entite = 1 AND email = '".$this->name."'";
					$result = pmb_mysql_query($query);
					if(pmb_mysql_result($result, 0, 'nb')) {
						$this->confidential = false;
					}
					break;
			}
		}
		return $this->confidential;
	}
	
	public function is_hote_readonly() {
		if($this->type == 'address' && empty($this->get_domain()->is_allowed_hote_override())) {
			return true;
		}
		return false;
	}
	
	public function is_authentification_readonly() {
		if($this->type == 'address' && empty($this->get_domain()->is_allowed_authentification_override())) {
			return true;
		}
		return false;
	}
	
	protected static function table_exists() {
		$query = "SHOW TABLES LIKE 'mails_configuration'";
		$result = pmb_mysql_query($query);
		if(pmb_mysql_num_rows($result)) {
			return true;
		}
		return false;
	}
	
	public static function get_address_configuration($address='') {
		if(!static::table_exists()) {
			return array();
		}
		$mail_configuration = new mail_configuration($address);
		if($mail_configuration->is_in_database() && $mail_configuration->get_hote() && $mail_configuration->is_validated()) {
			return array(
					strtolower($mail_configuration->get_protocol()),
					$mail_configuration->get_hote().(!empty($mail_configuration->get_port()) ? ":".$mail_configuration->get_port() : ''),
					$mail_configuration->get_authentification(),
					$mail_configuration->get_user(),
					$mail_configuration->get_password(),
					$mail_configuration->get_secure_protocol(),
					$mail_configuration->get_authentification_type()
			);
		} else {
			$mail_configuration = $mail_configuration->get_domain();
			if($mail_configuration->is_in_database() && $mail_configuration->get_hote() && $mail_configuration->is_validated()) {
				return array(
						strtolower($mail_configuration->get_protocol()),
						$mail_configuration->get_hote().(!empty($mail_configuration->get_port()) ? ":".$mail_configuration->get_port() : ''),
						$mail_configuration->get_authentification(),
						$mail_configuration->get_user(),
						$mail_configuration->get_password(),
						$mail_configuration->get_secure_protocol(),
						$mail_configuration->get_authentification_type()
				);
			}
		}
		return array();
	}
	
	public static function domain_is_confidential($domain) {
		global $pmb_mail_adresse_from, $opac_mail_adresse_from, $opac_biblio_email;
		global $PMBuseremail;
		
		$confidential = true;
		if (SESSrights & ADMINISTRATION_AUTH) {
			$confidential = false;
		}
		if(strpos($pmb_mail_adresse_from, $domain) !== false || strpos($opac_mail_adresse_from, $domain) !== false || strpos($opac_biblio_email, $domain) !== false) {
			$confidential = false;
		}
		if(strpos($PMBuseremail, $domain) !== false) {
			$confidential = false;
		}
		$query = "SELECT count(*) as nb FROM docs_location WHERE email LIKE '%".addslashes($domain)."'";
		$result = pmb_mysql_query($query);
		if(pmb_mysql_result($result, 0, 'nb')) {
			$confidential = false;
		}
		$query = "SELECT count(*) as nb FROM coordonnees JOIN entites ON entites.id_entite = coordonnees.num_entite WHERE type_entite = 1 AND email LIKE '%".addslashes($domain)."'";
		$result = pmb_mysql_query($query);
		if(pmb_mysql_result($result, 0, 'nb')) {
			$confidential = false;
		}
		return $confidential;
	}
}
	
