<?php
// +-------------------------------------------------+
// | 2002-2007 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_setting.class.php,v 1.4 2022/08/02 06:55:07 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

global $class_path, $include_path;
require_once($class_path."/mails/mails.class.php");
require_once($include_path."/templates/mails/mail_setting.tpl.php");

class mail_setting {
    
	protected $id;
	
	protected $classname;
	
	protected $sender;
	
	protected $copy_cc;
	
	protected $copy_bcc;
	
	protected $reply;
	
	protected $associated_campaign;
	
	protected $folder_path;
	
	public function __construct($id=0) {
		$this->id = intval($id);
		$this->fetch_data();
	}
	
	protected function init_properties() {
		$this->classname = '';
		$this->sender = '';
		$this->copy_cc = '';
		$this->copy_bcc = '';
		$this->reply = '';
		$this->associated_campaign = 0;
	}
	
	protected function fetch_data() {
		$this->init_properties();
		if(!static::table_exists()) {
			return false;
		}
		if($this->id) {
			$query = "select * from mails_settings where id_mail_setting = ".$this->id;
			$result = pmb_mysql_query($query);
			if(pmb_mysql_num_rows($result)) {
				$row = pmb_mysql_fetch_assoc($result);
				$this->classname = $row['mail_setting_classname'];
				$this->sender = $row['mail_setting_sender'];
				$this->copy_cc = $row['mail_setting_copy_cc'];
				$this->copy_bcc = $row['mail_setting_copy_bcc'];
				$this->reply = $row['mail_setting_reply'];
				$this->associated_campaign = $row['mail_setting_associated_campaign'];
			}
		}
	}
	
	protected function get_sender_selector_options() {
		global $msg;
	
		$options =array();
		if(strpos($this->classname, 'opac') !== false) {
			$options = array(
					'reader' => $msg['379'],
					'docs_location' => $msg['location'],
					'parameter' => $msg['opac_view_form_parameters'].' : biblio_name / biblio_email'
			);
		} else {
			$options = array(
					'user' => $msg['86'],
					'docs_location' => $msg['location'],
			);
			if(strpos($this->classname, 'accounting') !== false) {
				$options['accounting_bib_coords'] = $msg['acquisition_coord_lib'];
			}
		}
		return $options;
	}
	
	protected function get_sender_selector() {
		global $charset;
		
		$sender_selector = "<select name='mail_setting_sender'>";
		$options = $this->get_sender_selector_options();
		foreach ($options as $value=>$label) {
			$sender_selector .= "<option value='".$value."' ".($this->sender == $value ? "selected='selected'" : "").">".htmlentities($label, ENT_QUOTES, $charset)."</option>";
		}
		$sender_selector .= "</select>";
		return $sender_selector;
	}
	
	protected function get_copy_bcc_selector() {
		global $msg, $charset;
		
		$selector = "<select name='mail_setting_copy_bcc'>
						<option value='0' ".(empty($this->copy_bcc) ? "selected='selected'" : "").">".htmlentities($msg['39'], ENT_QUOTES, $charset)."</option>
						<option value='1' ".(!empty($this->copy_bcc) ? "selected='selected'" : "").">".htmlentities($msg['40'], ENT_QUOTES, $charset)."</option>
					</select>";
		return $selector;
	}
	
	protected function get_reply_selector() {
		global $msg, $charset;
		
		return "
			<select name='mail_setting_reply'>
					<option value='' ".(empty($this->reply) ? "selected='selected'" : "").">".htmlentities($msg['mail_setting_reply_unspecified'], ENT_QUOTES, $charset)."</option>
					<option value='user' ".($this->reply == 'user' ? "selected='selected'" : "").">".htmlentities($msg['86'], ENT_QUOTES, $charset)."</option>
			</select>";
	}
	
	protected function get_associated_campaign_selector() {
		global $msg, $charset;
		
		$selector = "<select name='mail_setting_associated_campaign'>
						<option value='0' ".(empty($this->associated_campaign) ? "selected='selected'" : "").">".htmlentities($msg['39'], ENT_QUOTES, $charset)."</option>
						<option value='1' ".(!empty($this->associated_campaign) ? "selected='selected'" : "").">".htmlentities($msg['40'], ENT_QUOTES, $charset)."</option>
					</select>";
		return $selector;
	}
	
	public function get_form() {
		global $mail_setting_content_form, $msg, $charset;
		
		$content_form = $mail_setting_content_form;
		
		$interface_form = new interface_form('mail_setting_form');
		$interface_form->set_label($msg['mail_setting_edit']);
		$content_form = str_replace('!!classname!!', htmlentities($this->get_label(), ENT_QUOTES, $charset), $content_form);
		$content_form = str_replace('!!sender_selector!!', $this->get_sender_selector(), $content_form);
		$content_form = str_replace('!!copy_cc!!', htmlentities($this->copy_cc, ENT_QUOTES, $charset), $content_form);
		$content_form = str_replace('!!copy_bcc_selector!!', $this->get_copy_bcc_selector(), $content_form);
		$content_form = str_replace('!!reply_selector!!', $this->get_reply_selector(), $content_form);
		$content_form = str_replace('!!associated_campaign_selector!!', $this->get_associated_campaign_selector(), $content_form);
		$interface_form->set_object_id(($this->is_in_database() ? 1 : 0))
		->set_confirm_delete_msg($msg['confirm_suppr_de']." ".$this->get_label()." ?")
		->set_content_form($content_form)
		->set_table_name('mails_settings');
		return $interface_form->get_display();
	}
	
	public function set_properties_from_form() {
		global $mail_setting_sender, $mail_setting_copy_cc, $mail_setting_copy_bcc, $mail_setting_reply;
		global $mail_setting_associated_campaign;
		
		$this->sender = stripslashes($mail_setting_sender);
		$this->copy_cc = stripslashes($mail_setting_copy_cc);
		$this->copy_bcc = stripslashes($mail_setting_copy_bcc);
		$this->reply = stripslashes($mail_setting_reply);
		$this->associated_campaign = intval($mail_setting_associated_campaign);
	}
	
	public function set_properties_from_folder($folder_path) {
		global $class_path;
		
		$group = str_replace($class_path.'/mail/', '', $folder_path);
		$instance = $this->classname::get_instance($group);
		$settings = $instance->get_settings();
		if(!empty($settings['sender'])) {
			$this->sender = $settings['sender'];
		}
		if(!empty($settings['copy_cc'])) {
			$this->copy_cc = $settings['copy_cc'];
		}
		if(!empty($settings['copy_bcc'])) {
			$this->copy_bcc = $settings['copy_bcc'];
		}
		if(!empty($settings['reply'])) {
			$this->reply = $settings['reply'];
		}
		if(!empty($settings['associated_campaign'])) {
			$this->associated_campaign = $settings['associated_campaign'];
		}
	}
	
	public function is_in_database() {
		$query = 'SELECT * FROM mails_settings
			WHERE id_mail_setting = '.$this->id;
		$result = pmb_mysql_query($query);
		if(pmb_mysql_num_rows($result)) {
			return true;
		}
		return false;
	}
	
	public function save() {
		if($this->is_in_database()){
			$query = "update mails_settings set ";
			$clause = " where id_mail_setting = ".$this->id;
		}else{
			$query = "insert into mails_settings set 
				id_mail_setting = ".$this->id.",
			";
			$clause= "";
		}
		$query.= "
			mail_setting_sender = '".addslashes($this->sender)."',
			mail_setting_copy_cc = '".addslashes($this->copy_cc)."',
			mail_setting_copy_bcc = '".addslashes($this->copy_bcc)."',
			mail_setting_reply = '".addslashes($this->reply)."',
			mail_setting_associated_campaign = '".intval($this->associated_campaign)."'
			".$clause;
		
		$result = pmb_mysql_query($query);
		if($result){
			return true;
		}
		return false;
	}
	
	public static function delete($id) {
		$id = intval($id);
		$query = "delete from mails_settings where id_mail_setting = '".$id."'";
		pmb_mysql_query($query);
		return true;
	}
	
	public function get_id() {
		return $this->id;
	}
	
	public function get_classname() {
		return $this->classname;
	}
	
	public function get_sender() {
		return $this->sender;
	}
	
	public function get_copy_cc() {
		return $this->copy_cc;
	}
	
	public function get_copy_bcc() {
		return $this->copy_bcc;
	}
	
	public function get_reply() {
		return $this->reply;
	}
	
	public function is_associated_campaign() {
		return $this->associated_campaign;
	}
	
	public function get_folder_path() {
		return $this->folder_path;
	}
	
	public function get_label() {
		$message = mails::get_message($this->classname);
		if($message) {
			return $message;
		}
		return $this->classname;
	}
	
	public function set_classname($classname) {
		$this->classname = $classname;
		return $this;
	}
	
	public function set_sender($sender) {
		$this->sender = $sender;
		return $this;
	}
	
	public function set_copy_cc($copy_cc) {
		$this->copy_cc = $copy_cc;
		return $this;
	}
	
	public function set_copy_bcc($copy_bcc) {
		$this->copy_bcc = $copy_bcc;
		return $this;
	}
	
	public function set_reply($reply) {
		$this->reply = $reply;
		return $this;
	}
	
	public function set_associated_campaign($associated_campaign) {
		$this->associated_campaign = $associated_campaign;
		return $this;
	}
	
	public function set_folder_path($folder_path) {
		$this->folder_path = $folder_path;
		return $this;
	}
	
	public function is_confidential() {
		return false;
	}
	
	public static function get_id_from_classname($classname) {
		if(!static::table_exists()) {
			return 0;
		}
		$query = "SELECT id_mail_setting FROM mails_settings WHERE mail_setting_classname = '".addslashes($classname)."'";
		$result = pmb_mysql_query($query);
		if(pmb_mysql_num_rows($result)) {
			return pmb_mysql_result($result, 0, 'id_mail_setting');
		}
		return 0;
	}
	
	protected static function table_exists() {
		$query = "SHOW TABLES LIKE 'mails_settings'";
		$result = pmb_mysql_query($query);
		if(pmb_mysql_num_rows($result)) {
			return true;
		}
		return false;
	}
}
	
