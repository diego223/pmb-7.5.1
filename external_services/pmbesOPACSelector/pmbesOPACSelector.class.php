<?php
// +-------------------------------------------------+
// | 2002-2007 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: pmbesOPACSelector.class.php,v 1.3 2023/02/17 15:02:53 dbellamy Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

global $class_path;

require_once "$class_path/external_services.class.php";
require_once "$class_path/encoding_normalize.class.php";

require_once "$class_path/thesaurus.class.php";
require_once "$class_path/categories.class.php";
require_once "$class_path/search_authorities.class.php";

class pmbesOPACSelector extends external_services_api_class 
{
    //Types de selecteurs pris en charge
    const TYPES_AVAILABLE = [
        'categories',
        'authors',
        'authors_person',
        'congres_name',
        'collectivite_name',
        'publishers',
        'titres_uniformes',
        'collections',
        'subcollections',
        'indexint',
        'serie'
    ];
    
    //Nombre max de resultats retournes
    const MAX_RESULTS_DEFAULT = 20;
    
    
    protected $max_results = 20;
    
    
    /**
     * Point d'entree 
     * @param string $type : type de selecteur 
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude : filtre d'exclusion 
     * @return array [id, label]
     */
    public function get($type, $search, $filter = '' , $exclude = '')
    {
        $response = [];
        //nettoyage $type
        if( !in_array($type, pmbesOPACSelector::TYPES_AVAILABLE) ) {
            return encoding_normalize::utf8_normalize($response);
        }
        
        $this->max_results = pmbesOPACSelector::MAX_RESULTS_DEFAULT;
        
        switch($type) {
            
            case 'categories' :
                $response = $this->getCategories($search, $filter, $exclude);
                break;
            case 'authors' :
                $response = $this->getAuthors($search, $filter, $exclude);
                break;
            case 'authors_person' :
                $response = $this->getAuthorsPerson($search, $filter, $exclude);
                break;
            case 'congres_name' :
                $response = $this->getCongresName($search, $filter, $exclude);
                break;
            case 'collectivite_name' :
                $response = $this->getCollectiviteName($search, $filter, $exclude);
                break;
            case 'publishers' :
                $response = $this->getPublishers($search, $filter, $exclude);
                break;
            case 'titres_uniformes' :
                $response = $this->getTitresUniformes($search, $filter, $exclude);
                break;
            case 'collections' :
                $response = $this->getCollections($search, $filter, $exclude);
                break;
            case 'subcollections' :
                $response = $this->getSubCollections($search, $filter, $exclude);
                break;
            case 'indexint' :
                $response = $this->getIndexint($search, $filter, $exclude);
                break;
            case 'serie' :
                $response = $this->getSerie($search, $filter, $exclude);
                break;
        }

        return encoding_normalize::utf8_normalize($response);
    }
    
    
    /**
     * Selecteur serie
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getSerie($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = inutilise
        $filter = intval($filter);
        
        //nettoyage $exclude = ids series a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $param1 = '';
        $equation_filters = search_authorities::get_join_and_clause_from_equation(AUT_TABLE_SERIES, $param1);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND serie_id not in ($autexclude) ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        
        $requete = "select serie_id as id, ";
        $requete.= "serie_name as label ";
        $requete.= "from series ".$equation_filters['join']." where serie_name like '".addslashes($start)."%' ".$restrict." ".$equation_filters['clause']." ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur indexation
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getIndexint($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = id plan de classement
        $filter = intval($filter);
        
        //nettoyage $exclude = ids indexations a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $param1 = '';
        $equation_filters = search_authorities::get_join_and_clause_from_equation(AUT_TABLE_INDEXINT, $param1);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND indexint_id not in ($autexclude) ";
        }
        if ($filter) {
            $restrict.= " AND num_pclass = ".$filter." ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select indexint_id as id, ";
        $requete.= "if(indexint_comment is not null and indexint_comment!='',concat(indexint_name,' : ',indexint_comment), indexint_name) as label ";
        $requete.= "from indexint ".$equation_filters['join']." ";
        $requete.= "where if(indexint_comment is not null and indexint_comment!='',concat(indexint_name,' - ',indexint_comment),indexint_name) like '".addslashes($start)."%' ".$restrict." ".$equation_filters['clause']." ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur sous-collections
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getSubCollections($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = id collection parent
        $filter = intval($filter);
        
        //nettoyage $exclude = ids sous-collections a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $param1 = '';
        $equation_filters = search_authorities::get_join_and_clause_from_equation(AUT_TABLE_SUB_COLLECTIONS, $param1);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND sub_coll_id not in ($autexclude) ";
        }
        if ($filter) {
            $restrict .= " AND sub_coll_parent = ".$filter;
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select sub_coll_id as id, ";
        $requete.= "if(sub_coll_issn is not null and sub_coll_issn!='',concat(sub_coll_name,', ',sub_coll_issn),sub_coll_name) as label ";
        $requete.= "from sub_collections ".$equation_filters['join']." ";
        $requete.= "where if(sub_coll_issn is not null and sub_coll_issn!='',concat(sub_coll_name,', ',sub_coll_issn),sub_coll_name) like '".addslashes($start)."%' ".$restrict." ".$equation_filters['clause']." ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur collections
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getCollections($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = id collection parent
        $filter = intval($filter);
        
        //nettoyage $exclude = ids collections a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $param1 = '';
        $equation_filters = search_authorities::get_join_and_clause_from_equation(AUT_TABLE_COLLECTIONS, $param1);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND collection_id not in ($autexclude) ";
        }
        if ($filter) {
            $restrict .= " AND collection_parent = ".$filter;
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select collection_id as id, ";
        $requete.= "if(collection_issn is not null and collection_issn!='', concat(collection_name,', ',collection_issn),collection_name) as label ";
        $requete.= "from collections ".$equation_filters['join']." ";
        $requete.= "where if(collection_issn is not null and collection_issn!='', concat(collection_name,', ',collection_issn),collection_name) like '".addslashes($start)."%' ".$restrict." ".$equation_filters['clause']." ";
        $requete.= "order by index_coll limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur titres uniformes
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getTitresUniformes($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = inutilise
        $filter = '';
        
        //nettoyage $exclude = ids titres uniformes a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $param1 = '';
        $equation_filters = search_authorities::get_join_and_clause_from_equation(AUT_TABLE_TITRES_UNIFORMES, $param1);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND tu_id not in ($autexclude) ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select tu_id as id, ";
        $requete.= "if(tu_comment is not null and tu_comment!='', concat(tu_name,' : ',tu_comment),tu_name) as label ";
        $requete.= "from titres_uniformes ".$equation_filters['join']." ";
        $requete.= "where if(tu_comment is not null and tu_comment!='',concat(tu_name,' - ',tu_comment),tu_name) like '".addslashes($start)."%' ".$restrict." ".$equation_filters['clause']." ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur editeurs
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getPublishers($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = inutilise
        $filter = '';
        
        //nettoyage $exclude = id editeurs a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $param1 = '';
        $equation_filters = search_authorities::get_join_and_clause_from_equation(AUT_TABLE_PUBLISHERS, $param1);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND ed_id not in ($autexclude) ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete="select ed_id as id, ";
        $requete.= "concat(
            ed_name,
            if((ed_ville is not null and ed_ville!='') or (ed_pays is not null and ed_pays!=''),' (',''),
            if(ed_ville is not null and ed_ville!='',ed_ville,''),
            if(ed_ville is not null and ed_ville!='' and ed_pays is not null and ed_pays!='',' - ',''),
            if(ed_pays is not null and ed_pays!='',ed_pays,''),
            if((ed_ville is not null and ed_ville!='') or (ed_pays is not null and ed_pays!=''),')','')
            ) as label ";
        $requete.= "from publishers ".$equation_filters["join"]." ";
        $requete.= "where concat(
            ed_name,
            if((ed_ville is not null and ed_ville!='') or (ed_pays is not null and ed_pays!=''),' (',''),
            if(ed_ville is not null and ed_ville!='',ed_ville,''),
            if(ed_ville is not null and ed_ville!='' and ed_pays is not null and ed_pays!='',' - ',''),
            if(ed_pays is not null and ed_pays!='',ed_pays,''),
            if((ed_ville is not null and ed_ville!='') or (ed_pays is not null and ed_pays!=''),')','')
            ) like '".addslashes($start)."%' ".$restrict." ".$equation_filters["clause"]." ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur auteurs de type collectivite
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getCollectiviteName($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = inutilise
        $filter = '';
        
        //nettoyage $exclude = id auteurs a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND author_id not in ($autexclude) ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select author_id as id, ";
        $requete.= "author_name as label ";
        $requete.= "from authors ";
        $requete.= "where author_type='71' and author_name like '".addslashes($start)."%' $restrict ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur auteurs de type congres
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getCongresName($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = inutilise
        $filter = '';
        
        //nettoyage $exclude = id auteurs a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND author_id not in ($autexclude) ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select author_id as id, ";
        $requete.= "author_name as label ";
        $requete.= "from authors ";
        $requete.= "where author_type='72' and author_name like '".addslashes($start)."%' $restrict ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur auteurs de type personne
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getAuthorsPerson($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = inutilise
        $filter = '';
        
        //nettoyage $exclude = id auteurs a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND author_id not in ($autexclude) ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select author_id as id, ";
        $requete.= "if(author_date!='',concat(if(author_rejete is not null and author_rejete!='',concat(author_name,', ',author_rejete),author_name),' (',author_date,')'),if(author_rejete is not null and author_rejete!='',concat(author_name,', ',author_rejete),author_name)) as label ";
        $requete.= "from authors ";
        $requete.= "where author_type='70' and if(author_rejete is not null and author_rejete!='',concat(author_name,', ',author_rejete),author_name) like '".addslashes($start)."%' $restrict ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur auteurs
     *
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion
     * @return array [id, label]
     */
    protected function getAuthors($search, $filter, $exclude)
    {
        $response = [];
        
        //nettoyage $filter = inutilise
        $filter = '';
        
        //nettoyage $exclude = id auteurs a exclure
        $tab_exclude = explode(',', $exclude);
        $tab_autexclude = [];
        foreach($tab_exclude as $v) {
            $v = intval($v);
            if($v) {
                $tab_autexclude[] =  $v;
            }
        }
        $autexclude = implode(',', $tab_autexclude);
        unset($tab_autexclude);
        
        $param1 = '';
        $equation_filters=search_authorities::get_join_and_clause_from_equation(AUT_TABLE_AUTHORS, $param1);
        
        $restrict = '';
        if ($autexclude) {
            $restrict = " AND author_id not in ($autexclude) ";
        }
        
        //Recherche "commence par"
        $start=stripslashes($search);
        $start = str_replace("*","%",$start);
        $requete = "select author_id as id, ";
        $requete.= "if(author_date!='', concat(if(author_rejete is not null and author_rejete!='', concat(author_name,', ',author_rejete),author_name),' (',author_date,')'), if(author_rejete is not null and author_rejete!='',concat(author_name,', ',author_rejete),author_name)) as label ";
        $requete.= "from authors ".$equation_filters['join']." ";
        $requete.= "where if(author_rejete is not null and author_rejete!='',concat(author_name,', ',author_rejete),author_name) like '".addslashes($start)."%' ".$restrict.$equation_filters['clause']." ";
        $requete.= "order by label limit ".$this->max_results;
        
        $res = pmb_mysql_query($requete);
        
        if(pmb_mysql_num_rows($res)) {
            while(($row = pmb_mysql_fetch_object($res)) ) {
                $response[] = [
                    'id' => $row->id,
                    'label' => $row->label,
                ];
            }
        }
        return $response;
    }
    
    
    /**
     * Selecteur categories
     * 
     * @param string $search : recherche
     * @param string $filter : filtre de recherche
     * @param string $exclude  : filtre d'exclusion 
     * @return array [id, label, short_label]
     */
    protected function getCategories($search, $filter, $exclude)
    {
        global $opac_thesaurus, $opac_thesaurus_defaut, $opac_categories_show_only_last, $thesaurus_liste_trad;
        global $lang;
        
        $response = [];
        
        //Liste des ids de categories deja traites
        $tab_categ_ids = [];
        
        //nettoyage $filter = ids des thesaurus a explorer
        $tab_filter = explode(',', $filter);
        $tab_thesaurus_filter = [];
        foreach($tab_filter as $v) {
            $v = intval($v);
            if($v) {
                $tab_thesaurus_filter[] =  $v;
            }
        }
        unset($tab_filter);

        //nettoyage $exclude = id categorie a exclure
        $autexclude = intval($exclude);
        
        $param1 = '';
        $equation_filters = search_authorities::get_join_and_clause_from_equation(AUT_TABLE_CATEG, $param1);
        
        // mono / multi thesaurus
        $multi_thesaurus = true;
        $thesaurus_where_clause = "";
        $tab_thesaurus = [];
        $id_thesaurus = 0;
        
        if (!$opac_thesaurus) {
            $multi_thesaurus = false;
            $id_thesaurus = $opac_thesaurus_defaut;
            $thesaurus_where_clause = "id_thesaurus = $id_thesaurus and ";
            $tab_thesaurus[$id_thesaurus] = new thesaurus($id_thesaurus);
        }
        
        //en mode multi thesaurus, on peut definir la liste des thesaurus a explorer
        $nb_thesaurus = 1;
        if($multi_thesaurus) {
            $nb_thesaurus = count($tab_thesaurus_filter);
            if($nb_thesaurus == 1) {
                $id_thesaurus = $tab_thesaurus_filter[0];
                $thesaurus_where_clause = "id_thesaurus = $id_thesaurus and ";
                $multi_thesaurus = false;
                $tab_thesaurus[$id_thesaurus] = new thesaurus($id_thesaurus);
            } 
            if($nb_thesaurus > 1 ) {
               $thesaurus_where_clause = "id_thesaurus in (".implode(',', $tab_thesaurus_filter).") and ";
            }
        }
        
        //liste des langues definies pour les thesaurus
        $tab_thesaurus_translation_list = explode(',', $thesaurus_liste_trad);
    
        //Recherche "commence par"
        $start = stripslashes($search);
        $start = str_replace("*","%",$start);
        $aq=new analyse_query($start);
        $members_catdef = $aq->get_query_members("catdef", "catdef.libelle_categorie", "catdef.index_categorie", "catdef.num_noeud");
        $members_catlg = $aq->get_query_members("catlg", "catlg.libelle_categorie", "catlg.index_categorie", "catlg.num_noeud");
        
        $requete = "SELECT noeuds.id_noeud AS categ_id, noeuds.num_renvoi_voir as categ_see, noeuds.num_thesaurus, noeuds.not_use_in_indexation, ";
        //1 seul thesaurus / 1 seule langue
        if( $id_thesaurus && ( ($lang == $tab_thesaurus[$id_thesaurus]->langue_defaut) || !in_array($lang, $tab_thesaurus_translation_list)  ) ) {
            $requete.= "catdef.langue as langue, ";
            $requete.= "catdef.libelle_categorie as categ_libelle, ";
            $requete.= "catdef.index_categorie as index_categorie, ";
            $requete.= "(".$members_catdef["select"].") as pert ";
            $requete.= "FROM noeuds ";
            $requete.= "JOIN categories as catdef on noeuds.id_noeud = catdef.num_noeud AND catdef.langue = '".$tab_thesaurus[$id_thesaurus]->langue_defaut."' ".$equation_filters['join']." ";
            $requete.= "WHERE noeuds.num_thesaurus = ".$id_thesaurus." and catdef.libelle_categorie like '".addslashes($start)."%' ";
            $requete.= $equation_filters['clause']." ";
            if ($autexclude) {
                $requete.= "AND noeuds.id_noeud != $autexclude AND (noeuds.path NOT LIKE '$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude') ";
            }
            $requete.= "order by pert desc, num_thesaurus, categ_libelle ";
        //+sieurs thesaurus / +sieurs langues
        } else {
            $requete.= "if (catlg.num_noeud is null, catdef.langue , catlg.langue) as langue, ";
            $requete.= "if (catlg.num_noeud is null, catdef.libelle_categorie, catlg.libelle_categorie ) as categ_libelle, ";
            $requete.= "if (catlg.num_noeud is null, catdef.index_categorie , catlg.index_categorie ) as index_categorie, ";
            $requete.= "if (catlg.num_noeud is null, ".$members_catdef["select"].", ".$members_catlg["select"].") as pert ";
            $requete.= "FROM thesaurus ";
            $requete.= "JOIN noeuds ON thesaurus.id_thesaurus = noeuds.num_thesaurus ".$equation_filters['join']." ";
            $requete.= "LEFT JOIN categories as catdef on noeuds.id_noeud = catdef.num_noeud AND catdef.langue=thesaurus.langue_defaut ";
            $requete.= "LEFT JOIN categories as catlg on catdef.num_noeud=catlg.num_noeud and catlg.langue = '".$lang."' ";
            $requete.= "WHERE $thesaurus_where_clause if(catlg.num_noeud is null, catdef.libelle_categorie like '".addslashes($start)."%', catlg.libelle_categorie like '".addslashes($start)."%') ";
            $requete.= $equation_filters['clause']." ";
            if ($autexclude) {
                $requete.= "AND noeuds.id_noeud != $autexclude AND (noeuds.path NOT LIKE '$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude') ";
            }
            $requete.= "order by pert desc, thesaurus_order, num_thesaurus, categ_libelle ";
        }

        $res = pmb_mysql_query($requete);
        
        $n = $this->max_results;
        
        if(pmb_mysql_num_rows($res)) {
            
            while( $n && ($categ = pmb_mysql_fetch_object($res)) ) {
                
                if( !in_array($categ->num_noeud, $tab_categ_ids) ) {
                    
                    $thesaurus_label = '';
                    $categ_id = $categ->categ_id;
                    
                    $categ_label = $categ->categ_libelle;
                    $short_categ_label = $categ->categ_libelle;
                    $categ_see_label = '';
                    $short_categ_see_label = '';
                    $categ_path = '';
                    $categ_not_use_in_indexation = $categ->not_use_in_indexation;
                    
                    //Libelle thesaurus en mode multi_thesaurus
                    if( $multi_thesaurus ) {
                        if (!array_key_exists($categ->num_thesaurus, $tab_thesaurus) ) {
                            $tab_thesaurus[$categ->num_thesaurus] = new thesaurus($categ->num_thesaurus);
                        }
                        $thesaurus_label = $tab_thesaurus[$categ->num_thesaurus]->libelle_thesaurus;
                    }
                    
                    //categorie renvoyee
                    if( $categ->categ_see ) {
                        $categ_id = $categ->categ_see;
                        $category = new category($categ_id);
                        $categ_not_use_in_indexation = $category->not_use_in_indexation;
                        $temp = new categories($categ->categ_see, $categ->langue);
                        $categ_see_label = $temp->libelle_categorie;
                        $short_categ_see_label = $temp->libelle_categorie;
                        $categ_path = categories::listAncestorNames($categ->categ_see, $categ->langue);
                        if( !$opac_categories_show_only_last) {
                            $categ_see_label = $categ_path;
                        }
                    } else {
                        $categ_path = categories::listAncestorNames($categ_id, $categ->langue);
                        if( !$opac_categories_show_only_last) {
                            $categ_label = $categ_path;
                        } 
                    }
                    
                    if( !in_array($categ_id, $tab_categ_ids) && !$categ_not_use_in_indexation &&  !preg_match("#:~|^~#i",$categ_path) ) {
                        
                        $final_categ_label = !empty($thesaurus_label) ? '['.$thesaurus_label.'] ' : '';
                        $short_final_categ_label = '';
                        if( empty($categ_see_label) ) {
                            $final_categ_label.= $categ_label;
                            $short_final_categ_label = $short_categ_label;
                        } else {
                            $final_categ_label.= $categ_label.' -> '.$categ_see_label .'@';
                            $short_final_categ_label = $short_categ_label.' -> '.$short_categ_see_label .'@';
                        }
                        
                        $response[] = [
                            'id' => $categ_id,
                            'label' => $final_categ_label,
                            'short_label' => $short_final_categ_label,
                        ];
                        
                        $tab_categ_ids[] = $categ_id;
                        $n--;
                    }
                }
            }
        }
        
        //Si pas assez de resultats, on complete
        if( count($response) < $this->max_results ) {
            
            //Recherche "contient"
            $aq = new analyse_query(stripslashes($search)."*");
            $members_catdef = $aq->get_query_members("catdef", "catdef.libelle_categorie", "catdef.index_categorie", "catdef.num_noeud");
            $members_catlg = $aq->get_query_members("catlg", "catlg.libelle_categorie", "catlg.index_categorie", "catlg.num_noeud");
            $requete = '';
            
            if (!$aq->error) {
                $requete = "SELECT noeuds.id_noeud AS categ_id, noeuds.num_renvoi_voir as categ_see, noeuds.num_thesaurus, noeuds.not_use_in_indexation, ";
                //1 seul thesaurus / 1 seule langue
                if( $id_thesaurus && ( ($lang == $tab_thesaurus[$id_thesaurus]->langue_defaut) || !in_array($lang, $tab_thesaurus_translation_list)  ) ) {
                    $requete.= "catdef.langue as langue, ";
                    $requete.= "catdef.libelle_categorie as categ_libelle, ";
                    $requete.= "catdef.index_categorie as index_categorie, ";
                    $requete.= "(".$members_catdef["select"].") as pert ";
                    $requete.= "FROM noeuds ";
                    $requete.= "JOIN categories as catdef on noeuds.id_noeud = catdef.num_noeud AND  catdef.langue = '".$tab_thesaurus[$id_thesaurus]->langue_defaut."' ".$equation_filters['join']." ";
                    $requete.= "WHERE noeuds.num_thesaurus='".$id_thesaurus."' and catdef.libelle_categorie not like '~%' and ".$members_catdef["where"]." ";
                    $requete.= $equation_filters['clause']." ";
                    if ($autexclude) {
                        $requete.= "AND noeuds.id_noeud != $autexclude AND (noeuds.path NOT LIKE '$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude') ";
                    }
                    $requete.= "order by pert desc, num_thesaurus, categ_libelle";
                //+sieurs thesaurus / +sieurs langues
                } else {
                    $requete.= "if (catlg.num_noeud is null, catdef.langue , catlg.langue) as langue, ";
                    $requete.= "if (catlg.num_noeud is null, catdef.libelle_categorie , catlg.libelle_categorie ) as categ_libelle, ";
                    $requete.= "if (catlg.num_noeud is null, catdef.index_categorie , catlg.index_categorie ) as index_categorie, ";
                    $requete.= "if (catlg.num_noeud is null, ".$members_catdef["select"].", ".$members_catlg["select"].") as pert ";
                    $requete.= "FROM thesaurus ";
                    $requete.= "JOIN noeuds ON thesaurus.id_thesaurus = noeuds.num_thesaurus ".$equation_filters['join']." ";
                    $requete.= "LEFT JOIN categories as catdef on noeuds.id_noeud = catdef.num_noeud AND catdef.langue=thesaurus.langue_defaut ";
                    $requete.= "LEFT JOIN categories as catlg on catdef.num_noeud=catlg.num_noeud and catlg.langue = '".$lang."' ";
                    $requete.= "WHERE $thesaurus_where_clause if(catlg.num_noeud is null, ".$members_catdef["where"].", ".$members_catlg["where"].") ";
                    $requete.= $equation_filters['clause']." ";
                    if ($autexclude) {
                        $requete.= "AND noeuds.id_noeud != $autexclude AND (noeuds.path NOT LIKE '$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude/%' AND noeuds.path NOT LIKE '%/$autexclude') ";
                    }
                    $requete.= "order by pert desc, thesaurus_order, num_thesaurus, categ_libelle";
                }
                
            }

            if (!$requete) {
                return $response;
            }
            
            $res = pmb_mysql_query($requete);
            
            if(pmb_mysql_num_rows($res)) {
                
                while( $n && ($categ = pmb_mysql_fetch_object($res)) ) {
                    
                    if( !in_array($categ->num_noeud, $tab_categ_ids) ) {
                        
                        $thesaurus_label = '';
                        $categ_id = $categ->categ_id;
                        $categ_label = $categ->categ_libelle;
                        $short_categ_label = $categ->categ_libelle;
                        $categ_see_label = '';
                        $short_categ_see_label = '';
                        $categ_path = '';
                        $categ_not_use_in_indexation = $categ->not_use_in_indexation;
                        
                        //Libelle thesaurus en mode multi_thesaurus
                        if( $multi_thesaurus ) {
                            if (!array_key_exists($categ->num_thesaurus, $tab_thesaurus) ) {
                                $tab_thesaurus[$categ->num_thesaurus] = new thesaurus($categ->num_thesaurus);
                            }
                            $thesaurus_label = $tab_thesaurus[$categ->num_thesaurus]->libelle_thesaurus;
                        }
                        
                        //categorie renvoyee
                        if( $categ->categ_see ) {
                            $categ_id = $categ->categ_see;
                            $category = new category($categ_id);
                            $categ_not_use_in_indexation = $category->not_use_in_indexation;
                            $temp = new categories($categ->categ_see, $categ->langue);
                            $categ_see_label = $temp->libelle_categorie;
                            $short_categ_see_label = $temp->libelle_categorie;
                            $categ_path = categories::listAncestorNames($categ->categ_see, $categ->langue);
                            if( !$opac_categories_show_only_last) {
                                $categ_see_label = $categ_path;
                            }
                        } else {
                            $categ_path = categories::listAncestorNames($categ->categ_id, $categ->langue);
                            if( !$opac_categories_show_only_last) {
                                $categ_label = $categ_path;
                            }
                        }
                        
                        if( !in_array($categ_id, $tab_categ_ids) && !$categ_not_use_in_indexation &&  !preg_match("#:~|^~#i",$categ_path) ) {
                            
                            $final_categ_label = !empty($thesaurus_label) ? '['.$thesaurus_label.'] ' : '';
                            $short_final_categ_label = '';
                            if( empty($categ_see_label) ) {
                                $final_categ_label.= $categ_label;
                                $short_final_categ_label = $short_categ_label;
                            } else {
                                $final_categ_label.= $categ_label.' -> '.$categ_see_label .'@';
                                $short_final_categ_label = $short_categ_label.' -> '.$short_categ_see_label .'@';
                            }
                            
                            $response[] = [
                                'id' => $categ_id,
                                'label' => $final_categ_label,
                                'short_label' => $short_final_categ_label,
                            ];
                            
                            $tab_categ_ids[] = $categ_id;
                            $n--;
                        }
                    }
                }
           }
        }
        return $response;
        
    }
    
}
