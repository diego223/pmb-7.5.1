<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: form.tpl.php,v 1.1 2022/07/22 15:36:14 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".tpl.php")) die("no access");

global $msg, $form_content_form;

$form_content_form = "
<div class='row'>
	<label class='etiquette' for='form_autorisations_all'>".$msg["form_autorisations_all"]."</label>
	<input type='checkbox' id='form_autorisations_all' name='form_autorisations_all' value='1' !!autorisations_all!! />
</div>
<div class='row'>
	<label class='etiquette' for='form_autorisations'>".$msg['form_autorisations']."</label>
	<input type='button' class='bouton_small align_middle' value='".$msg['tout_cocher_checkbox']."' onclick='check_checkbox(document.getElementById(\"auto_id_list\").value,1);'>
	<input type='button' class='bouton_small align_middle' value='".$msg['tout_decocher_checkbox']."' onclick='check_checkbox(document.getElementById(\"auto_id_list\").value,0);'>
</div>
<div class='row'>
	!!autorisations_users!!
</div>
<div class='row'>
	<label class='etiquette' for='form_duplicable'>".$msg["form_duplicable"]."</label>
	<input type='checkbox' id='form_duplicable' name='form_duplicable' value='1' !!duplicable!! />
</div>
<div class='row'>
	<label class='etiquette' for='form_deletable_on_auth'>".$msg["form_deletable_on_auth"]."</label>
	<input type='checkbox' id='form_deletable_on_auth' name='form_deletable_on_auth' value='1' !!deletable_on_auth!! />
</div>
<input type='hidden' id='form_model_name' name='form_model_name' value='!!model_name!!' />
<input type='hidden' id='form_module' name='form_module' value='!!module!!' />";

?>