<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_configuration.tpl.php,v 1.4 2023/01/20 15:13:55 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".tpl.php")) die("no access");

global $mail_configuration_domain_content_form, $mail_configuration_address_content_form, $msg;
global $mail_configuration_protocol_content_form, $mail_configuration_hote_content_form, $mail_configuration_secure_protocol_content_form; 
global $mail_configuration_hote_read_form, $mail_configuration_secure_protocol_read_form;
global $mail_configuration_authentification_content_form, $mail_configuration_authentification_user_pwd_content_form;
global $mail_configuration_authentification_type_content_form;
global $mail_configuration_authentification_type_settings_content_form, $mail_configuration_authentification_type_settings_xoauth2_content_form;
global $mail_configuration_allowed_overrides_content_form;

$mail_configuration_domain_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_domain'>".$msg['mail_configuration_domain']."</label>
</div>
<div class='row'>
	!!name!!
	<input type='hidden' id='name' name='name' value='!!name!!' />
</div>
";

$mail_configuration_address_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_address'>".$msg['mail_configuration_address']."</label>
</div>
<div class='row'>
	!!name!!
	<input type='hidden' id='name' name='name' value='!!name!!' />
</div>
";

$mail_configuration_protocol_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_protocol'>".$msg['mail_configuration_protocol']."</label>
</div>
<div class='row'>
	!!protocol_selector!!
</div>
";

$mail_configuration_hote_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_hote'>".$msg['mail_configuration_hote']." !!auto_fill!!</label>
</div>
<div class='row'>
	<input type='text' class='saisie-50em' name='mail_configuration_hote' id='mail_configuration_hote' value='!!hote!!' !!hote_readonly!!/>
</div>
<div class='row'>
	<label class='etiquette' for='mail_configuration_port'>".$msg['mail_configuration_port']." !!auto_fill!!</label>
</div>
<div class='row'>
	<input type='text' class='saisie-10em' name='mail_configuration_port' id='mail_configuration_port' value='!!port!!' !!hote_readonly!!/>
</div>
";

$mail_configuration_hote_read_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_hote'>".$msg['mail_configuration_hote']."</label>
</div>
<div class='row'>
	!!hote!!
</div>
<div class='row'>
	<label class='etiquette' for='mail_configuration_port'>".$msg['mail_configuration_port']."</label>
</div>
<div class='row'>
	!!port!!
</div>
";

$mail_configuration_secure_protocol_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_secure_protocol'>".$msg['mail_configuration_secure_protocol']." !!auto_fill!!</label>
</div>
<div class='row'>
	!!secure_protocol_selector!!
</div>";

$mail_configuration_secure_protocol_read_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_secure_protocol'>".$msg['mail_configuration_secure_protocol']."</label>
</div>
<div class='row'>
	!!secure_protocol!!
</div>";

$mail_configuration_authentification_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_authentification'>".$msg['mail_configuration_authentification']."</label>
</div>
<div class='row'>
	<input type='radio' name='mail_configuration_authentification' id='mail_configuration_authentification_none' value='0' !!authentification_none!! />
	<label for='mail_configuration_authentification_none'>".$msg['mail_configuration_authentification_none']."</label>
	<br />
	<input type='radio' name='mail_configuration_authentification' id='mail_configuration_authentification_domain_reserved' value='1' !!authentification_domain_reserved!! />
	<label for='mail_configuration_authentification_domain_reserved'>".$msg['mail_configuration_authentification_domain_reserved']."</label>
	<br />
	<input type='radio' name='mail_configuration_authentification' id='mail_configuration_authentification_mail_reserved' value='2' !!authentification_mail_reserved!! />
	<label for='mail_configuration_authentification_mail_reserved'>".$msg['mail_configuration_authentification_mail_reserved']."</label>
</div>
";

$mail_configuration_authentification_user_pwd_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_user'>".$msg['mail_configuration_user']."</label>
</div>
<div class='row'>
	<input type='text' class='saisie-50em' name='mail_configuration_user' id='mail_configuration_user' value='!!user!!' !!authentification_readonly!! />
</div>
<div class='row'>
	<label class='etiquette' for='mail_configuration_password'>".$msg['mail_configuration_password']."</label>
</div>
<div class='row'>
	<input type='password' class='saisie-50em' name='mail_configuration_password' id='mail_configuration_password' value='' placeholder='!!password_placeholder!!' !!authentification_readonly!! autocomplete='new-password' />
	<span class='fa fa-eye' onclick='toggle_password(this, \"mail_configuration_password\");'></span>
</div>";

$mail_configuration_authentification_type_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_authentification_type'>".$msg['mail_configuration_authentification_type']."</label>
</div>
<div class='row'>
	!!authentification_type_selector!!
</div>
";

$mail_configuration_authentification_type_settings_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_authentification_type_settings'>".$msg['mail_configuration_authentification_type_settings']."</label>
</div>
<div class='row'>
	!!authentification_type_settings!!
</div>
";

// $mail_configuration_authentification_type_settings_xoauth2_content_form = "
// <div class='row'>
// 	<label class='etiquette' for='xoauth2_provider'>".$msg['xoauth2_provider']."</label>
// </div>
// <div class='row'>
// 	<input type='text' class='saisie-50em' name='mail_configuration_authentification_type_settings[xoauth2_provider]' id='xoauth2_provider' value='!!xoauth2_provider!!' />
// </div>
// <div class='row'>
// 	<label class='etiquette' for='xoauth2_tenant_id'>".$msg['xoauth2_tenant_id']."</label>
// </div>
// <div class='row'>
// 	<input type='text' class='saisie-50em' name='mail_configuration_authentification_type_settings[xoauth2_tenant_id]' id='xoauth2_tenant_id' value='!!xoauth2_tenant_id!!' />
// </div>
// <div class='row'>
// 	<label class='etiquette' for='xoauth2_client_id'>".$msg['xoauth2_client_id']."</label>
// </div>
// <div class='row'>
// 	<input type='text' class='saisie-50em' name='mail_configuration_authentification_type_settings[xoauth2_client_id]' id='xoauth2_client_id' value='!!xoauth2_client_id!!' />
// </div>
// <div class='row'>
// 	<label class='etiquette' for='xoauth2_secret_value'>".$msg['xoauth2_secret_value']."</label>
// </div>
// <div class='row'>
// 	<input type='text' class='saisie-50em' name='mail_configuration_authentification_type_settings[xoauth2_secret_value]' id='xoauth2_secret_value' value='!!xoauth2_secret_value!!' />
// </div>
// <div class='row'>
// 	<label class='etiquette' for='xoauth2_refresh_token'>".$msg['xoauth2_refresh_token']."</label>
// </div>
// <div class='row'>
// 	<input type='text' class='saisie-50em' name='mail_configuration_authentification_type_settings[xoauth2_refresh_token]' id='xoauth2_refresh_token' value='!!xoauth2_refresh_token!!' />
// </div>
// ";

$mail_configuration_allowed_overrides_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_configuration_allowed_hote_override'>".$msg['mail_configuration_allowed_hote_override']."</label>
</div>
<div class='row'>
	<input type='checkbox' name='mail_configuration_allowed_hote_override' id='mail_configuration_allowed_hote_override' value='1' !!allowed_hote_override!! />
</div>
";
