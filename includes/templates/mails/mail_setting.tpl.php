<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_setting.tpl.php,v 1.2 2022/08/02 06:55:07 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".tpl.php")) die("no access");

global $mail_setting_content_form, $msg;

$mail_setting_content_form = "
<div class='row'>
	<label class='etiquette' for='mail_setting_type'>".$msg['mail_setting_classname']."</label>
</div>
<div class='row'>
	!!classname!!
	<input type='hidden' id='mail_setting_classname' name='mail_setting_classname' value='!!classname!!' />
</div>
<div class='row'>
	<label class='etiquette' for='mail_setting_from'>".$msg['mail_setting_sender']."</label>
</div>
<div class='row'>
	!!sender_selector!!
</div>
<div class='row'>
	<label class='etiquette' for='mail_setting_copy_cc'>".$msg['mail_setting_copy_cc']."</label>
</div>
<div class='row'>
	<input type='text' class='saisie-50em' name='mail_setting_copy_cc' id='mail_setting_copy_cc' value='!!copy_cc!!' />
</div>
<div class='row'>
	<label class='etiquette' for='mail_setting_copy_bcc'>".$msg['mail_setting_copy_bcc']."</label>
</div>
<div class='row'>
	!!copy_bcc_selector!!
</div>
<div class='row'>
	<label class='etiquette' for='mail_setting_reply'>".$msg['mail_setting_reply']."</label>
</div>
<div class='row'>
	!!reply_selector!!
</div>
<div class='row'>
	<label class='etiquette' for='mail_setting_associated_campaign'>".$msg['mail_setting_associated_campaign']."</label>
</div>
<div class='row'>
	!!associated_campaign_selector!!
</div>
";