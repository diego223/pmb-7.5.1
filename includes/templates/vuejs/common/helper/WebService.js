class WebService {

	constructor(url_base) {
		this.url = url_base;
	}

	get HTTP_POST() {
		return "POST";
	}

	get HTTP_GET() {
		return "GET";
	}

	async fetch(http_method, fetch_url, data) {

		if (!this.url) {
			throw new Error("[Webservice] url not set !");
		}

		let url = this.url + fetch_url;
		let init = {
			method: this.HTTP_GET,
			cache: 'no-cache'
		};

		if (http_method == this.HTTP_POST) {
			let post = new URLSearchParams();
			for (let prop in data) {
				if (typeof data[prop] == "boolean") {
					data[prop] = data[prop] ? 1 : 0;
				}
			}
			post.append("data", JSON.stringify(data));
			init['method'] = this.HTTP_POST;
			init['body'] = post;
		} else {
			url += '?';
			for (let prop in data) {
				url += '&' + prop + '=' + data[prop];
			}
		}

		try {
			let response = await fetch(url, init);
			let result = await response.json();
			if (result.error) {
				throw result.errorMessage;
			}
			return result;
		} catch (e) {
			return {
				error: true,
				errorMessage: e
			};
		}
	}

	post(route, action, data) {
		return this.fetch(this.HTTP_POST, `${route}/${action}`, data);
	}

	get(route, action, data) {
		return this.fetch(this.HTTP_GET, `${route}/${action}`, data);
	}
}

export default WebService;