import Vue from "vue";
import list from "./components/list.vue";
import add from "./components/add.vue";
import loader from "../components/loader.vue"

// Web service
import WebService from "../../common/helper/WebService.js";
Vue.prototype.ws = new WebService($data.url_webservice);

import Messages from "../../common/helper/Messages.js";
Vue.prototype.messages = Messages;

import Notif from "../../common/helper/Notif.js";
Vue.prototype.notif = Notif;

import Helper from "../../common/helper/Helper.js";
Vue.prototype.helper = Helper;

var loaderActive = false;
Vue.prototype.showLoader = () => {
	if (!loaderActive) {			
        window.dispatchEvent(new Event("showLoader"));
        loaderActive = true;
	}
}

Vue.prototype.hiddenLoader = () => {
	if (loaderActive) {
		setTimeout(() => {
	        window.dispatchEvent(new Event("hiddenLoader"));
	        loaderActive = false;
	    }, 300);
    }
}
new Vue({
	el : "#diffusions",
	data : {
		...$data
	},
	components : {
		list,
		add,
		loader
	}
});