import Vue from "vue";
import list from "./components/list.vue";
import edit from "./components/edit.vue";
import RMCForm from "../components/RMCForm.vue";

// Web service
import WebService from "../../common/helper/WebService.js";
Vue.prototype.ws = new WebService($data.url_webservice);

import Messages from "../../common/helper/Messages.js";
Vue.prototype.messages = Messages;

import Notif from "../../common/helper/Notif.js";
Vue.prototype.notif = Notif;

import Helper from "../../common/helper/Helper.js";
Vue.prototype.helper = Helper;

new Vue({
	el : "#sourcesItems",
	data : {
		...$data
	},
	components : {
		list,
		edit,
		RMCForm
	}
});