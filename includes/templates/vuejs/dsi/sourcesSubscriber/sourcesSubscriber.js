import Vue from "vue";

// Web service
import WebService from "../../common/helper/WebService.js";
Vue.prototype.ws = new WebService($data.url_webservice);

import Messages from "../../common/helper/Messages.js";
Vue.prototype.messages = Messages;

import Helper from "../../common/helper/Helper.js";
Vue.prototype.helper = Helper;

new Vue({
	el : "#sourcesSubscriber",
	data : {
		...$data
	},
	components : {}
});