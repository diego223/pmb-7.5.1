import Vue from "vue";
import status from "../components/status.vue";
import statusform from "../components/statusForm.vue";

// Web service
import WebService from "../../common/helper/WebService.js";
Vue.prototype.ws = new WebService($data.url_webservice);

import Notif from "../../common/helper/Notif.js";
Vue.prototype.notif = Notif;

import Messages from "../../common/helper/Messages.js";
Vue.prototype.messages = Messages;

import Helper from "../../common/helper/Helper.js";
Vue.prototype.helper = Helper;

new Vue({
	el : "#statusProducts",
	data : {
		...$data
	},
	components : {
		status, 
		statusform
	}
});