// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: EntityForm.js,v 1.1.4.1 2023/03/08 16:04:59 qvarin Exp $


define([
        "dojo/_base/declare", 
        "dojo/_base/lang",
        "dojo/on",
        "dojo/dom",
        "dijit/registry",
        "apps/pmb/PMBDojoxDialogSimple",
        "apps/pmb/pmbEventsHandler",
        ], 
		function(declare, lang, on, dom, registry, DialogSimple, pmbEventsHandler){
	return declare(null, {
		className: null,
		indexation: null,
		signals: null,
		dijits:null,
		formName:null,
		constructor: function(params){
			lang.mixin(this, params);
			this.signals = [];

			this.init();
			this.dijits = [];
		},
		
		handleEvents: function(evtType,evtArgs){
			switch(evtType){
				case 'leafRootClicked':
				case 'leafClicked':
					this.destroy();
					break;
				default:
					if (typeof this[evtType] == 'function') {
						this[evtType](evtArgs);
					}
					break;
			}
		},
		
		init: function(){
			pmbEventsHandler.initEvents(this);
		},
		destroy:function(){
			this.signals.forEach(function(signal){
				signal.remove();
			});
			this.dijits.forEach(function(dijit){
				dijit.destroyRecursive();
			});
		},

		loadDialog : function(params, evt, path) {
			var dijitId = params.entity_type+"_dialog";
			if(!this.dijits[dijitId]){
				var myDijit = registry.byId(dijitId);
				if (myDijit) {
					myDijit.destroyDescendants();
					myDijit.destroy();
				}
				this.dijits[dijitId] = new DialogSimple({title: pmbDojo.messages.getMessage('dsi', 'search_rmc_title'), executeScripts:true, id : dijitId, style:{width:'85%'}});				
				this.dijits[dijitId].attr('href', path);
				this.dijits[dijitId].startup();				
				this.signals.push(on(this.dijits[dijitId],"load", lang.hitch(this, function() {
					pmbEventsHandler.initEvents(this, dom.byId(dijitId));
					pmbEventsHandler.formToAjax(dom.byId(dijitId));
				})));
				this.signals.push(on(this.dijits[dijitId],"hide", lang.hitch(this, function() {
					this.dijits[dijitId].destroyRecursive();
					this.dijits = [];
				})));
			}
			this.dijits[dijitId].resize();
			this.dijits[dijitId].show();
			return this.dijits[dijitId];
		},
		
		hideDialog : function(params) {
			if (!params.className) {
				params.className = this.className;
			}
			var dijitId = params.entity_type + "_dialog";
			if (this.dijits[dijitId]) {
				this.dijits[dijitId].hide();
				this.dijits[dijitId].destroyRecursive();
				this.dijits = [];
			}
		},
		
		removeDialog : function(params) {
			var dijitId = params.entity_type+"_"+params.entity_id+"_dialog";
			if(this.dijits[dijitId]){
				this.dijits[dijitId].destroy();
			}
		}
	});
});