<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: mail_opac_reader.class.php,v 1.4 2022/08/01 06:44:59 dgoron Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

abstract class mail_opac_reader extends mail_opac {
	
	protected $empr;
	
	protected function get_mail_to_name() {
		return emprunteur::get_name($this->mail_to_id, 1);
	}
	
	protected function get_mail_to_mail() {
		return emprunteur::get_mail_empr($this->mail_to_id);
	}
	
	public function set_empr($empr) {
		$this->empr = $empr;
		return $this;
	}
}