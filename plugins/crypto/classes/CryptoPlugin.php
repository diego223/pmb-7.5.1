<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: CryptoPlugin.php,v 1.3 2023/01/03 09:51:33 dbellamy Exp $

if (stristr($_SERVER['REQUEST_URI'], '.class.php')) {
    die('no access');
}

use Pmb\Common\Library\Crypto\Crypto;

class CryptoPlugin {

    static protected $templates = [];
    
    static protected $crypto_class = null;
    static protected $keys_already_defined = false;
    
    private function __construct()
    {
    }

    /**
     * Chargement des templates d'affichage
     */
    protected static function loadTemplates()
    {
        if(!empty(static::$templates)) {
            return;
        }        
        require_once __DIR__.'/../includes/templates/crypto_plugin.tpl.php';
        if(!empty($crypto_plugin_templates)) {
            static::$templates = $crypto_plugin_templates;
        }
    }
    
    
    /** 
     * Chargement de la classe crypto et v�rification de la pr�sence des cl�s RSA
     */
    protected static function loadPMBRSAContext()
    {
        if( is_null(static::$crypto_class) ) {
            $crypto = static::$crypto_class = new Crypto();
            
            try {
                $crypto->loadPMBRSAContext();
                static::$keys_already_defined = true;
            } catch(Exception $e) {
            }
        }
    }

    
    public static function getNoAccessForm()
    {
        static::loadTemplates();
        $tpl = static::$templates['no_access'];
        return $tpl;
    }
    
    
    public static function getKeyGenerationForm()
    {
        static::loadTemplates();
        $tpl = static::$templates['key_generation'];
        
        static::loadPMBRSAContext();
        $crypto = static::$crypto_class;
        
        $RSA_keys = ['public' => '', 'private' => ''];
        
        try {
            $RSA_keys = $crypto->generateRSAKeyPair();
        } catch(Exception $e) {
        }
        
        $tpl = str_replace('<!-- crypto_private_key_value -->', $RSA_keys['private'], $tpl);
        $tpl = str_replace('<!-- crypto_public_key_value -->',  $RSA_keys['public'], $tpl);
        
        if(static::$keys_already_defined) {
            $tpl = str_replace('<!-- crypto_keys_already_defined -->', static::$templates['keys_already_defined'], $tpl);
        }
        return $tpl;
    }

    
    public static function getDataEncryptionForm(string $data = '', string $action = '')
    {
        static::loadTemplates();
        $tpl = static::$templates['data_encryption'];
        
        static::loadPMBRSAContext();
        $crypto = static::$crypto_class;
        
        if(!static::$keys_already_defined) {
            $tpl = str_replace('<!-- crypto_keys_not_defined -->', static::$templates['keys_not_defined'], $tpl);
            $action = '';
        }
        
        switch($action) {
            
            case 'encrypt' : 
                
                $data= stripslashes($data);
                
                // Chiffrement avec la cl� priv�e et transfo Binaire en Hexa
                $bin_encrypted_chunks = [];
                $hex_encrypted_chunks = [];
                $hex_encrypted_data = '';
                try {
                    $bin_encrypted_chunks = $crypto->encryptWithPrivateRSAKey($data);
                } catch (Exception $e) {
                }
                for($i = 0; $i < count($bin_encrypted_chunks); $i++) {
                    $hex_encrypted_chunks[$i] = bin2hex($bin_encrypted_chunks[$i]);
                }
                $hex_encrypted_data = implode(Crypto::CHUNK_SEPARATOR, $hex_encrypted_chunks);
                $tpl = str_replace('<!-- crypto_data_to_encrypt -->', $data, $tpl);
                $tpl = str_replace('<!-- crypto_encrypted_data -->', Crypto::INDICATOR.$hex_encrypted_data, $tpl);
                $tpl = str_replace('<!-- crypto_data_to_decrypt -->', '', $tpl);
                
                // V�rification par d�chiffrement avec la cl� publique
                 $decrypted_data = '';
                try {
                    $decrypted_data = $crypto->decryptWithPublicRSAKey($bin_encrypted_chunks);
                } catch (Exception $e) {
                }
                $tpl = str_replace('<!-- crypto_decrypted_data -->', $decrypted_data, $tpl);

                break;

            case 'decrypt' : 
                
                $data = stripslashes($data);
                $data_wo_indicator = str_replace(Crypto::INDICATOR, '', $data);
                $hex_encrypted_chunks = explode(Crypto::CHUNK_SEPARATOR, $data_wo_indicator);
                $bin_encrypted_chunks = [];
                
                //Transfo donn�es � d�chiffrer Hexa vers binaire
                for($i=0 ; $i<count($hex_encrypted_chunks); $i++) {
                    $bin_encrypted_chunks[$i] = @hex2bin($hex_encrypted_chunks[$i]);
                    if(false == $bin_encrypted_chunks[$i]) {
                        $tpl = str_replace('<!-- crypto_data_to_encrypt -->', '', $tpl);
                        $tpl = str_replace('<!-- crypto_encrypted_data -->', '', $tpl);
                        $tpl = str_replace('<!-- crypto_data_to_decrypt -->', $data, $tpl);      
                        $error_msg = plugins::get_message('crypto', 'crypto_invalid_data_to_decrypt');
                        $tpl = str_replace('<!-- crypto_decrypted_data -->', $error_msg, $tpl);
                        return $tpl;
                    }
                }
                //D�chiffrement avec cl� la publique
                try {
                    $decrypted_data = $crypto->decryptWithPublicRSAKey($bin_encrypted_chunks);
                } catch (Exception $e) {
                }
                
                $tpl = str_replace('<!-- crypto_data_to_encrypt -->', '', $tpl);
                $tpl = str_replace('<!-- crypto_data_to_decrypt -->', $data, $tpl);
                $tpl = str_replace('<!-- crypto_decrypted_data -->', $decrypted_data, $tpl);
                
                //V�rification par chiffrement avec la cl� priv�e
                $bin_encrypted_chunks = [];
                $hex_encrypted_chunks = [];
                $hex_encrypted_data = '';
                try {
                    $bin_encrypted_chunks = $crypto->encryptWithPrivateRSAKey($decrypted_data);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                for($i = 0; $i < count($bin_encrypted_chunks); $i++) {
                    $hex_encrypted_chunks[$i] = bin2hex($bin_encrypted_chunks[$i]);
                }
                $hex_encrypted_data = implode(Crypto::CHUNK_SEPARATOR, $hex_encrypted_chunks);
                $tpl = str_replace('<!-- crypto_encrypted_data -->', Crypto::INDICATOR.$hex_encrypted_data, $tpl);
                break;
            
            default :
                $tpl = str_replace('<!-- crypto_data_to_encrypt -->', '', $tpl);
                $tpl = str_replace('<!-- crypto_encrypted_data -->', '', $tpl);
                $tpl = str_replace('<!-- crypto_data_to_decrypt -->', '', $tpl);
                $tpl = str_replace('<!-- crypto_decrypted_data -->', '', $tpl);
                break;
        }

        return $tpl;
    }
}
